webpackJsonp(["academic.module.0"],{

/***/ "../../../../../src/app/views/employee/academic/academic-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcademicRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__institutewise_student_info_institutewise_student_info_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/institutewise-student-info/institutewise-student-info.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__class_timetable_class_timetable_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/class-timetable/class-timetable.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__student_attendance_entry_student_attendance_entry_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-entry/student-attendance-entry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__student_attendance_extraclass_student_attendance_extraclass_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-extraclass/student-attendance-extraclass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__student_attendance_muster_student_attendance_muster_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-muster/student-attendance-muster.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        data: {
            title: 'Academic'
        },
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_3__class_timetable_class_timetable_component__["a" /* ClassTimetableComponent */],
                data: {
                    title: 'My class time Table'
                }
            },
            {
                path: 'InstitutewiseStudentInfo',
                component: __WEBPACK_IMPORTED_MODULE_2__institutewise_student_info_institutewise_student_info_component__["a" /* InstitutewiseStudentInfoComponent */],
                data: {
                    title: 'Institute Wise Student Information'
                }
            },
            {
                path: 'Studentattendanceentry',
                component: __WEBPACK_IMPORTED_MODULE_4__student_attendance_entry_student_attendance_entry_component__["a" /* StudentAttendanceEntryComponent */],
                data: {
                    title: 'Student Attendance Entry'
                }
            },
            {
                path: 'StudentAttendanceMuster',
                component: __WEBPACK_IMPORTED_MODULE_6__student_attendance_muster_student_attendance_muster_component__["a" /* StudentAttendanceMusterComponent */],
                data: {
                    title: 'Student Attendance Muster'
                }
            },
            {
                path: 'StudentAttendanceExtraclass',
                component: __WEBPACK_IMPORTED_MODULE_5__student_attendance_extraclass_student_attendance_extraclass_component__["a" /* StudentAttendanceExtraclassComponent */],
                data: {
                    title: 'Student Attendance Extra class'
                }
            },
        ]
    }
];
var AcademicRoutingModule = (function () {
    function AcademicRoutingModule() {
    }
    AcademicRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], AcademicRoutingModule);
    return AcademicRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/academic/academic.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcademicModule", function() { return AcademicModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__institutewise_student_info_institutewise_student_info_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/institutewise-student-info/institutewise-student-info.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__class_timetable_class_timetable_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/class-timetable/class-timetable.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__student_attendance_entry_student_attendance_entry_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-entry/student-attendance-entry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__student_attendance_extraclass_student_attendance_extraclass_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-extraclass/student-attendance-extraclass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__student_attendance_muster_student_attendance_muster_component__ = __webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-muster/student-attendance-muster.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_tabs__ = __webpack_require__("../../../../ngx-bootstrap/tabs/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__academic_routing_module__ = __webpack_require__("../../../../../src/app/views/employee/academic/academic-routing.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// Forms Component
//import { FormsComponent } from './forms.component';

// Modal Component

// Tabs Component

// Components Routing

var AcademicModule = (function () {
    function AcademicModule() {
    }
    AcademicModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_9__academic_routing_module__["a" /* AcademicRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap_modal__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_tabs__["a" /* TabsModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__institutewise_student_info_institutewise_student_info_component__["a" /* InstitutewiseStudentInfoComponent */],
                __WEBPACK_IMPORTED_MODULE_2__class_timetable_class_timetable_component__["a" /* ClassTimetableComponent */],
                __WEBPACK_IMPORTED_MODULE_3__student_attendance_entry_student_attendance_entry_component__["a" /* StudentAttendanceEntryComponent */],
                __WEBPACK_IMPORTED_MODULE_4__student_attendance_extraclass_student_attendance_extraclass_component__["a" /* StudentAttendanceExtraclassComponent */],
                __WEBPACK_IMPORTED_MODULE_5__student_attendance_muster_student_attendance_muster_component__["a" /* StudentAttendanceMusterComponent */]
            ]
        })
    ], AcademicModule);
    return AcademicModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/academic/class-timetable/class-timetable.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Time Table Teacher Wise</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label \" for=\"email-input\">Registration Code<strong style=\"color:red\">*</strong></label>\n                <div class=\"col-sm-4\">\n\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control detailsearch\" placeholder=\"2422\" (dblclick)=\"largeModal.show()\">\n                </div>\n\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Employee Code <strong style=\"color:red\">*</strong></label>\n                <div class=\"col-sm-4\">\n                    <input desabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"6731\">\n                </div>\n                <label>Shanu Sharma</label>\n            </div>\n            <div class=\"form-group row\">\n                <div class=\"col-md-12\">\n                    <button class=\"btn btn-success\">Show</button>\n                    <button class=\"btn btn-danger\">Close</button>\n                    <!-- <button (click)=\"openPdf()\">Open PDF</button> -->\n\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>\n\n\n<div bsModal #largeModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Select Registration Master</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal.hide()\" aria-label=\"Close\">\n                <span aria-hidden=\"true\">&times;</span>\n              </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n                                <option>All</option>\n                                <option>SI No</option>\n                                <option>Registration Code</option>\n                                <option>Registration Desc</option>\n                                <option>Registration Date From</option>\n                                <option>Registration Date To</option>\n                         </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\"><strong>0 Records of 0</strong></span>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-sm-12\" style=\"overflow:auto\">\n                        <table class=\"table table-bordered table-striped table-sm\">\n                            <thead>\n                                <tr>\n                                    <th>SI No</th>\n                                    <th>Employee Code</th>\n                                    <th>Employee Name</th>\n                                    <th>Registration Code</th>\n                                    <th>Registration Desc</th>\n                                    <th>Registration Date From</th>\n                                    <th>Registration Date To</th>\n                                </tr>\n                            </thead>\n                            <tbody>\n\n                                <tr></tr>\n                            </tbody>\n                        </table>\n                    </div>\n\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal -->"

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/class-timetable/class-timetable.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".detailsearch {\n  background-color: #fcfccd;\n  border: 1px solid #1f1fdf;\n  border-radius: 5px;\n  box-shadow: 4px 4px 8px 0px darkgrey; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/class-timetable/class-timetable.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassTimetableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//declare const pdfMake;
var ClassTimetableComponent = (function () {
    function ClassTimetableComponent() {
        this.docDefinition = {};
    }
    ClassTimetableComponent.prototype.ngOnInit = function () {
        // playground requires you to assign document definition to a variable called dd
        this.docDefinition = {
            content: [
                { text: 'Tables', style: 'header' },
                'Official documentation is in progress, this document is just a glimpse of what is possible with pdfmake and its layout engine.',
                { text: 'A simple table (no headers, no width specified, no spans, no styling)', style: 'subheader' },
                'The following table has nothing more than a body array',
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            ['Column 1', 'Column 2', 'Column 3'],
                            ['One value goes here', 'Another one here', 'OK?']
                        ]
                    }
                },
                { text: 'A simple table with nested elements', style: 'subheader' },
                'It is of course possible to nest any other type of nodes available in pdfmake inside table cells',
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            ['Column 1', 'Column 2', 'Column 3'],
                            [
                                {
                                    stack: [
                                        'Let\'s try an unordered list',
                                        {
                                            ul: [
                                                'item 1',
                                                'item 2'
                                            ]
                                        }
                                    ]
                                },
                                [
                                    'or a nested table',
                                    {
                                        table: {
                                            body: [
                                                ['Col1', 'Col2', 'Col3'],
                                                ['1', '2', '3'],
                                                ['1', '2', '3']
                                            ]
                                        },
                                    }
                                ],
                                { text: [
                                        'Inlines can be ',
                                        { text: 'styled\n', italics: true },
                                        { text: 'easily as everywhere else', fontSize: 10 }
                                    ]
                                }
                            ]
                        ]
                    }
                },
                { text: 'Defining column widths', style: 'subheader' },
                'Tables support the same width definitions as standard columns:',
                {
                    bold: true,
                    ul: [
                        'auto',
                        'star',
                        'fixed value'
                    ]
                },
                {
                    style: 'tableExample',
                    table: {
                        widths: [100, '*', 200, '*'],
                        body: [
                            ['width=100', 'star-sized', 'width=200', 'star-sized'],
                            ['fixed-width cells have exactly the specified width', { text: 'nothing interesting here', italics: true, color: 'gray' }, { text: 'nothing interesting here', italics: true, color: 'gray' }, { text: 'nothing interesting here', italics: true, color: 'gray' }]
                        ]
                    }
                },
                {
                    style: 'tableExample',
                    table: {
                        widths: ['*', 'auto'],
                        body: [
                            ['This is a star-sized column. The next column over, an auto-sized column, will wrap to accomodate all the text in this cell.', 'I am auto sized.'],
                        ]
                    }
                },
                {
                    style: 'tableExample',
                    table: {
                        widths: ['*', 'auto'],
                        body: [
                            ['This is a star-sized column. The next column over, an auto-sized column, will not wrap to accomodate all the text in this cell, because it has been given the noWrap style.', { text: 'I am auto sized.', noWrap: true }],
                        ]
                    }
                },
                { text: 'Defining row heights', style: 'subheader' },
                {
                    style: 'tableExample',
                    table: {
                        heights: [20, 50, 70],
                        body: [
                            ['row 1 with height 20', 'column B'],
                            ['row 2 with height 50', 'column B'],
                            ['row 3 with height 70', 'column B']
                        ]
                    }
                },
                'With same height:',
                {
                    style: 'tableExample',
                    table: {
                        heights: 40,
                        body: [
                            ['row 1', 'column B'],
                            ['row 2', 'column B'],
                            ['row 3', 'column B']
                        ]
                    }
                },
                'With height from function:',
                {
                    style: 'tableExample',
                    table: {
                        heights: function (row) {
                            return (row + 1) * 25;
                        },
                        body: [
                            ['row 1', 'column B'],
                            ['row 2', 'column B'],
                            ['row 3', 'column B']
                        ]
                    }
                },
                { text: 'Column/row spans', pageBreak: 'before', style: 'subheader' },
                'Each cell-element can set a rowSpan or colSpan',
                {
                    style: 'tableExample',
                    color: '#444',
                    table: {
                        widths: [200, 'auto', 'auto'],
                        headerRows: 2,
                        // keepWithHeaderRows: 1,
                        body: [
                            [{ text: 'Header with Colspan = 2', style: 'tableHeader', colSpan: 2, alignment: 'center' }, {}, { text: 'Header 3', style: 'tableHeader', alignment: 'center' }],
                            [{ text: 'Header 1', style: 'tableHeader', alignment: 'center' }, { text: 'Header 2', style: 'tableHeader', alignment: 'center' }, { text: 'Header 3', style: 'tableHeader', alignment: 'center' }],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            [{ rowSpan: 3, text: 'rowSpan set to 3\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor' }, 'Sample value 2', 'Sample value 3'],
                            ['', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', { colSpan: 2, rowSpan: 2, text: 'Both:\nrowSpan and colSpan\ncan be defined at the same time' }, ''],
                            ['Sample value 1', '', ''],
                        ]
                    }
                },
                { text: 'Headers', pageBreak: 'before', style: 'subheader' },
                'You can declare how many rows should be treated as a header. Headers are automatically repeated on the following pages',
                { text: ['It is also possible to set keepWithHeaderRows to make sure there will be no page-break between the header and these rows. Take a look at the document-definition and play with it. If you set it to one, the following table will automatically start on the next page, since there\'s not enough space for the first row to be rendered here'], color: 'gray', italics: true },
                {
                    style: 'tableExample',
                    table: {
                        headerRows: 1,
                        // dontBreakRows: true,
                        // keepWithHeaderRows: 1,
                        body: [
                            [{ text: 'Header 1', style: 'tableHeader' }, { text: 'Header 2', style: 'tableHeader' }, { text: 'Header 3', style: 'tableHeader' }],
                            [
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                            ]
                        ]
                    }
                },
                { text: 'Styling tables', style: 'subheader' },
                'You can provide a custom styler for the table. Currently it supports:',
                {
                    ul: [
                        'line widths',
                        'line colors',
                        'cell paddings',
                    ]
                },
                'with more options coming soon...\n\npdfmake currently has a few predefined styles (see them on the next page)',
                { text: 'noBorders:', fontSize: 14, bold: true, pageBreak: 'before', margin: [0, 0, 0, 8] },
                {
                    style: 'tableExample',
                    table: {
                        headerRows: 1,
                        body: [
                            [{ text: 'Header 1', style: 'tableHeader' }, { text: 'Header 2', style: 'tableHeader' }, { text: 'Header 3', style: 'tableHeader' }],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                        ]
                    },
                    layout: 'noBorders'
                },
                { text: 'headerLineOnly:', fontSize: 14, bold: true, margin: [0, 20, 0, 8] },
                {
                    style: 'tableExample',
                    table: {
                        headerRows: 1,
                        body: [
                            [{ text: 'Header 1', style: 'tableHeader' }, { text: 'Header 2', style: 'tableHeader' }, { text: 'Header 3', style: 'tableHeader' }],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                        ]
                    },
                    layout: 'headerLineOnly'
                },
                { text: 'lightHorizontalLines:', fontSize: 14, bold: true, margin: [0, 20, 0, 8] },
                {
                    style: 'tableExample',
                    table: {
                        headerRows: 1,
                        body: [
                            [{ text: 'Header 1', style: 'tableHeader' }, { text: 'Header 2', style: 'tableHeader' }, { text: 'Header 3', style: 'tableHeader' }],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                        ]
                    },
                    layout: 'lightHorizontalLines'
                },
                { text: 'but you can provide a custom styler as well', margin: [0, 20, 0, 8] },
                {
                    style: 'tableExample',
                    table: {
                        headerRows: 1,
                        body: [
                            [{ text: 'Header 1', style: 'tableHeader' }, { text: 'Header 2', style: 'tableHeader' }, { text: 'Header 3', style: 'tableHeader' }],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                        ]
                    },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 2 : 1;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                        },
                    }
                },
                { text: 'zebra style', margin: [0, 20, 0, 8] },
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                            ['Sample value 1', 'Sample value 2', 'Sample value 3'],
                        ]
                    },
                    layout: {
                        fillColor: function (i, node) {
                            return (i % 2 === 0) ? '#CCCCCC' : null;
                        }
                    }
                },
                { text: 'Optional border', fontSize: 14, bold: true, pageBreak: 'before', margin: [0, 0, 0, 8] },
                'Each cell contains an optional border property: an array of 4 booleans for left border, top border, right border, bottom border.',
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            [
                                {
                                    border: [false, true, false, false],
                                    fillColor: '#eeeeee',
                                    text: 'border:\n[false, true, false, false]'
                                },
                                {
                                    border: [false, false, false, false],
                                    fillColor: '#dddddd',
                                    text: 'border:\n[false, false, false, false]'
                                },
                                {
                                    border: [true, true, true, true],
                                    fillColor: '#eeeeee',
                                    text: 'border:\n[true, true, true, true]'
                                }
                            ],
                            [
                                {
                                    rowSpan: 3,
                                    border: [true, true, true, true],
                                    fillColor: '#eeeeff',
                                    text: 'rowSpan: 3\n\nborder:\n[true, true, true, true]'
                                },
                                {
                                    border: undefined,
                                    fillColor: '#eeeeee',
                                    text: 'border:\nundefined'
                                },
                                {
                                    border: [true, false, false, false],
                                    fillColor: '#dddddd',
                                    text: 'border:\n[true, false, false, false]'
                                }
                            ],
                            [
                                '',
                                {
                                    colSpan: 2,
                                    border: [true, true, true, true],
                                    fillColor: '#eeffee',
                                    text: 'colSpan: 2\n\nborder:\n[true, true, true, true]'
                                },
                                ''
                            ],
                            [
                                '',
                                {
                                    border: undefined,
                                    fillColor: '#eeeeee',
                                    text: 'border:\nundefined'
                                },
                                {
                                    border: [false, false, true, true],
                                    fillColor: '#dddddd',
                                    text: 'border:\n[false, false, true, true]'
                                }
                            ]
                        ]
                    },
                    layout: {
                        defaultBorder: false,
                    }
                },
                'For every cell without a border property, whether it has all borders or not is determined by layout.defaultBorder, which is false in the table above and true (by default) in the table below.',
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            [
                                {
                                    border: [false, false, false, false],
                                    fillColor: '#eeeeee',
                                    text: 'border:\n[false, false, false, false]'
                                },
                                {
                                    fillColor: '#dddddd',
                                    text: 'border:\nundefined'
                                },
                                {
                                    fillColor: '#eeeeee',
                                    text: 'border:\nundefined'
                                },
                            ],
                            [
                                {
                                    fillColor: '#dddddd',
                                    text: 'border:\nundefined'
                                },
                                {
                                    fillColor: '#eeeeee',
                                    text: 'border:\nundefined'
                                },
                                {
                                    border: [true, true, false, false],
                                    fillColor: '#dddddd',
                                    text: 'border:\n[true, true, false, false]'
                                },
                            ]
                        ]
                    }
                },
                'And some other examples with rowSpan/colSpan...',
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            [
                                '',
                                'column 1',
                                'column 2',
                                'column 3'
                            ],
                            [
                                'row 1',
                                {
                                    rowSpan: 3,
                                    colSpan: 3,
                                    border: [true, true, true, true],
                                    fillColor: '#cccccc',
                                    text: 'rowSpan: 3\ncolSpan: 3\n\nborder:\n[true, true, true, true]'
                                },
                                '',
                                ''
                            ],
                            [
                                'row 2',
                                '',
                                '',
                                ''
                            ],
                            [
                                'row 3',
                                '',
                                '',
                                ''
                            ]
                        ]
                    },
                    layout: {
                        defaultBorder: false,
                    }
                },
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            [
                                {
                                    colSpan: 3,
                                    text: 'colSpan: 3\n\nborder:\n[false, false, false, false]',
                                    fillColor: '#eeeeee',
                                    border: [false, false, false, false]
                                },
                                '',
                                ''
                            ],
                            [
                                'border:\nundefined',
                                'border:\nundefined',
                                'border:\nundefined'
                            ]
                        ]
                    }
                },
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            [
                                { rowSpan: 3, text: 'rowSpan: 3\n\nborder:\n[false, false, false, false]', fillColor: '#eeeeee', border: [false, false, false, false] },
                                'border:\nundefined',
                                'border:\nundefined'
                            ],
                            [
                                '',
                                'border:\nundefined',
                                'border:\nundefined'
                            ],
                            [
                                '',
                                'border:\nundefined',
                                'border:\nundefined'
                            ]
                        ]
                    }
                }
            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 0, 0, 10]
                },
                subheader: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 10, 0, 5]
                },
                tableExample: {
                    margin: [0, 5, 0, 15]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                }
            },
            defaultStyle: {}
        };
    };
    ClassTimetableComponent.prototype.openPdf = function () {
        // pdfMake.createPdf(this.docDefinition).open();
    };
    ClassTimetableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-class-timetable',
            template: __webpack_require__("../../../../../src/app/views/employee/academic/class-timetable/class-timetable.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/academic/class-timetable/class-timetable.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ClassTimetableComponent);
    return ClassTimetableComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/academic/institutewise-student-info/institutewise-student-info.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Student Information Search</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"form-group row\">\n                <label class=\"col-md-4 col-form-label\">Show Student Whose Status Is:</label>\n                <div class=\"col-md-8\">\n                    <label class=\"checkbox-inline\" for=\"inline-checkbox1\">\n                    <input type=\"checkbox\" id=\"inline-checkbox1\" name=\"inline-checkbox1\" value=\"Active\"> Active&nbsp;\n                  </label>\n                    <label class=\"checkbox-inline\" for=\"inline-checkbox2\">\n                    <input type=\"checkbox\" id=\"inline-checkbox2\" name=\"inline-checkbox2\" value=\"Deactive\"> Deactive&nbsp;\n                  </label>\n                    <label class=\"checkbox-inline\" for=\"inline-checkbox3\">\n                    <input type=\"checkbox\" id=\"inline-checkbox3\" name=\"inline-checkbox3\" value=\"AdmissionCancel\"> Admission Cancel&nbsp;\n                  </label>\n                    <label class=\"checkbox-inline\" for=\"inline-checkbox3\">\n                      <input type=\"checkbox\" id=\"inline-checkbox3\" name=\"inline-checkbox3\" value=\"Suspended\"> Suspended&nbsp;\n                    </label>\n                    <label class=\"checkbox-inline\" for=\"inline-checkbox3\">\n                        <input type=\"checkbox\" id=\"inline-checkbox3\" name=\"inline-checkbox3\" value=\"ProgramComplete\"> Program Complete&nbsp;\n                      </label>\n                    <label class=\"checkbox-inline\" for=\"inline-checkbox3\">\n                          <input type=\"checkbox\" id=\"inline-checkbox3\" name=\"inline-checkbox3\" value=\"NotInRollCall\"> Not In Roll Call&nbsp;\n                        </label>\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-md-4 col-form-label\">Show Student Whose criteria to Search is</label>\n                <div class=\"col-md-8\">\n                    <label class=\"radio-inline\" for=\"inline-radio1\">\n                    <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios\" value=\"StudentName\"> Student Name\n                     </label>\n                    <label class=\"radio-inline\" for=\"inline-radio2\">\n                       <input type=\"radio\" id=\"inline-radio2\" name=\"inline-radios\" value=\"RegistrationNo\"> Registration No.\n                       </label>\n                    <label class=\"radio-inline\" for=\"inline-radio3\">\n                          <input type=\"radio\" id=\"inline-radio3\" name=\"inline-radios\" value=\"AcademicYear\"> Academic Year\n                     </label>\n                    <label class=\"radio-inline\" for=\"inline-radio3\">\n                      <input type=\"radio\" id=\"inline-radio3\" name=\"inline-radios\" value=\"Program\"> Program\n                    </label>\n                    <label class=\"radio-inline\" for=\"inline-radio3\">\n                        <input type=\"radio\" id=\"inline-radio3\" name=\"inline-radios\" value=\"STYNo\"> STY(semester)No.\n                      </label>\n                    <label class=\"radio-inline\" for=\"inline-radio3\">\n                          <input type=\"radio\" id=\"inline-radio3\" name=\"inline-radios\" value=\"Rank\"> Rank\n                        </label>\n\n                    <label class=\"radio-inline\" for=\"inline-radio3\">\n                            <input type=\"radio\" id=\"inline-radio3\" name=\"inline-radios\" value=\"BloodGroup\"> Blood Group\n                          </label>\n                    <label class=\"radio-inline\" for=\"inline-radio3\">\n                              <input type=\"radio\" id=\"inline-radio3\" name=\"inline-radios\" value=\"Branch\">Branch\n                            </label>\n\n                    <label class=\"radio-inline\" for=\"inline-radio3\">\n                                <input type=\"radio\" id=\"inline-radio3\" name=\"inline-radios\" value=\"CategoryRank\"> Category Rank\n                              </label>\n                    <label class=\"radio-inline\" for=\"inline-radio3\">\n                                  <input type=\"radio\" id=\"inline-radio3\" name=\"inline-radios\" value=\"Section\"> Section\n                                </label>\n                    <label class=\"radio-inline\" for=\"inline-radio3\">\n                                    <input type=\"radio\" id=\"inline-radio3\" name=\"inline-radios\" value=\"StudentCategory\">Student Category\n                                  </label>\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-md-4 col-form-label\">Enter Desired Character(s) to Search</label>\n                <div class=\"controls col-sm-6\">\n                    <div class=\"input-group\">\n                        <input id=\"appendedInputButtons\" class=\"form-control\" type=\"text\">\n                        <span class=\"input-group-btn\">\n                <button class=\"btn btn-info\" type=\"button\">Search</button>\n\n              </span>\n                    </div>\n                    <!-- <button class=\"btn btn-danger\" type=\"button\">Close</button> -->\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Click the Desired Student Name to Continue</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"col-sm-12\" style=\"overflow:auto\">\n                <table class=\"table table-bordered table-striped table-sm\">\n                    <thead>\n                        <tr>\n                            <th>SI No.</th>\n                            <th>Registration No.-Student Name</th>\n                            <th>Academic Year</th>\n                            <th>Active Status</th>\n                            <th>Program-Branch-Section</th>\n                            <th>STY</th>\n\n\n                        </tr>\n                    </thead>\n                    <tbody>\n\n                        <tr></tr>\n                    </tbody>\n                </table>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/institutewise-student-info/institutewise-student-info.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/institutewise-student-info/institutewise-student-info.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstitutewiseStudentInfoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InstitutewiseStudentInfoComponent = (function () {
    function InstitutewiseStudentInfoComponent() {
    }
    InstitutewiseStudentInfoComponent.prototype.ngOnInit = function () {
    };
    InstitutewiseStudentInfoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-institutewise-student-info',
            template: __webpack_require__("../../../../../src/app/views/employee/academic/institutewise-student-info/institutewise-student-info.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/academic/institutewise-student-info/institutewise-student-info.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InstitutewiseStudentInfoComponent);
    return InstitutewiseStudentInfoComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/academic/student-attendance-entry/student-attendance-entry.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Date Wise Attendance Entry</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label \" for=\"email-input\">Semester Code<strong style=\"color:red\">*</strong></label>\n                <div class=\"col-sm-4\">\n                    <select class=\"form-control\">\n                     <option>SEM1</option>\n                     <option>SEM2</option>\n                        </select>\n                </div>\n                <label class=\"col-sm-2 col-form-label \" for=\"email-input\">Attendance Date<strong style=\"color:red\">*</strong></label>\n                <div class=\"col-sm-4\">\n                    <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                </div>\n\n            </div>\n            <div class=\"form-group row\">\n                <div class=\"col-md-12\">\n                    <button class=\"btn btn-success\">Show Selected Day Attendance</button>\n                    <button class=\"btn btn-info\">Show All Day Status</button>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Date Wise Attendance Entry</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"row\">\n                <div class=\"col-sm-12 row\" style=\"margin-bottom:5px\">\n                    <div class=\"donediv\"></div>&nbsp;&nbsp;&nbsp;<span>Attendance Done&nbsp;&nbsp;&nbsp;</span>\n                    <div class=\"pendingdiv\"></div>&nbsp;&nbsp;&nbsp;<span>Attendance Pending</span>\n                </div>\n\n                <div class=\"col-sm-12\" style=\"overflow:auto\">\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>Date</th>\n                                <th>Day</th>\n                                <th>Time Slots</th>\n                                <th>Subject Code</th>\n                                <th>Program/STY</th>\n                                <th>Section</th>\n                                <th>LTP</th>\n                                <th>Sub Section</th>\n                                <th>Remarks</th>\n                                <th>Total Class</th>\n                                <th>Print</th>\n\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                            <tr></tr>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/student-attendance-entry/student-attendance-entry.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".donediv {\n  width: 5%;\n  height: 40px;\n  background-color: green; }\n\n.pendingdiv {\n  width: 5%;\n  height: 40px;\n  background-color: #6d95c4; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/student-attendance-entry/student-attendance-entry.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentAttendanceEntryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StudentAttendanceEntryComponent = (function () {
    function StudentAttendanceEntryComponent() {
    }
    StudentAttendanceEntryComponent.prototype.ngOnInit = function () {
    };
    StudentAttendanceEntryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-student-attendance-entry',
            template: __webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-entry/student-attendance-entry.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-entry/student-attendance-entry.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StudentAttendanceEntryComponent);
    return StudentAttendanceEntryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/academic/student-attendance-extraclass/student-attendance-extraclass.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Student Attendance Entry</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"form-group row\">\n                <label class=\"col-md-4 col-form-label\">Attendance Type</label>\n                <div class=\"col-md-8\">\n                    <label class=\"radio-inline\" for=\"inline-radio1\">\n                    <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios\" value=\"ExtraClass\"> Extra Class\n                     </label>\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-md-2 col-form-label\" for=\"disabled-input\">Semester Code</label>\n                <div class=\"col-md-2\">\n                    <select id=\"select\" name=\"select\" class=\"form-control\">\n                    <option value=\"0\">Please select</option>\n                    <option value=\"1\">Option #1</option>\n                    <option value=\"2\">Option #2</option>\n                    <option value=\"3\">Option #3</option>\n                  </select>\n                </div>\n                <label class=\"col-md-2 col-form-label\" for=\"disabled-input\">Subject</label>\n                <div class=\"col-md-2\">\n                    <select id=\"select\" name=\"select\" class=\"form-control\">\n                    <option value=\"0\">Please select</option>\n                    <option value=\"1\">Option #1</option>\n                    <option value=\"2\">Option #2</option>\n                    <option value=\"3\">Option #3</option>\n                  </select>\n                </div>\n                <label class=\"col-md-2 col-form-label\" for=\"disabled-input\">Component</label>\n                <div class=\"col-md-2\">\n                    <select id=\"select\" name=\"select\" class=\"form-control\">\n                    <option value=\"0\">Please select</option>\n                    <option value=\"1\">Option #1</option>\n                    <option value=\"2\">Option #2</option>\n                    <option value=\"3\">Option #3</option>\n                  </select>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-sm-6\">\n                    <div class=\"card\">\n                        <div class=\"card-body\">\n                            <div style=\"overflow:auto\">\n                                <table class=\"table table-bordered table-striped table-sm\">\n                                    <thead>\n                                        <tr>\n                                            <th><input type=\"checkbox\"></th>\n                                            <th>SI No.</th>\n                                            <th>Academic Year</th>\n                                            <th>Program</th>\n                                            <th>STY No</th>\n                                            <th>Section</th>\n                                            <th>Sub Section</th>\n                                            <th>Extra Class</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n\n                                        <tr></tr>\n                                    </tbody>\n                                </table>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-sm-3\">\n                    <div class=\"card\">\n                        <div class=\"card-body\">\n                            <div class=\"form-grup row\">\n                                <div class=\"col-sm-12\">\n                                    <button class=\"btn btn-success\">All Posted Attendance</button>\n                                </div>\n\n                            </div>\n                            <div class=\"form-grup row\">\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-6 col-form-label\" for=\"disabled-input\">Attendance Date</label>\n                                    <div class=\"col-sm-6\">\n                                        <input type=\"date\" id=\"disabled-input\" name=\"disabled-input\" class=\"form-control\" placeholder=\"\">\n                                    </div>\n                                </div>\n                                <div class=\"form-group row\">\n                                    <div class=\"col-sm-12\">\n                                        <label class=\"checkbox-inline\" for=\"inline-checkbox1\">\n                                        <input type=\"checkbox\" id=\"inline-checkbox1\" name=\"inline-checkbox1\" value=\"option1\">Attendance With Lesson and Topic\n                                      </label>\n                                    </div>\n                                </div>\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-6 col-form-label\" for=\"disabled-input\">Lesson Code</label>\n                                    <div class=\"col-sm-6\">\n                                        <select id=\"select\" name=\"select\" class=\"form-control\">\n                                        <option value=\"0\">Please select</option>\n                                        <option value=\"1\">Option #1</option>\n                                        <option value=\"2\">Option #2</option>\n                                        <option value=\"3\">Option #3</option>\n                                      </select>\n                                    </div>\n                                </div>\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-6 col-form-label\" for=\"disabled-input\">Topic Code</label>\n                                    <div class=\"col-sm-6\">\n                                        <select id=\"select\" name=\"select\" class=\"form-control\">\n                                        <option value=\"0\">Please select</option>\n                                        <option value=\"1\">Option #1</option>\n                                        <option value=\"2\">Option #2</option>\n                                        <option value=\"3\">Option #3</option>\n                                      </select>\n                                    </div>\n                                </div>\n                                <div class=\"form-grup row\">\n                                    <div class=\"col-sm-12\">\n                                        <button class=\"btn btn-info\" disabled>Load Data</button>\n                                    </div>\n\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-sm-3\">\n                    <div class=\"card\">\n                        <div class=\"card-body\">\n                            <div class=\"form-group row\">\n                                <div class=\"col-sm-12\">\n                                    <label class=\"radio-inline\" for=\"inline-radio1\">\n                                    <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios1\" value=\"SelectTime\"> Select Time\n                                     </label>\n                                </div>\n                                <label class=\"col-sm-6 col-form-label\" for=\"disabled-input\">Class Time From</label>\n                                <div class=\"col-sm-6\">\n                                    <input type=\"time\" id=\"disabled-input\" name=\"disabled-input\" class=\"form-control\" placeholder=\"\">\n                                </div>\n                                <label class=\"col-sm-6 col-form-label\" for=\"disabled-input\">Class Time To</label>\n                                <div class=\"col-sm-6\">\n                                    <input type=\"time\" id=\"disabled-input\" name=\"disabled-input\" class=\"form-control\" placeholder=\"\">\n                                </div>\n                            </div>\n                            <div class=\"form-group row\">\n                                <div class=\"col-sm-12\">\n                                    <label class=\"radio-inline\" for=\"inline-radio1\">\n                                    <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios1\" value=\"SelectTime\"> Select Slot Code\n                                     </label>\n                                </div>\n                                <label class=\"col-sm-6 col-form-label\" for=\"disabled-input\">Slot Code</label>\n                                <div class=\"col-sm-6\">\n                                    <select id=\"select\" name=\"select\" class=\"form-control\">\n                                    <option value=\"0\">Please select</option>\n                                    <option value=\"1\">Option #1</option>\n                                    <option value=\"2\">Option #2</option>\n                                    <option value=\"3\">Option #3</option>\n                                  </select>\n                                </div>\n                            </div>\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/student-attendance-extraclass/student-attendance-extraclass.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/student-attendance-extraclass/student-attendance-extraclass.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentAttendanceExtraclassComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StudentAttendanceExtraclassComponent = (function () {
    function StudentAttendanceExtraclassComponent() {
    }
    StudentAttendanceExtraclassComponent.prototype.ngOnInit = function () {
    };
    StudentAttendanceExtraclassComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-student-attendance-extraclass',
            template: __webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-extraclass/student-attendance-extraclass.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-extraclass/student-attendance-extraclass.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StudentAttendanceExtraclassComponent);
    return StudentAttendanceExtraclassComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/academic/student-attendance-muster/student-attendance-muster.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Student Class Attendance Sheet</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"form-group row\">\n                <label class=\"col-md-3 col-form-label\" for=\"select\">Semester Code</label>\n                <div class=\"col-md-3\">\n                    <select id=\"select\" name=\"select\" class=\"form-control\">\n                    <option value=\"0\">Please select</option>\n                    <option value=\"1\">Option #1</option>\n                    <option value=\"2\">Option #2</option>\n                    <option value=\"3\">Option #3</option>\n                  </select>\n                </div>\n                <label class=\"col-md-3 col-form-label\" for=\"disabled-input\">Faculty<strong style=\"color:red\">*</strong></label>\n                <div class=\"col-md-3\">\n                    <input type=\"text\" id=\"disabled-input\" name=\"disabled-input\" class=\"form-control\" placeholder=\"\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-md-3 col-form-label\" for=\"disabled-input\">Department</label>\n                <div class=\"col-md-3\">\n                    <input type=\"text\" id=\"disabled-input\" name=\"disabled-input\" class=\"form-control\" placeholder=\"\">\n                </div>\n                <label class=\"col-md-3 col-form-label\" for=\"disabled-input\">Designation</label>\n                <div class=\"col-md-3\">\n                    <input type=\"text\" id=\"disabled-input\" name=\"disabled-input\" class=\"form-control\" placeholder=\"\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-md-3 col-form-label\" for=\"disabled-input\">Select Code</label>\n                <div class=\"col-md-3\">\n                    <select id=\"select\" name=\"select\" class=\"form-control\">\n                    <option value=\"0\">Please select</option>\n                    <option value=\"1\">Option #1</option>\n                    <option value=\"2\">Option #2</option>\n                    <option value=\"3\">Option #3</option>\n                  </select>\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-md-3 col-form-label\" for=\"disabled-input\">Subject Component</label>\n                <div class=\"col-md-3\">\n                    <select id=\"select\" name=\"select\" class=\"form-control\">\n                    <option value=\"0\">Please select</option>\n                    <option value=\"1\">Option #1</option>\n                    <option value=\"2\">Option #2</option>\n                    <option value=\"3\">Option #3</option>\n                  </select>\n                </div>\n                <label class=\"col-md-3 col-form-label\" for=\"disabled-input\">Branch</label>\n                <div class=\"col-md-3\">\n                    <select id=\"select\" name=\"select\" class=\"form-control\">\n                    <option value=\"0\">Please select</option>\n                    <option value=\"1\">Option #1</option>\n                    <option value=\"2\">Option #2</option>\n                    <option value=\"3\">Option #3</option>\n                  </select>\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-md-3 col-form-label\" for=\"disabled-input\">From Date</label>\n                <div class=\"col-md-3\">\n                    <input type=\"date\" id=\"disabled-input\" name=\"disabled-input\" class=\"form-control\" placeholder=\"\">\n                </div>\n                <label class=\"col-md-3 col-form-label\" for=\"disabled-input\">To Date</label>\n                <div class=\"col-md-3\">\n                    <input type=\"date\" id=\"disabled-input\" name=\"disabled-input\" class=\"form-control\" placeholder=\"\">\n                </div>\n            </div>\n        </div>\n        <div class=\"card-footer\">\n            <button type=\"submit\" class=\"btn btn-sm btn-primary\"><i class=\"fa fa-eye\"></i> Show</button>\n            <button type=\"reset\" class=\"btn btn-sm btn-warning\"><i class=\"fa fa-ban\"></i> Reset</button>\n            <button type=\"submit\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-window-close\"></i> Close</button>\n            <button type=\"reset\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-file-excel-o\"></i> Export to Excel</button>\n        </div>\n    </div>\n\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Student Class Attendance Sheet</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"col-sm-12\" style=\"overflow:auto\">\n                <table class=\"table table-bordered table-striped table-sm\">\n                    <thead>\n                        <tr>\n                            <th>SI No.</th>\n                            <th>Registration No.</th>\n                            <th>Name Of Student</th>\n                            <th>Sub Section</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n\n                        <tr></tr>\n                    </tbody>\n                </table>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/student-attendance-muster/student-attendance-muster.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/academic/student-attendance-muster/student-attendance-muster.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentAttendanceMusterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StudentAttendanceMusterComponent = (function () {
    function StudentAttendanceMusterComponent() {
    }
    StudentAttendanceMusterComponent.prototype.ngOnInit = function () {
    };
    StudentAttendanceMusterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-student-attendance-muster',
            template: __webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-muster/student-attendance-muster.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/academic/student-attendance-muster/student-attendance-muster.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StudentAttendanceMusterComponent);
    return StudentAttendanceMusterComponent;
}());



/***/ })

});
//# sourceMappingURL=academic.module.0.chunk.js.map