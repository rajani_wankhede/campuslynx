webpackJsonp(["exam.module"],{

/***/ "../../../../../src/app/views/student/exam/exam-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__exam_schedule_exam_schedule_component__ = __webpack_require__("../../../../../src/app/views/student/exam/exam-schedule/exam-schedule.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__internal_exam_marks_internal_exam_marks_component__ = __webpack_require__("../../../../../src/app/views/student/exam/internal-exam-marks/internal-exam-marks.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__result_result_component__ = __webpack_require__("../../../../../src/app/views/student/exam/result/result.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '',
        data: {
            title: 'Exam'
        },
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_2__exam_schedule_exam_schedule_component__["a" /* ExamScheduleComponent */],
                data: {
                    title: 'Examination Schedule'
                }
            },
            {
                path: 'internalExamEventMarks',
                component: __WEBPACK_IMPORTED_MODULE_3__internal_exam_marks_internal_exam_marks_component__["a" /* InternalExamMarksComponent */],
                data: {
                    title: 'Internal Exam Event Wise Marks'
                }
            },
            {
                path: 'result',
                component: __WEBPACK_IMPORTED_MODULE_4__result_result_component__["a" /* ResultComponent */],
                data: {
                    title: 'Student Result SGPA/CGPA'
                }
            }
        ]
    }
];
var ExamRoutingModule = (function () {
    function ExamRoutingModule() {
    }
    ExamRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], ExamRoutingModule);
    return ExamRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/exam/exam-schedule/exam-schedule.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>My Examination Schedule</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Semester Code*:</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control detailsearch\" placeholder=\"..\" (dblclick)=\"largeModal.show()\">\n                                            </div>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\"> Registration Number*:</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control detailsearch\" placeholder=\"..\" (dblclick)=\"largeModal1.show()\">\n                                            </div>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\"> Exam Event:</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control detailsearch\" placeholder=\"..\" (dblclick)=\"largeModal2.show()\">\n                                            </div>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\"> Exam Type*:</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Name\">\n                                            </div>\n                                            <div class=\"col-md-4\">\n                                                <button class=\"btn-primary\"> ok</button>\n                                                <button class=\"btn-danger\"> block</button>\n                                                <button class=\"btn-danger\"> close</button>\n                                            </div>\n                                        </div>\n\n                                        <div class=\"row\">\n                                            <div class=\"col-lg-12\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-header\">\n                                                    </div>\n                                                    <div class=\"card-body\">\n                                                        <table class=\"table table-bordered table-striped table-sm\">\n                                                            <thead>\n                                                                <tr>\n                                                                    <th>SL.No</th>\n                                                                    <th>date</th>\n                                                                    <th>Time(From-To) </th>\n                                                                    <th>Subject</th>\n                                                                    <th>Exam Center</th>\n                                                                    <th>room Number</th>\n                                                                    <th>Seat No</th>\n                                                                </tr>\n                                                            </thead>\n                                                            <tbody>\n                                                            </tbody>\n                                                        </table>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <!--/.col-->\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>\n<!-- /.modal  -->\n<div bsModal #largeModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Semester Code</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n              <option>All</option>\n              <option>SI No</option>\n              <option>Registration code</option>\n              <option>Registrationdesc</option>\n\n            </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\">\n            <strong>0 Records of 0</strong>\n          </span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>Registration code</th>\n                                <th>Registrationdesc</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal1 -->\n<div bsModal #largeModal1=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Registration Number</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal1.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n              <option>All</option>\n              <option>SI No</option>\n              <option>EnrollmentNo</option>\n              <option>Name</option>\n\n            </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\">\n            <strong>0 Records of 0</strong>\n          </span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>EnrollmentNo</th>\n                                <th>Name</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal1.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal1 -->\n<div bsModal #largeModal2=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Exam Event</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal2.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n              <option>All</option>\n              <option>SI No</option>\n              <option>ExamEventcode</option>\n              <option>ExamEventDesc</option>\n\n            </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\">\n            <strong>0 Records of 0</strong>\n          </span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>ExamEventcode</th>\n                                <th>ExamEventDesc</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal2.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/exam/exam-schedule/exam-schedule.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/exam/exam-schedule/exam-schedule.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamScheduleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ExamScheduleComponent = (function () {
    function ExamScheduleComponent() {
    }
    ExamScheduleComponent.prototype.ngOnInit = function () {
    };
    ExamScheduleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-exam-schedule',
            template: __webpack_require__("../../../../../src/app/views/student/exam/exam-schedule/exam-schedule.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/exam/exam-schedule/exam-schedule.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ExamScheduleComponent);
    return ExamScheduleComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/exam/exam.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  exam works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/student/exam/exam.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/exam/exam.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ExamComponent = (function () {
    function ExamComponent() {
    }
    ExamComponent.prototype.ngOnInit = function () {
    };
    ExamComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-exam',
            template: __webpack_require__("../../../../../src/app/views/student/exam/exam.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/exam/exam.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ExamComponent);
    return ExamComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/exam/exam.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExamModule", function() { return ExamModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__exam_component__ = __webpack_require__("../../../../../src/app/views/student/exam/exam.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__exam_schedule_exam_schedule_component__ = __webpack_require__("../../../../../src/app/views/student/exam/exam-schedule/exam-schedule.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__internal_exam_marks_internal_exam_marks_component__ = __webpack_require__("../../../../../src/app/views/student/exam/internal-exam-marks/internal-exam-marks.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_tabs__ = __webpack_require__("../../../../ngx-bootstrap/tabs/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__exam_routing_module__ = __webpack_require__("../../../../../src/app/views/student/exam/exam-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_app_views_student_exam_result_result_component__ = __webpack_require__("../../../../../src/app/views/student/exam/result/result.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// Forms Component
//import { FormsComponent } from './forms.component';

// Modal Component

// Tabs Component

// Components Routing


var ExamModule = (function () {
    function ExamModule() {
    }
    ExamModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_7__exam_routing_module__["a" /* ExamRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap_modal__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_tabs__["a" /* TabsModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__exam_component__["a" /* ExamComponent */],
                __WEBPACK_IMPORTED_MODULE_3__internal_exam_marks_internal_exam_marks_component__["a" /* InternalExamMarksComponent */],
                __WEBPACK_IMPORTED_MODULE_8_app_views_student_exam_result_result_component__["a" /* ResultComponent */],
                __WEBPACK_IMPORTED_MODULE_2__exam_schedule_exam_schedule_component__["a" /* ExamScheduleComponent */]
            ]
        })
    ], ExamModule);
    return ExamModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/exam/internal-exam-marks/internal-exam-marks.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>My Examination Schedule</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">  Enrollment No.*:</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Name\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\"> Name *:</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"branch\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Semester Code :*</label>\n                                            <div class=\"col-md-6\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Select SRC For\">\n                                            </div>\n                                            <div class=\"col-md-2\">\n                                                <button> Submit</button>\n                                            </div>\n\n                                        </div>\n                                        <div class=\"row\">\n                                            <div class=\"col-lg-12\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-header\">\n                                                        <i class=\"fa fa-align-justify\"></i>Exam-Schedule\n                                                    </div>\n                                                    <div class=\"card-body\">\n                                                        <table class=\"table table-bordered table-striped table-sm\">\n                                                            <thead>\n                                                                <tr>\n                                                                    <th>SL.No</th>\n                                                                    <th>Subject Code</th>\n                                                                    <th>Subject Code Desc </th>\n                                                                    <th>Event Code</th>\n                                                                    <th>Total Marks</th>\n                                                                    <th>Passing Marks</th>\n                                                                    <th>Marks Obtained</th>\n                                                                </tr>\n                                                            </thead>\n                                                            <tbody>\n                                                            </tbody>\n                                                        </table>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <!--/.col-->\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/exam/internal-exam-marks/internal-exam-marks.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/exam/internal-exam-marks/internal-exam-marks.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InternalExamMarksComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InternalExamMarksComponent = (function () {
    function InternalExamMarksComponent() {
    }
    InternalExamMarksComponent.prototype.ngOnInit = function () {
    };
    InternalExamMarksComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-internal-exam-marks',
            template: __webpack_require__("../../../../../src/app/views/student/exam/internal-exam-marks/internal-exam-marks.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/exam/internal-exam-marks/internal-exam-marks.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InternalExamMarksComponent);
    return InternalExamMarksComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/exam/result/result.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Student's Result</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">  Roll/Exam No.*:</label>\n                                            <div class=\"col-md-4\">\n                                                <label class=\"col-md-8 col-form-label\">1234</label> </div>\n                                            <label class=\"col-md-2 col-form-label\">Student Name *:</label>\n                                            <div class=\"col-md-4\">\n                                                <label class=\"col-md-8 col-form-label\"> Aditi</label> </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">STY Number :*</label>\n                                            <div class=\"col-md-6\">\n                                                <label class=\"col-md-8 col-form-label\">123456</label> </div>\n                                            <div class=\"col-md-2\">\n                                                <button class=\"btn btn-danger\"> Close</button>\n                                            </div>\n\n                                        </div>\n                                        <div class=\"row\">\n                                            <div class=\"col-lg-8\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-body\">\n                                                        <table class=\"table table-bordered table-striped table-sm\">\n                                                            <thead>\n                                                                <tr>\n                                                                    <th>STY Number</th>\n                                                                    <th>TOTAL COURSE CREDITS</th>\n                                                                    <th>CREDITS EARNED</th>\n                                                                    <th>PROGRESSIVE CREDITS EARNED </th>\n                                                                    <th>GRADE POINTS EARNED</th>\n                                                                    <th>PROGRESSIVE GRADE POINTS EARNED</th>\n                                                                    <th>SPI</th>\n                                                                    <th>PPI/CPI</th>\n                                                                </tr>\n                                                            </thead>\n                                                            <tbody>\n\n                                                            </tbody>\n                                                        </table>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <!--/.col-->\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Total Course Credits :*</label>\n                                            <div class=\"col-md-4\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\">Total Earned Credits :*</label>\n                                            <div class=\"col-md-4\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">SPI:*</label>\n                                            <div class=\"col-md-4\">\n                                                <label class=\"col-md-8 col-form-label\">Semester performance index</label>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">PPI:*</label>\n                                            <div class=\"col-md-4\">\n                                                <label class=\"col-md-8 col-form-label\">Progressive Performance index</label>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">CPI:*</label>\n                                            <div class=\"col-md-4\">\n                                                <label class=\"col-md-8 col-form-label\">Cumulative  Performance index</label>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Disclaimer:*</label>\n                                            <div class=\"col-md-7\">\n                                                <label class=\"col-md-8 col-form-label\">The above information is electronically generated has no value ,</label>\n\n                                                <label class=\"col-md-8 col-form-label\">However the same be got confirmed from the offical documents issued by the univercity</label>\n                                            </div>\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/exam/result/result.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/exam/result/result.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ResultComponent = (function () {
    function ResultComponent() {
    }
    ResultComponent.prototype.ngOnInit = function () {
    };
    ResultComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-result',
            template: __webpack_require__("../../../../../src/app/views/student/exam/result/result.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/exam/result/result.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ResultComponent);
    return ResultComponent;
}());



/***/ })

});
//# sourceMappingURL=exam.module.chunk.js.map