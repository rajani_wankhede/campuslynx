webpackJsonp(["personal.module"],{

/***/ "../../../../../src/app/views/employee/personal/edit-contact-info/edit-contact-info.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Employee Contact information</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Peramnent Cell/Mobile</label>\n                <div class=\"col-sm-4\">\n                    <span>:</span>\n                    <span>3232</span>\n                    <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"6731\"> -->\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Peramnent Cell/Mobile </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"3232\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Correspondance Cell/Mobile</label>\n                <div class=\"col-sm-4\">\n                    <span>:</span>\n                    <span>987654321</span>\n                    <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"EXAMINATION SECTION\"> -->\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Correspondance Cell/Mobile </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"987654321\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Peramnent Phone</label>\n                <div class=\"col-sm-4\">\n                    <span>:</span>\n                    <span>3232</span>\n                    <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"6731\"> -->\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Peramnent Phone </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"3232\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Correspondance Phone</label>\n                <div class=\"col-sm-4\">\n                    <span>:</span>\n                    <span>987654321</span>\n                    <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"EXAMINATION SECTION\"> -->\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Correspondance Phone </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"987654321\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Email</label>\n                <div class=\"col-sm-4\">\n                    <span>:</span>\n                    <span>sharma@nirmauni.ac.in</span>\n                    <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"EXAMINATION SECTION\"> -->\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Email </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"email\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"sharma@nirmauni.ac.in\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Date of Birth</label>\n                <div class=\"col-sm-4\">\n                    <span>:</span>\n                    <span>02/11/1992</span>\n                    <!-- <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\"> -->\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\"> Date of Birth </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" value=\"1992-11-02\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Date of Joining</label>\n                <div class=\"col-sm-4\">\n                    <span>:</span>\n                    <span>02/11/2012</span>\n                    <!-- <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\"> -->\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\"> Date of Joining </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" value=\"2012-11-02\">\n                </div>\n            </div>\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <i class=\"fa fa-align-justify\"></i> <strong>Correspondance Address</strong>\n                </div>\n                <div class=\"card-body\">\n                    <!-- <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <label><strong><u>Correspondance Address</u></strong></label>\n                        </div>\n                    </div> -->\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 1</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span>Kerala</span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 1</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Kerala\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 2</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span>Kerala</span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 2</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Kerala\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 3</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span>Kerala</span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 3</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Kerala\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">District</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span></span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">District</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">City</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span></span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">City</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">PIN</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span></span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">PIN</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">State</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span></span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">State</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <i class=\"fa fa-align-justify\"></i> <strong>Permanent Address</strong>\n                </div>\n                <div class=\"card-body\">\n                    <!-- <div class=\"row\">\n                <div class=\"col-sm-12\">\n                    <label><strong><u>Permanent Address</u></strong></label>\n                </div>\n            </div> -->\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 1</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span>Kerala</span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 1</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Kerala\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 2</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span>Kerala</span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 2</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Kerala\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 3</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span>Kerala</span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">Address 3</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Kerala\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">District</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span></span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">District</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">City</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span></span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">City</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">PIN</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span></span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">PIN</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">State</label>\n                        <div class=\"col-md-4\">\n                            <span>:</span>\n                            <span></span>\n                            <!-- <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\"> -->\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"email-input\">State</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                            <!-- <span class=\"help-block\">Please enter your email</span> -->\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <div class=\"col-sm-12\">\n                    <button type=\"submit\" style=\"float:right\" class=\"btn btn-lg btn-primary\"> Save</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/edit-contact-info/edit-contact-info.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/edit-contact-info/edit-contact-info.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditContactInfoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EditContactInfoComponent = (function () {
    function EditContactInfoComponent() {
    }
    EditContactInfoComponent.prototype.ngOnInit = function () {
    };
    EditContactInfoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit-contact-info',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/edit-contact-info/edit-contact-info.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/edit-contact-info/edit-contact-info.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EditContactInfoComponent);
    return EditContactInfoComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/facultly-info-updation/facultly-info-updation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <!--/.row-->\n    <div class=\"row\" *ngIf=\"!editmode\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <i class=\"fa fa-align-justify\"></i> Facaulty Information\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"col-sm-12\" style=\"overflow:auto\">\n                        <table class=\"table table-bordered table-striped table-sm\">\n                            <thead>\n                                <tr>\n                                    <th>SI. NO.</th>\n                                    <th>Employee Code</th>\n                                    <th>Shortname</th>\n                                    <th>Employee Name</th>\n                                    <th>Date of birth</th>\n                                    <th>Father's Name</th>\n                                    <th>Mother's Name</th>\n                                    <th>Husband Name</th>\n                                    <th>Date of joining</th>\n                                    <th>Remarks</th>\n                                    <th>Action</th>\n                                </tr>\n                            </thead>\n                            <tbody>\n                                <tr (dblclick)=\"editmode=!editmode\">\n                                    <td>1</td>\n                                    <td>6731</td>\n                                    <td>SHNM114</td>\n                                    <td>Shanu Sharma</td>\n                                    <td>02/10/1978</td>\n                                    <td>Patel Jagdish</td>\n                                    <td>Kamla</td>\n                                    <td>..</td>\n                                    <td>03/11/2016</td>\n                                    <td>\n                                        <span class=\"badge badge-success\">Active</span>\n                                    </td>\n                                    <td><i class=\"icon-trash icons\"></i></td>\n                            </tbody>\n                        </table>\n                    </div>\n                    <nav>\n                        <ul class=\"pagination\">\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Prev</a></li>\n                            <li class=\"page-item active\">\n                                <a class=\"page-link\" href=\"#\">1</a>\n                            </li>\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">4</a></li>\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\n                        </ul>\n                    </nav>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n    <!--/.row-->\n    <div class=\"row\" *ngIf=\"editmode\">\n        <div class=\"col-md-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong> Facaulty Information Updation</strong>\n                </div>\n                <div class=\"card-body\">\n                    <form class=\"form-horizontal\">\n                        <div class=\"row\">\n                            <div class=\"col-sm-8\">\n                                <div class=\"form-group row\">\n                                    <label class=\"col-md-3 col-form-label\">Company Code <span style=\"color:red\">*</span></label>\n\n                                    <div class=\"col-md-3\">\n                                        <input disabled type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"NIRU\">\n                                        <!-- <span class=\"help-block\">This is a help text</span> -->\n                                    </div>\n                                    <div class=\"col-md-2\">\n                                        <p class=\"form-control-static\">Nirma Univarsity</p>\n                                    </div>\n\n                                </div>\n                                <div class=\"form-group row\">\n                                    <label class=\"col-md-3 col-form-label\" for=\"text-input\">Employee Code<span style=\"color:red\">*</span></label>\n                                    <div class=\"col-md-3\">\n                                        <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"6731\" disabled>\n                                        <!-- <span class=\"help-block\">This is a help text</span> -->\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-sm-4\">\n                                <div class=\"form-froup row\">\n                                    <div style=\"float:right; width:150px;height:150px;border:1px solid #c2cfd6;margin-left:150px;margin-bottom:5px\"></div>\n                                    <div class=\"row\">\n                                        <label class=\"col-sm-4 col-form-label\" for=\"file-input\">Upload Photo</label>\n                                        <div class=\"col-sm-4\">\n                                            <input type=\"file\" id=\"file-input\" name=\"file-input\">\n                                        </div>\n                                    </div>\n\n                                </div>\n\n                            </div>\n                        </div>\n\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Employee Name<span style=\"color:red\">*</span></label>\n                            <div class=\"col-md-1\">\n                                <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Name\">\n                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                            </div>\n                            <label disabled class=\"col-md-1 col-form-label\" for=\"email-input\">First Name<span style=\"color:red\">*</span></label>\n                            <div class=\"col-md-2\">\n                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Shanu\">\n                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                            </div>\n                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Middle Name</label>\n                            <div class=\"col-md-2\">\n                                <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Sharma\">\n                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                            </div>\n                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Last Name</label>\n                            <div class=\"col-md-2\">\n                                <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-3 col-form-label\" for=\"password-input\">Shortname</label>\n                            <div class=\"col-md-3\">\n                                <input type=\"text\" id=\"password-input\" name=\"password-input\" class=\"form-control\" placeholder=\"SHNM114\">\n                                <!-- <span class=\"help-block\">Please enter a complex password</span> -->\n                            </div>\n                            <label class=\"col-md-3 col-form-label\" for=\"password-input\">Deactive</label>\n                            <div class=\"col-md-3\">\n                                <select class=\"form-control\" disabled>\n                                        <option value=\"Yes\">\n                                          Yes\n                                        </option>\n                                        <option value=\"No\">\n                                            No\n                                          </option>\n                                    </select>\n                            </div>\n\n                        </div>\n\n                        <!-- <div class=\"form-group row\">\n                            <label class=\"col-md-3 col-form-label\" for=\"file-input\">Upload Photo</label>\n                            <div class=\"col-md-9\">\n                                <input type=\"file\" id=\"file-input\" name=\"file-input\">\n                            </div>\n                        </div> -->\n                        <div class=\"row\">\n                            <div class=\"col-md-12 mb-12\">\n                                <!-- Nav tabs -->\n                                <tabset>\n                                    <tab heading=\"[P]ersonal details\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Date of Birth<span style=\"color:red\">*</span></label>\n                                            <div class=\"col-md-2\">\n                                                <input disabled type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" value=\"02-11-1992\" placeholder=\"02/11/1992\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Gender</label>\n                                            <select class=\"col-md-2 form-control\">\n                                              <option>Male</option>\n                                                <option>Female</option>\n                                          </select>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Per.File no.(If any)</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Father Name</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Mother Name</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Married</label>\n\n                                            <div class=\"col-md-4\">\n                                                <label class=\"radio-inline\" for=\"inline-radio1\">\n                                                  <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios\" value=\"Yes\"> Yes\n                                                     </label>\n                                                <label class=\"radio-inline\" for=\"inline-radio2\">\n                                                       <input type=\"radio\" id=\"inline-radio2\" name=\"inline-radios\" value=\"No\"> No\n                                                        </label>\n\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Marriage date</label>\n                                            <div class=\"col-md-4\">\n                                                <input disabled type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Spouse Name</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Blood Group</label>\n                                            <select class=\"col-md-4 form-control\">\n                                              <option>A+</option>\n                                              <option>A-</option>\n                                              <option>B+</option>\n                                              <option>B-</option>\n                                              <option>AB+</option>\n                                              <option>AB-</option>\n                                              <option>O+</option>\n                                              <option>O-</option>\n                                            </select>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Religion</label>\n                                            <select class=\"col-md-2 form-control\">\n                                              <option>Hindu</option>\n                                              <option>Muslim</option>\n                                              <option>Shikh</option>\n                                              <option>Christian</option>\n                                            </select>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Nationality</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Addhar Number</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n\n                                        </div>\n                                        <div class=\"row\">\n                                            <label><strong>[C]ontact Details</strong></label>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-3 col-form-label\" for=\"email-input\">Email-ID</label>\n                                            <div class=\"col-md-6\">\n                                                <input disabled type=\"email\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-3 col-form-label\" for=\"email-input\">Office Phone Numbers</label>\n                                            <div class=\"col-md-6\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-3 col-form-label\" for=\"email-input\">Ext. No.</label>\n                                            <div class=\"col-md-6\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                    </tab>\n                                    <tab heading=\"Employee [A]ddress\">\n                                        <div class=\"row\">\n                                            <label class=\"col-md-6\"><strong><u>Present Address</u></strong></label>\n                                            <label class=\"col-md-6\"><strong><u>Permanent Address</u></strong>&nbsp;  <input type=\"checkbox\">Same as Present address</label>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Address 1</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Address 1</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Address 2</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Address 2</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Address 3</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Address 3</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Country</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Country</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">State</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">State</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">City</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">City</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">District</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">District</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">PIN</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">PIN</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Phone No.</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Phone No.</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Cell No.</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Cell No.</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                    </tab>\n                                    <tab heading=\"[E]mployee Details\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Posted At.</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Posted On</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">ESI Dispensary</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">PAN No.</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Passport No.</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Passport Issued From</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Passport Issue Date</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Passport valid Upto</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Bank A/C No.</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Bank Code</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control detailsearch\" placeholder=\"..\" (dblclick)=\"largeModal.show()\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Staff Block</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Staff Room</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Staff Other Info</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <div class=\"col-md-6\">\n                                                <div class=\"checkbox\">\n                                                    <label for=\"checkbox1\">\n                                                  <input type=\"checkbox\" id=\"checkbox1\" name=\"checkbox1\" value=\"AllowLeaveEntry\"> Allow Leave Entry\n                                                </label>\n                                                </div>\n                                            </div>\n                                        </div>\n                                        <div class=\"row\">\n                                            <div class=\"col-sm-4\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-header\">\n                                                        <strong>VPF</strong>\n                                                        <small>Detail</small>\n                                                    </div>\n                                                    <div class=\"card-body\">\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-sm-6 col-form-label\" for=\"name\">VPF</label>\n                                                            <div class=\"col-sm-6\">\n                                                                <label class=\"radio-inline\" for=\"inline-radio1\">\n                                                                          <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios\" value=\"Yes\"> Yes\n                                                                        </label>\n                                                                <label class=\"radio-inline\" for=\"inline-radio2\">\n                                                                          <input type=\"radio\" id=\"inline-radio2\" name=\"inline-radios\" value=\"No\"> No\n                                                                        </label>\n                                                            </div>\n                                                        </div>\n                                                        <!--/.row-->\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-sm-6 col-form-label\" for=\"email-input\">Ammount</label>\n                                                            <div class=\"col-sm-6\">\n                                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-sm-4\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-header\">\n                                                        <strong>Bond</strong>\n                                                        <small>Detail</small>\n                                                    </div>\n                                                    <div class=\"card-body\">\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-sm-6 col-form-label\" for=\"name\">Bond</label>\n                                                            <div class=\"col-sm-6\">\n                                                                <label class=\"radio-inline\" for=\"inline-radio1\">\n                                                                          <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios\" value=\"Yes\"> Yes\n                                                                        </label>\n                                                                <label class=\"radio-inline\" for=\"inline-radio2\">\n                                                                          <input type=\"radio\" id=\"inline-radio2\" name=\"inline-radios\" value=\"No\"> No\n                                                                        </label>\n                                                            </div>\n                                                        </div>\n                                                        <!--/.row-->\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-sm-6 col-form-label\" for=\"email-input\">Duration(in Months)</label>\n                                                            <div class=\"col-sm-6\">\n                                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-sm-4\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-header\">\n                                                        <strong>ESI</strong>\n                                                        <small>Detail</small>\n                                                    </div>\n                                                    <div class=\"card-body\">\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-sm-6 col-form-label\" for=\"name\">ESI</label>\n                                                            <div class=\"col-sm-6\">\n                                                                <label class=\"radio-inline\" for=\"inline-radio1\">\n                                                                          <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios\" value=\"Yes\"> Yes\n                                                                        </label>\n                                                                <label class=\"radio-inline\" for=\"inline-radio2\">\n                                                                          <input type=\"radio\" id=\"inline-radio2\" name=\"inline-radios\" value=\"No\"> No\n                                                                        </label>\n                                                            </div>\n                                                        </div>\n                                                        <!--/.row-->\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-sm-6 col-form-label\" for=\"email-input\">ESI No.</label>\n                                                            <div class=\"col-sm-6\">\n                                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n\n                                        </div>\n                                        <div class=\"row\">\n                                            <div class=\"col-sm-4\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-header\">\n                                                        <strong>PF</strong>\n                                                        <small>Detail</small>\n                                                    </div>\n                                                    <div class=\"card-body\">\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-sm-6 col-form-label\" for=\"name\">PF</label>\n                                                            <div class=\"col-sm-6\">\n                                                                <label class=\"radio-inline\" for=\"inline-radio1\">\n                                                                          <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios\" value=\"Yes\"> Yes\n                                                                        </label>\n                                                                <label class=\"radio-inline\" for=\"inline-radio2\">\n                                                                          <input type=\"radio\" id=\"inline-radio2\" name=\"inline-radios\" value=\"No\"> No\n                                                                        </label>\n                                                            </div>\n                                                        </div>\n                                                        <!--/.row-->\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-sm-6 col-form-label\" for=\"email-input\">PF No.</label>\n                                                            <div class=\"col-sm-6\">\n                                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                                <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-sm-4\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-header\">\n                                                        <strong>Overtime</strong>\n                                                        <small>Detail</small>\n                                                    </div>\n                                                    <div class=\"card-body\">\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-sm-6 col-form-label\" for=\"name\">Overtime</label>\n                                                            <div class=\"col-sm-6\">\n                                                                <label class=\"radio-inline\" for=\"inline-radio1\">\n                                                                          <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios\" value=\"Yes\"> Yes\n                                                                        </label>\n                                                                <label class=\"radio-inline\" for=\"inline-radio2\">\n                                                                          <input type=\"radio\" id=\"inline-radio2\" name=\"inline-radios\" value=\"No\"> No\n                                                                        </label>\n                                                            </div>\n                                                        </div>\n\n                                                    </div>\n                                                </div>\n                                            </div>\n\n                                        </div>\n                                    </tab>\n                                    <tab heading=\"[P]rofesional Details\">\n                                        <div class=\"row\">\n                                            <div class=\"col-sm-6\">\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Grade</label>\n                                                    <div class=\"col-sm-6\">\n                                                        <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                                                        <!-- <span class=\"help-block\">Please enter your email</span> -->\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Emp. type</label>\n                                                    <div class=\"col-sm-6\">\n                                                        <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"TECH\">\n                                                        <span class=\"help-block\">TEACHING EMPLOYEES</span>\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Category<strong style=\"color:red\">*</strong></label>\n                                                    <div class=\"col-sm-6\">\n                                                        <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"GEN\">\n                                                        <span class=\"help-block\">GENERAL</span>\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Designation</label>\n                                                    <div class=\"col-sm-6\">\n                                                        <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"JROO1\">\n                                                        <span class=\"help-block\">JROO1</span>\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Department</label>\n                                                    <div class=\"col-sm-6\">\n                                                        <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"EXAM\">\n                                                        <span class=\"help-block\">EXAMINATION SECTION</span>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-sm-6\">\n                                                <div class=\"col-sm-12\">\n                                                    <div class=\"card\">\n                                                        <div class=\"card-header\">\n                                                            <strong>Transfer</strong>\n                                                            <small>Detail</small>\n                                                        </div>\n                                                        <div class=\"card-body\">\n                                                            <div class=\"form-group row\">\n                                                                <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Transfer from</label>\n                                                                <div class=\"col-sm-9\">\n                                                                    <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"form-group row\">\n                                                                <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Transfer To</label>\n                                                                <div class=\"col-sm-9\">\n                                                                    <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"form-group row\">\n                                                                <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Date of Trans. In</label>\n                                                                <div class=\"col-sm-9\">\n                                                                    <input disabled type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                                <div class=\"col-sm-12\">\n                                                    <div class=\"form-group row\">\n                                                        <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Date of Joining<strong style=\"color:red\">*</strong></label>\n                                                        <div class=\"col-sm-9\">\n                                                            <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\" disabled>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                                <div class=\"col-sm-12\">\n                                                    <div class=\"card\">\n                                                        <div class=\"card-header\">\n                                                            <strong>Stream and Qualification Category</strong>\n                                                            <small>Detail</small>\n                                                        </div>\n                                                        <div class=\"card-body\">\n                                                            <div class=\"form-group row\">\n                                                                <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Stream</label>\n                                                                <div class=\"col-sm-9\">\n                                                                    <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"form-group row\">\n                                                                <label class=\"col-sm-3 col-form-label\" for=\"email-input\">Quali. Category</label>\n                                                                <div class=\"col-sm-9\">\n                                                                    <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n\n                                    </tab>\n                                    <tab heading=\"[E]xperience\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Name of Organization</label>\n                                            <div class=\"col-sm-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Field Experience</label>\n                                            <div class=\"col-sm-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Post Held</label>\n                                            <div class=\"col-sm-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Nature of Job</label>\n                                            <div class=\"col-sm-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Date From</label>\n                                            <div class=\"col-sm-4\">\n                                                <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Date To</label>\n                                            <div class=\"col-sm-4\">\n                                                <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Gross Pay</label>\n                                            <div class=\"col-sm-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\"> Experience(Months)</label>\n                                            <div class=\"col-sm-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Remarks</label>\n                                            <div class=\"col-sm-6\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <div class=\"controls col-sm-6\">\n                                                <div class=\"input-group\">\n                                                    <input id=\"appendedInputButtons\" class=\"form-control\" type=\"text\">\n                                                    <span class=\"input-group-btn\">\n                                                    <button class=\"btn btn-secondary\" type=\"button\">Search</button>\n\n                                                  </span>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-sm-6\">\n                                                <button type=\"submit\" class=\"btn btn-sm btn-primary\"><i class=\"fa fa-dot-circle-o\"></i> Validate</button>\n                                                <button type=\"reset\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\n                                                <button class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Delete</button>\n                                            </div>\n                                        </div>\n                                        <div class=\"row\">\n                                            <div class=\"col-lg-12\" style=\"overflow:auto\">\n\n                                                <table class=\"table table-bordered table-striped table-sm\">\n                                                    <thead>\n                                                        <tr>\n                                                            <th>SI. NO.</th>\n                                                            <th>Preveious Organization</th>\n                                                            <th>Post Held</th>\n                                                            <th>From Date</th>\n                                                            <th>To Date</th>\n                                                            <th>Months</th>\n                                                            <th>Field Experience</th>\n                                                            <th>Nature of Job</th>\n                                                            <th>Gross Pay</th>\n                                                            <th>Remarks</th>\n\n                                                        </tr>\n                                                    </thead>\n                                                    <tbody>\n\n                                                        <tr></tr>\n                                                    </tbody>\n                                                </table>\n\n                                            </div>\n                                        </div>\n                                    </tab>\n                                    <tab heading=\"[Q]ualification\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Qualification</label>\n                                            <div class=\"col-sm-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control detailsearch\" placeholder=\"...\">\n                                            </div>\n                                            <div class=\"col-sm-4\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Year of Passing</label>\n                                            <div class=\"col-sm-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Board/University</label>\n                                            <div class=\"col-sm-6\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Institute</label>\n                                            <div class=\"col-sm-6\">\n                                                <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Division</label>\n                                            <div class=\"col-sm-2 \">\n                                                <select class=\"form-control\">\n                                                            <option>1st</option>\n                                                            <option>2nd</option>\n                                                            <option>3rd</option>\n                                                          </select>\n                                            </div>\n\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Main Qualification</label>\n                                            <div class=\"col-sm-6\">\n                                                <select class=\"form-control\">\n                                                            <option>Select</option>\n                                                    <option>Yes</option>\n                                                 <option>No</option>\n                                                    </select>\n                                            </div>\n\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\"> Percentage</label>\n                                            <div class=\"col-sm-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Remarks</label>\n                                            <div class=\"col-sm-6\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                                            </div>\n\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <div class=\"controls col-sm-6\">\n                                                <div class=\"input-group\">\n                                                    <input id=\"appendedInputButtons\" class=\"form-control\" type=\"text\">\n                                                    <span class=\"input-group-btn\">\n                                                            <button class=\"btn btn-secondary\" type=\"button\">Search</button>\n\n                                                          </span>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-sm-6\">\n                                                <button type=\"submit\" class=\"btn btn-sm btn-primary\"><i class=\"fa fa-dot-circle-o\"></i> Validate</button>\n                                                <button type=\"reset\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\n                                                <button class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Delete</button>\n                                            </div>\n                                        </div>\n                                        <div class=\"row\">\n                                            <div class=\"col-lg-12\" style=\"overflow:auto\">\n                                                <table class=\"table table-bordered table-striped table-sm\">\n                                                    <thead>\n                                                        <tr>\n                                                            <th>SI. NO.</th>\n                                                            <th>Qualification Code</th>\n                                                            <th>Qualification</th>\n                                                            <th>Board/University</th>\n                                                            <th>Institute</th>\n                                                            <th>Division</th>\n                                                            <th>Percentage</th>\n                                                            <th>Year of Passing</th>\n                                                            <th>Main Qualification</th>\n                                                            <th>Remarks</th>\n\n                                                        </tr>\n                                                    </thead>\n                                                    <tbody>\n\n                                                        <tr></tr>\n                                                    </tbody>\n                                                </table>\n                                            </div>\n                                        </div>\n                                    </tab>\n                                    <tab heading=\"[L]eaving Details\" disabled>\n\n                                    </tab>\n                                    <tab heading=\"[G]roup Details\" disabled>\n\n                                    </tab>\n                                    <tab heading=\"[E]mp\">\n\n                                    </tab>\n                                </tabset>\n                            </div>\n                        </div>\n                    </form>\n                </div>\n                <div class=\"card-footer\">\n                    <button type=\"submit\" class=\"btn btn-sm btn-primary\"><i class=\"fa fa-dot-circle-o\"></i> Submit</button>\n                    <button type=\"reset\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\n                    <button class=\"btn btn-sm btn-success\" (click)=\"editmode=!editmode\"><i class=\"fa fa-ban\"></i> back</button>\n                </div>\n            </div>\n\n        </div>\n\n    </div>\n\n    <!--/.row-->\n</div>\n\n\n<div bsModal #largeModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Select Employee Bank Master</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal.hide()\" aria-label=\"Close\">\n                <span aria-hidden=\"true\">&times;</span>\n              </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n                                <option>All</option>\n                                <option>SI No</option>\n                                <option>Bank Code</option>\n                                <option>Bank Name</option>\n                                <option>Branch</option>\n                                <option>Address1</option>\n                                <option>Address2</option>\n                                <option>Address3</option>\n                                <option>City Name</option>\n                                <option>State Name</option>\n                                <option>PIN</option>\n                         </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\"><strong>0 Records of 0</strong></span>\n                </div>\n                <div class=\"row\" style=\"overflow:auto\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>Bank Code</th>\n                                <th>Bank Name</th>\n                                <th>Branch</th>\n                                <th>Address1</th>\n                                <th>Address2</th>\n                                <th>Address3</th>\n                                <th>City Name</th>\n                                <th>State Name</th>\n                                <th>PIN</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal -->"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/facultly-info-updation/facultly-info-updation.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".detailsearch {\n  background-color: #fcfccd;\n  border: 1px solid #1f1fdf;\n  border-radius: 5px;\n  box-shadow: 4px 4px 8px 0px darkgrey; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/facultly-info-updation/facultly-info-updation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FacultlyInfoUpdationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FacultlyInfoUpdationComponent = (function () {
    function FacultlyInfoUpdationComponent() {
        this.editmode = false;
    }
    FacultlyInfoUpdationComponent.prototype.ngOnInit = function () {
    };
    FacultlyInfoUpdationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-facultly-info-updation',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/facultly-info-updation/facultly-info-updation.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/facultly-info-updation/facultly-info-updation.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FacultlyInfoUpdationComponent);
    return FacultlyInfoUpdationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/leave-query-self/leave-query-self.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  leave-query-self works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/leave-query-self/leave-query-self.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/leave-query-self/leave-query-self.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaveQuerySelfComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LeaveQuerySelfComponent = (function () {
    function LeaveQuerySelfComponent() {
    }
    LeaveQuerySelfComponent.prototype.ngOnInit = function () {
    };
    LeaveQuerySelfComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-leave-query-self',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/leave-query-self/leave-query-self.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/leave-query-self/leave-query-self.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LeaveQuerySelfComponent);
    return LeaveQuerySelfComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/my-dr-cr-advice/my-dr-cr-advice.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Debit Credit Advice</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Employee Code</label>\n                <div class=\"col-sm-4\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"6731\">\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Employee Name </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Shanu Sharma\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Department</label>\n                <div class=\"col-sm-4\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"EXAMINATION SECTION\">\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Designation </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"J100\">\n                </div>\n            </div>\n\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\"> From Date</label>\n                <div class=\"col-sm-4\">\n                    <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">To Date </label>\n                <div class=\"col-sm-4\">\n                    <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                </div>\n            </div>\n\n            <div class=\"form-group row\">\n                <div class=\"col-sm-6\" style=\"margin-left:40%\">\n                    <button type=\"submit\" class=\"btn  btn-primary\"><i class=\"fa fa-eye\"></i> Show</button>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Debit Credit Advice</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"form-group row\">\n                <div class=\"controls col-sm-8\">\n                    <div class=\"input-group\">\n                        <input id=\"appendedInputButtons\" class=\"form-control\" type=\"text\">\n                        <span class=\"input-group-btn\">\n                  <button class=\"btn btn-secondary\" type=\"button\">Search</button>\n    \n                </span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"row\">\n\n                <div class=\"col-lg-12\" style=\"overflow:auto\">\n\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI. NO.</th>\n                                <th>Advice Date</th>\n                                <th>Advice Type CR/DR</th>\n                                <th>Amount</th>\n                                <th>Voucher No/Voucher Date</th>\n                                <th>From/Description</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/my-dr-cr-advice/my-dr-cr-advice.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/my-dr-cr-advice/my-dr-cr-advice.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyDrCrAdviceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MyDrCrAdviceComponent = (function () {
    function MyDrCrAdviceComponent() {
    }
    MyDrCrAdviceComponent.prototype.ngOnInit = function () {
    };
    MyDrCrAdviceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-my-dr-cr-advice',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/my-dr-cr-advice/my-dr-cr-advice.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/my-dr-cr-advice/my-dr-cr-advice.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MyDrCrAdviceComponent);
    return MyDrCrAdviceComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/personal-info/personal-info.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Employee Personal Information</strong>\n                    <!-- <small>Taken by registrar office if any</small> -->\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-9\">\n                            <div class=\"form-group row\">\n                                <label class=\"col-md-3 col-form-label\">Name</label>\n                                <label class=\"col-md-9 col-form-label\">: Mr. Shanu Sharma</label>\n                            </div>\n                            <div class=\"form-group row\">\n                                <label class=\"col-md-3 col-form-label\">Employee Code</label>\n                                <label class=\"col-md-9 col-form-label\">: 1139</label>\n                            </div>\n                            <div class=\"form-group row\">\n                                <label class=\"col-md-3 col-form-label\">Spouse Name</label>\n                                <label class=\"col-md-9 col-form-label\">: ...</label>\n                            </div>\n                            <div class=\"form-group row\">\n                                <label class=\"col-md-3 col-form-label\">Father's Name</label>\n                                <label class=\"col-md-9 col-form-label\">: XYZ</label>\n                            </div>\n                            <div class=\"form-group row\">\n                                <label class=\"col-md-3 col-form-label\"> Mother's Name</label>\n                                <label class=\"col-md-9 col-form-label\">: ABC</label>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-3\">\n                            <div class=\"card\">\n                                <img src=\"assets/img/avatars/6.jpg\" alt=\"Paris\" width=\"100%\" height=\"100%\">\n                            </div>\n\n                        </div>\n\n                    </div>\n\n                </div>\n            </div>\n        </div>\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <!-- <div class=\"card-header\">\n                    <strong>Employee Personal Information</strong> -->\n                <!-- <small>Taken by registrar office if any</small> -->\n                <!-- </div> -->\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"form-group row\">\n                                <label class=\"col-md-2 col-form-label\"> Date of Joining</label>\n                                <label class=\"col-md-4 col-form-label\">: 11/11/2010</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Date of Birth</label>\n                                <label class=\"col-md-4 col-form-label\">: 21/02/1992</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Category</label>\n                                <label class=\"col-md-4 col-form-label\">: GEN-GENERAL</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Grade</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Department</label>\n                                <label class=\"col-md-4 col-form-label\">: CIVIL ENgg.</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Designation</label>\n                                <label class=\"col-md-4 col-form-label\">: AP001-ASISTANT PROFESSOR</label>\n\n                                <label class=\"col-md-1 col-form-label\"> Staff Block</label>\n                                <label class=\"col-md-2 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-1 col-form-label\"> Staff Room</label>\n                                <label class=\"col-md-2 col-form-label\">:...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Staff other Info.</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> PF A/C Number</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\">PAN Number</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\">Bank A/C No/Name</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> E-mail</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\">Passport Number</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Issue From</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Paaport Issue On</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Passport Valid Upto</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Cell No.(Correspondance)</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Corresponadance Phone</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Cell No.(Permanent)</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                                <label class=\"col-md-2 col-form-label\"> Permanent Phone</label>\n                                <label class=\"col-md-4 col-form-label\">: ...</label>\n\n                            </div>\n\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-sm-6\">\n                            <div class=\"card\">\n                                <div class=\"card-header\">\n                                    <strong>Correspondance Address</strong>\n\n                                </div>\n                                <div class=\"card-body\">\n                                    <div class=\"row\">\n                                        <div class=\"col-sm-12\">\n                                            <div class=\"form-group row\">\n                                                <label class=\"col-md-3 col-form-label\"> Address</label>\n                                                <label class=\"col-md-9 col-form-label\">: ...</label>\n\n                                                <label class=\"col-md-3 col-form-label\"> District</label>\n                                                <label class=\"col-md-9 col-form-label\">: ...</label>\n\n                                                <label class=\"col-md-3 col-form-label\"> City/PIN</label>\n                                                <label class=\"col-md-9 col-form-label\">: ...</label>\n\n                                                <label class=\"col-md-3 col-form-label\"> State</label>\n                                                <label class=\"col-md-9 col-form-label\">: ...</label>\n\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-6\">\n                            <div class=\"card\">\n                                <div class=\"card-header\">\n                                    <strong>Permanent Address</strong>\n\n                                </div>\n                                <div class=\"card-body\">\n                                    <div class=\"row\">\n                                        <div class=\"col-sm-12\">\n                                            <div class=\"form-group row\">\n                                                <label class=\"col-md-3 col-form-label\"> Address</label>\n                                                <label class=\"col-md-9 col-form-label\">: ...</label>\n\n                                                <label class=\"col-md-3 col-form-label\"> District</label>\n                                                <label class=\"col-md-9 col-form-label\">: ...</label>\n\n                                                <label class=\"col-md-3 col-form-label\"> City/PIN</label>\n                                                <label class=\"col-md-9 col-form-label\">: ...</label>\n\n                                                <label class=\"col-md-3 col-form-label\"> State</label>\n                                                <label class=\"col-md-9 col-form-label\">: ...</label>\n\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/personal-info/personal-info.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/personal-info/personal-info.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonalInfoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PersonalInfoComponent = (function () {
    function PersonalInfoComponent() {
    }
    PersonalInfoComponent.prototype.ngOnInit = function () {
    };
    PersonalInfoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-personal-info',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/personal-info/personal-info.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/personal-info/personal-info.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PersonalInfoComponent);
    return PersonalInfoComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/personal-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonalRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__personal_info_personal_info_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/personal-info/personal-info.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__facultly_info_updation_facultly_info_updation_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/facultly-info-updation/facultly-info-updation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__edit_contact_info_edit_contact_info_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/edit-contact-info/edit-contact-info.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__purchase_requisition_purchase_requisition_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/purchase-requisition/purchase-requisition.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__staff_file_upload_staff_file_upload_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/staff-file-upload/staff-file-upload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__staff_file_download_staff_file_download_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/staff-file-download/staff-file-download.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__self_salaryslip_self_salaryslip_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/self-salaryslip/self-salaryslip.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__self_vacationdetail_report_self_vacationdetail_report_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/self-vacationdetail-report/self-vacationdetail-report.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__leave_query_self_leave_query_self_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/leave-query-self/leave-query-self.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__my_dr_cr_advice_my_dr_cr_advice_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/my-dr-cr-advice/my-dr-cr-advice.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_app_views_employee_personal_self_tax_declarationform_self_tax_declarationform_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/self-tax-declarationform/self-tax-declarationform.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_app_views_employee_personal_self_tax_declaration_self_tax_declaration_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/self-tax-declaration/self-tax-declaration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var routes = [
    {
        path: '',
        data: {
            title: 'Personal Information'
        },
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_2__personal_info_personal_info_component__["a" /* PersonalInfoComponent */],
                data: {
                    title: 'Personal Information'
                }
            },
            {
                path: 'ViewSelfTaxDeclarationForm',
                component: __WEBPACK_IMPORTED_MODULE_12_app_views_employee_personal_self_tax_declarationform_self_tax_declarationform_component__["a" /* SelfTaxDeclarationformComponent */],
                data: {
                    title: 'View Self Tax Declaration Form'
                }
            },
            {
                path: 'FacultyInfoUpdation',
                component: __WEBPACK_IMPORTED_MODULE_3__facultly_info_updation_facultly_info_updation_component__["a" /* FacultlyInfoUpdationComponent */],
                data: {
                    title: 'Facaulty Information Updation'
                }
            },
            {
                path: 'EditContactInformation',
                component: __WEBPACK_IMPORTED_MODULE_4__edit_contact_info_edit_contact_info_component__["a" /* EditContactInfoComponent */],
                data: {
                    title: 'Edit Contact Information by Staff'
                }
            },
            {
                path: 'PurchaseRequisition',
                component: __WEBPACK_IMPORTED_MODULE_5__purchase_requisition_purchase_requisition_component__["a" /* PurchaseRequisitionComponent */],
                data: {
                    title: 'Purchase Requisition'
                }
            },
            {
                path: 'StaffFileDownload',
                component: __WEBPACK_IMPORTED_MODULE_7__staff_file_download_staff_file_download_component__["a" /* StaffFileDownloadComponent */],
                data: {
                    title: 'Staff File Download'
                }
            },
            {
                path: 'StaffFileUpload',
                component: __WEBPACK_IMPORTED_MODULE_6__staff_file_upload_staff_file_upload_component__["a" /* StaffFileUploadComponent */],
                data: {
                    title: 'Staff File Upload'
                }
            },
            {
                path: 'SelfTaxDeclaration',
                component: __WEBPACK_IMPORTED_MODULE_13_app_views_employee_personal_self_tax_declaration_self_tax_declaration_component__["a" /* SelfTaxDeclarationComponent */],
                data: {
                    title: 'Self Tax Declaration'
                }
            },
            {
                path: 'SelfSalarySlip',
                component: __WEBPACK_IMPORTED_MODULE_8__self_salaryslip_self_salaryslip_component__["a" /* SelfSalaryslipComponent */],
                data: {
                    title: 'Self Salary Slip'
                }
            },
            {
                path: 'SelfVacationDetailReport',
                component: __WEBPACK_IMPORTED_MODULE_9__self_vacationdetail_report_self_vacationdetail_report_component__["a" /* SelfVacationdetailReportComponent */],
                data: {
                    title: 'Employee Self Vacation Detail Report'
                }
            },
            {
                path: 'LeaveQueryself',
                component: __WEBPACK_IMPORTED_MODULE_10__leave_query_self_leave_query_self_component__["a" /* LeaveQuerySelfComponent */],
                data: {
                    title: 'Leave Query Self'
                }
            },
            {
                path: 'myDrCrAdvice',
                component: __WEBPACK_IMPORTED_MODULE_11__my_dr_cr_advice_my_dr_cr_advice_component__["a" /* MyDrCrAdviceComponent */],
                data: {
                    title: 'My DR/CR Advice'
                }
            },
        ]
    }
];
var PersonalRoutingModule = (function () {
    function PersonalRoutingModule() {
    }
    PersonalRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(routes), __WEBPACK_IMPORTED_MODULE_14__angular_forms__["a" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_15__angular_common__["CommonModule"]],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_14__angular_forms__["a" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_15__angular_common__["CommonModule"]]
        })
    ], PersonalRoutingModule);
    return PersonalRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/personal.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PersonalModule", function() { return PersonalModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__personal_info_personal_info_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/personal-info/personal-info.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap_tabs__ = __webpack_require__("../../../../ngx-bootstrap/tabs/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__personal_routing_module__ = __webpack_require__("../../../../../src/app/views/employee/personal/personal-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__facultly_info_updation_facultly_info_updation_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/facultly-info-updation/facultly-info-updation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__edit_contact_info_edit_contact_info_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/edit-contact-info/edit-contact-info.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__purchase_requisition_purchase_requisition_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/purchase-requisition/purchase-requisition.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__staff_file_upload_staff_file_upload_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/staff-file-upload/staff-file-upload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__staff_file_download_staff_file_download_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/staff-file-download/staff-file-download.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__self_salaryslip_self_salaryslip_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/self-salaryslip/self-salaryslip.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__self_vacationdetail_report_self_vacationdetail_report_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/self-vacationdetail-report/self-vacationdetail-report.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__leave_query_self_leave_query_self_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/leave-query-self/leave-query-self.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__my_dr_cr_advice_my_dr_cr_advice_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/my-dr-cr-advice/my-dr-cr-advice.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_app_views_employee_personal_self_tax_declarationform_self_tax_declarationform_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/self-tax-declarationform/self-tax-declarationform.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_app_views_employee_personal_self_tax_declaration_self_tax_declaration_component__ = __webpack_require__("../../../../../src/app/views/employee/personal/self-tax-declaration/self-tax-declaration.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// Forms Component
//import { FormsComponent } from './forms.component';

// Modal Component

// Tabs Component

// Components Routing












var PersonalModule = (function () {
    function PersonalModule() {
    }
    PersonalModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_5__personal_routing_module__["a" /* PersonalRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_modal__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap_tabs__["a" /* TabsModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__personal_info_personal_info_component__["a" /* PersonalInfoComponent */],
                __WEBPACK_IMPORTED_MODULE_6__facultly_info_updation_facultly_info_updation_component__["a" /* FacultlyInfoUpdationComponent */],
                __WEBPACK_IMPORTED_MODULE_7__edit_contact_info_edit_contact_info_component__["a" /* EditContactInfoComponent */],
                __WEBPACK_IMPORTED_MODULE_8__purchase_requisition_purchase_requisition_component__["a" /* PurchaseRequisitionComponent */],
                __WEBPACK_IMPORTED_MODULE_9__staff_file_upload_staff_file_upload_component__["a" /* StaffFileUploadComponent */],
                __WEBPACK_IMPORTED_MODULE_10__staff_file_download_staff_file_download_component__["a" /* StaffFileDownloadComponent */],
                __WEBPACK_IMPORTED_MODULE_11__self_salaryslip_self_salaryslip_component__["a" /* SelfSalaryslipComponent */],
                __WEBPACK_IMPORTED_MODULE_12__self_vacationdetail_report_self_vacationdetail_report_component__["a" /* SelfVacationdetailReportComponent */],
                __WEBPACK_IMPORTED_MODULE_13__leave_query_self_leave_query_self_component__["a" /* LeaveQuerySelfComponent */],
                __WEBPACK_IMPORTED_MODULE_14__my_dr_cr_advice_my_dr_cr_advice_component__["a" /* MyDrCrAdviceComponent */],
                __WEBPACK_IMPORTED_MODULE_16_app_views_employee_personal_self_tax_declaration_self_tax_declaration_component__["a" /* SelfTaxDeclarationComponent */],
                __WEBPACK_IMPORTED_MODULE_15_app_views_employee_personal_self_tax_declarationform_self_tax_declarationform_component__["a" /* SelfTaxDeclarationformComponent */]
            ]
        })
    ], PersonalModule);
    return PersonalModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/purchase-requisition/purchase-requisition.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  purchase-requisition works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/purchase-requisition/purchase-requisition.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/purchase-requisition/purchase-requisition.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PurchaseRequisitionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PurchaseRequisitionComponent = (function () {
    function PurchaseRequisitionComponent() {
    }
    PurchaseRequisitionComponent.prototype.ngOnInit = function () {
    };
    PurchaseRequisitionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-purchase-requisition',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/purchase-requisition/purchase-requisition.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/purchase-requisition/purchase-requisition.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PurchaseRequisitionComponent);
    return PurchaseRequisitionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-salaryslip/self-salaryslip.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  self-salaryslip works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-salaryslip/self-salaryslip.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-salaryslip/self-salaryslip.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelfSalaryslipComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SelfSalaryslipComponent = (function () {
    function SelfSalaryslipComponent() {
    }
    SelfSalaryslipComponent.prototype.ngOnInit = function () {
    };
    SelfSalaryslipComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-self-salaryslip',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/self-salaryslip/self-salaryslip.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/self-salaryslip/self-salaryslip.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SelfSalaryslipComponent);
    return SelfSalaryslipComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-tax-declaration/self-tax-declaration.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  self-tax-declaration works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-tax-declaration/self-tax-declaration.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-tax-declaration/self-tax-declaration.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelfTaxDeclarationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SelfTaxDeclarationComponent = (function () {
    function SelfTaxDeclarationComponent() {
    }
    SelfTaxDeclarationComponent.prototype.ngOnInit = function () {
    };
    SelfTaxDeclarationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-self-tax-declaration',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/self-tax-declaration/self-tax-declaration.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/self-tax-declaration/self-tax-declaration.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SelfTaxDeclarationComponent);
    return SelfTaxDeclarationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-tax-declarationform/self-tax-declarationform.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  self-tax-declarationform works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-tax-declarationform/self-tax-declarationform.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-tax-declarationform/self-tax-declarationform.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelfTaxDeclarationformComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SelfTaxDeclarationformComponent = (function () {
    function SelfTaxDeclarationformComponent() {
    }
    SelfTaxDeclarationformComponent.prototype.ngOnInit = function () {
    };
    SelfTaxDeclarationformComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-self-tax-declarationform',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/self-tax-declarationform/self-tax-declarationform.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/self-tax-declarationform/self-tax-declarationform.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SelfTaxDeclarationformComponent);
    return SelfTaxDeclarationformComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-vacationdetail-report/self-vacationdetail-report.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  self-vacationdetail-report works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-vacationdetail-report/self-vacationdetail-report.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/self-vacationdetail-report/self-vacationdetail-report.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelfVacationdetailReportComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SelfVacationdetailReportComponent = (function () {
    function SelfVacationdetailReportComponent() {
    }
    SelfVacationdetailReportComponent.prototype.ngOnInit = function () {
    };
    SelfVacationdetailReportComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-self-vacationdetail-report',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/self-vacationdetail-report/self-vacationdetail-report.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/self-vacationdetail-report/self-vacationdetail-report.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SelfVacationdetailReportComponent);
    return SelfVacationdetailReportComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/staff-file-download/staff-file-download.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>View/Download Uploaded File</strong>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"form-group row\">\n                <div class=\"col-sm-6\">\n                    <label class=\"radio-inline\" for=\"inline-radio1\">\n                              <input type=\"radio\" id=\"inline-radio1\" name=\"inline-radios\" value=\"Self\" (click)=\"changetype('Self')\" [checked]='true'> Self Upload File\n                            </label>\n                    <label class=\"radio-inline\" for=\"inline-radio2\">\n                              <input type=\"radio\" id=\"inline-radio2\" name=\"inline-radios\" value=\"Other\"(click)=\"changetype('Other')\" > Other Upload File\n                            </label>\n                </div>\n            </div>\n            <div class=\"form-group row\" *ngIf=\"selectedtype=='Other'?true:false\">\n\n                <label class=\"col-md-2 col-form-label\" for=\"email-input\">Staff Code</label>\n                <div class=\"col-md-2\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control detailsearch\" placeholder=\"..\" (dblclick)=\"largeModal.show()\">\n                    <!-- <span class=\"help-block\">Please enter your email</span> -->\n                </div>\n                <div class=\"col-md-4\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"..\">\n                    <!-- <span class=\"help-block\">Please enter your email</span> -->\n                </div>\n                <button class=\"btn btn-sm btn-primary\">View Details</button>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Name<strong style=\"color:red\">*</strong> </label>\n                <div class=\"col-sm-4\">\n                    <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"Shanu Sharma\">\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Code</label>\n                <div class=\"col-sm-4\">\n                    <input disabled type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"6731\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Uploading Title</label>\n                <div class=\"col-sm-4\">\n                    <select class=\"form-control\">\n                     <option>Select</option>\n                     <option>..</option>\n                   </select>\n                </div>\n\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\">Downloading From Date<strong style=\"color:red\">*</strong></label>\n                <div class=\"col-sm-4\">\n                    <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                </div>\n                <label class=\"col-sm-2 col-form-label\" for=\"email-input\"> To Date <strong style=\"color:red\">*</strong></label>\n                <div class=\"col-sm-4\">\n                    <input type=\"date\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"...\">\n                </div>\n            </div>\n\n            <div class=\"form-group row\">\n                <div class=\"controls col-sm-6\">\n                    <div class=\"input-group\">\n                        <input id=\"appendedInputButtons\" class=\"form-control\" type=\"text\">\n                        <span class=\"input-group-btn\">\n                <button class=\"btn btn-secondary\" type=\"button\">Search</button>\n\n              </span>\n                    </div>\n                </div>\n                <div class=\"col-sm-6\">\n                    <button type=\"submit\" class=\"btn btn-sm btn-primary\"><i class=\"fa fa-dot-circle-o\"></i> Validate</button>\n                    <button type=\"reset\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\n                    <button class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Delete</button>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-lg-12\" style=\"overflow:auto\">\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI. NO.</th>\n                                <th>Title</th>\n                                <th>Date and Time of Upload</th>\n                                <th>Remarks</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr></tr>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div bsModal #largeModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Select Staff</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal.hide()\" aria-label=\"Close\">\n                <span aria-hidden=\"true\">&times;</span>\n              </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n                                <option>All</option>\n                                <option>SI No</option>\n                                <option>Employee Code</option>\n                                <option>Employee Name</option>\n\n                         </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\"><strong>0 Records of 0</strong></span>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-sm-10\" style=\"overflow:auto\">\n                        <table class=\"table table-bordered table-striped table-sm\">\n                            <thead>\n                                <tr>\n                                    <th>SI No</th>\n                                    <th>Employee Code</th>\n                                    <th>Employee Name</th>\n\n                                </tr>\n                            </thead>\n                            <tbody>\n\n                                <tr></tr>\n                            </tbody>\n                        </table>\n                    </div>\n\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal -->"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/staff-file-download/staff-file-download.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".detailsearch {\n  background-color: #fcfccd;\n  border: 1px solid #1f1fdf;\n  border-radius: 5px;\n  box-shadow: 4px 4px 8px 0px darkgrey; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/staff-file-download/staff-file-download.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StaffFileDownloadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StaffFileDownloadComponent = (function () {
    function StaffFileDownloadComponent() {
        this.selectedtype = 'Self';
    }
    StaffFileDownloadComponent.prototype.ngOnInit = function () {
    };
    StaffFileDownloadComponent.prototype.changetype = function (current) {
        this.selectedtype = current;
    };
    StaffFileDownloadComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-staff-file-download',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/staff-file-download/staff-file-download.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/staff-file-download/staff-file-download.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StaffFileDownloadComponent);
    return StaffFileDownloadComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/personal/staff-file-upload/staff-file-upload.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            <i class=\"fa fa-align-justify\"></i> <strong>Staff File Upload</strong>\n\n        </div>\n        <div class=\"card-body\">\n\n\n            <div class=\"form-group row\">\n                <label class=\"col-sm-4 col-form-label\" for=\"email-input\">Topic<strong style=\"color:red\">*</strong> </label>\n                <div class=\"col-sm-6\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                </div>\n\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-4 col-form-label\" for=\"email-input\">Remarks</label>\n                <div class=\"col-sm-6\">\n                    <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <div style=\"margin-left:50%\">\n                    <button class=\"btn btn-sm btn-success\" (click)=\"FileModal.show()\"><i class=\"fa fa-paperclip\"></i> Attach File(s)</button>\n                </div>\n\n            </div>\n\n        </div>\n    </div>\n</div>\n<div class=\"card\">\n    <div class=\"card-header\">\n        <span style=\"margin-left:30%;color:rgb(92, 90, 228)\">Please Select User To Grant Access Of Attached Document(Optional)</span>\n    </div>\n    <div class=\"card-body\">\n        <div class=\"form-group row\">\n\n            <label class=\"col-sm-2 col-form-label\" for=\"email-input\">User<strong style=\"color:red\">*</strong></label>\n            <div class=\"col-sm-4\">\n                <input type=\"text\" id=\"email-input\" name=\"email-input\" (dblclick)=\"largeModal.show()\" class=\"form-control detailsearch\" placeholder=\"\">\n            </div>\n\n            <div class=\"col-sm-6\">\n                <button type=\"submit\" class=\"btn btn-sm btn-primary\"><i class=\"fa fa-dot-circle-o\"></i> Validate</button>\n                <button type=\"reset\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\n                <button class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Delete</button>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-lg-12\" style=\"overflow:auto\">\n\n                <table class=\"table table-bordered table-striped table-sm\">\n                    <thead>\n                        <tr>\n                            <th>Action</th>\n                            <th>SI. NO.</th>\n                            <th>Employee Name</th>\n                            <th>Designation</th>\n                            <th>Department</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr></tr>\n                    </tbody>\n                </table>\n\n            </div>\n        </div>\n    </div>\n    <div class=\"card-footer\">\n        <button type=\"submit\" class=\"btn btn-sm btn-primary\"><i class=\"fa fa-dot-circle-o\"></i> Submit</button>\n        <button type=\"reset\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-ban\"></i> Reset</button>\n    </div>\n</div>\n<div bsModal #largeModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Select Staff</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal.hide()\" aria-label=\"Close\">\n                <span aria-hidden=\"true\">&times;</span>\n              </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n                                <option>All</option>\n                                <option>SI No</option>\n                                <option>Employee Code</option>\n                                <option>Employee Name</option>\n\n                         </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\"><strong>0 Records of 0</strong></span>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-sm-10\" style=\"overflow:auto\">\n                        <table class=\"table table-bordered table-striped table-sm\">\n                            <thead>\n                                <tr>\n                                    <th>SI No</th>\n                                    <th>Employee Code</th>\n                                    <th>Employee Name</th>\n\n                                </tr>\n                            </thead>\n                            <tbody>\n\n                                <tr></tr>\n                            </tbody>\n                        </table>\n                    </div>\n\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal -->\n\n<div bsModal #FileModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Select Staff</h4>\n                <button type=\"button\" class=\"close\" (click)=\"FileModal.hide()\" aria-label=\"Close\">\n                <span aria-hidden=\"true\">&times;</span>\n              </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-sm-12\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-3 col-form-label\" for=\"file-input\">Attachment</label>\n                            <div class=\"col-md-9\">\n                                <input type=\"file\" id=\"file-input\" multiple name=\"file-input\">\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"FileModal.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal -->"

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/staff-file-upload/staff-file-upload.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".detailsearch {\n  background-color: #fcfccd;\n  border: 1px solid #1f1fdf;\n  border-radius: 5px;\n  box-shadow: 4px 4px 8px 0px darkgrey; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/personal/staff-file-upload/staff-file-upload.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StaffFileUploadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StaffFileUploadComponent = (function () {
    function StaffFileUploadComponent() {
    }
    StaffFileUploadComponent.prototype.ngOnInit = function () {
    };
    StaffFileUploadComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-staff-file-upload',
            template: __webpack_require__("../../../../../src/app/views/employee/personal/staff-file-upload/staff-file-upload.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/personal/staff-file-upload/staff-file-upload.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StaffFileUploadComponent);
    return StaffFileUploadComponent;
}());



/***/ })

});
//# sourceMappingURL=personal.module.chunk.js.map