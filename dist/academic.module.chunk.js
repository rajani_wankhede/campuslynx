webpackJsonp(["academic.module"],{

/***/ "../../../../../src/app/views/student/academic/academic-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcademicRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__class_attendance_class_attendance_component__ = __webpack_require__("../../../../../src/app/views/student/academic/class-attendance/class-attendance.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__class_time_table_class_time_table_component__ = __webpack_require__("../../../../../src/app/views/student/academic/class-time-table/class-time-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__course_registration_course_registration_component__ = __webpack_require__("../../../../../src/app/views/student/academic/course-registration/course-registration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__desciplinary_action_desciplinary_action_component__ = __webpack_require__("../../../../../src/app/views/student/academic/desciplinary-action/desciplinary-action.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__elective_choice_elective_choice_component__ = __webpack_require__("../../../../../src/app/views/student/academic/elective-choice/elective-choice.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__fee_detail_fee_detail_component__ = __webpack_require__("../../../../../src/app/views/student/academic/fee-detail/fee-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__information_updation_information_updation_component__ = __webpack_require__("../../../../../src/app/views/student/academic/information-updation/information-updation.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '',
        data: {
            title: 'Academic'
        },
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_6__elective_choice_elective_choice_component__["a" /* ElectiveChoiceComponent */],
                data: {
                    title: 'Student Elective Choice'
                }
            },
            {
                path: 'ClassTimetable',
                component: __WEBPACK_IMPORTED_MODULE_3__class_time_table_class_time_table_component__["a" /* ClassTimeTableComponent */],
                data: {
                    title: 'My class time Table'
                }
            },
            {
                path: 'informationUpdate',
                component: __WEBPACK_IMPORTED_MODULE_8__information_updation_information_updation_component__["a" /* InformationUpdationComponent */],
                data: {
                    title: 'Information Updation'
                }
            },
            {
                path: 'CourseRegistration',
                component: __WEBPACK_IMPORTED_MODULE_4__course_registration_course_registration_component__["a" /* CourseRegistrationComponent */],
                data: {
                    title: 'Course Registration'
                }
            },
            {
                path: 'ClassAttendance',
                component: __WEBPACK_IMPORTED_MODULE_2__class_attendance_class_attendance_component__["a" /* ClassAttendanceComponent */],
                data: {
                    title: 'Class Attendance'
                }
            },
            {
                path: 'DesciplinaryAction',
                component: __WEBPACK_IMPORTED_MODULE_5__desciplinary_action_desciplinary_action_component__["a" /* DesciplinaryActionComponent */],
                data: {
                    title: 'Desciplinary Action'
                }
            },
            {
                path: 'FeeDetails',
                component: __WEBPACK_IMPORTED_MODULE_7__fee_detail_fee_detail_component__["a" /* FeeDetailComponent */],
                data: {
                    title: 'Fee Details'
                }
            },
        ]
    }
];
var AcademicRoutingModule = (function () {
    function AcademicRoutingModule() {
    }
    AcademicRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], AcademicRoutingModule);
    return AcademicRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/academic/academic.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  academic works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/student/academic/academic.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcademicComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AcademicComponent = (function () {
    function AcademicComponent() {
    }
    AcademicComponent.prototype.ngOnInit = function () {
    };
    AcademicComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-academic',
            template: __webpack_require__("../../../../../src/app/views/student/academic/academic.component.html"),
        }),
        __metadata("design:paramtypes", [])
    ], AcademicComponent);
    return AcademicComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/academic/academic.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcademicModule", function() { return AcademicModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__academic_component__ = __webpack_require__("../../../../../src/app/views/student/academic/academic.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__class_attendance_class_attendance_component__ = __webpack_require__("../../../../../src/app/views/student/academic/class-attendance/class-attendance.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__class_time_table_class_time_table_component__ = __webpack_require__("../../../../../src/app/views/student/academic/class-time-table/class-time-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__course_registration_course_registration_component__ = __webpack_require__("../../../../../src/app/views/student/academic/course-registration/course-registration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__desciplinary_action_desciplinary_action_component__ = __webpack_require__("../../../../../src/app/views/student/academic/desciplinary-action/desciplinary-action.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__elective_choice_elective_choice_component__ = __webpack_require__("../../../../../src/app/views/student/academic/elective-choice/elective-choice.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__fee_detail_fee_detail_component__ = __webpack_require__("../../../../../src/app/views/student/academic/fee-detail/fee-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__information_updation_information_updation_component__ = __webpack_require__("../../../../../src/app/views/student/academic/information-updation/information-updation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_bootstrap_tabs__ = __webpack_require__("../../../../ngx-bootstrap/tabs/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__academic_routing_module__ = __webpack_require__("../../../../../src/app/views/student/academic/academic-routing.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









// Forms Component
//import { FormsComponent } from './forms.component';

// Modal Component

// Tabs Component

// Components Routing

var AcademicModule = (function () {
    function AcademicModule() {
    }
    AcademicModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_12__academic_routing_module__["a" /* AcademicRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_9_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_10_ngx_bootstrap_modal__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_11_ngx_bootstrap_tabs__["a" /* TabsModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__academic_component__["a" /* AcademicComponent */],
                __WEBPACK_IMPORTED_MODULE_3__class_time_table_class_time_table_component__["a" /* ClassTimeTableComponent */],
                __WEBPACK_IMPORTED_MODULE_2__class_attendance_class_attendance_component__["a" /* ClassAttendanceComponent */],
                __WEBPACK_IMPORTED_MODULE_4__course_registration_course_registration_component__["a" /* CourseRegistrationComponent */],
                __WEBPACK_IMPORTED_MODULE_5__desciplinary_action_desciplinary_action_component__["a" /* DesciplinaryActionComponent */],
                __WEBPACK_IMPORTED_MODULE_6__elective_choice_elective_choice_component__["a" /* ElectiveChoiceComponent */],
                __WEBPACK_IMPORTED_MODULE_7__fee_detail_fee_detail_component__["a" /* FeeDetailComponent */],
                __WEBPACK_IMPORTED_MODULE_8__information_updation_information_updation_component__["a" /* InformationUpdationComponent */]
            ]
        })
    ], AcademicModule);
    return AcademicModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/academic/class-attendance/class-attendance.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Student</strong>\n                    <small>Class Attendance Details</small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"company\">Name</label>\n                            <p>Firstname Lastname[11MCHC14] </p>\n                        </div>\n                        <div class=\"form-group col-md-4\">\n                            <label for=\"vat\">Course /Branch</label>\n                            <p>MTECH[MCHC]</p>\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"street\">Current Semester</label>\n                            <p>8</p>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"city\">Semester Code</label>\n                            <select class=\"form-control\" id=\"ccmonth\">\n                              <option>1</option>\n                              <option>2</option>\n                              <option>3</option>\n                              <option>4</option>\n                              <option>5</option>\n                              <option>6</option>\n                              <option>7</option>\n                              <option>8</option>\n                              <option>9</option>\n                              <option>10</option>\n                              <option>11</option>\n                              <option>12</option>\n                            </select>\n                        </div>\n\n                    </div>\n                    <!--/.row-->\n\n                    <div class=\"form-actions\">\n                        <button type=\"submit\" class=\"btn btn-success\"><i class=\"fa fa-eye \"></i> View Attendance</button>\n                        <button class=\"btn btn-danger\" type=\"button\"><i class=\"fa fa-ban\"></i> Cancel</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/academic/class-attendance/class-attendance.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/academic/class-attendance/class-attendance.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassAttendanceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ClassAttendanceComponent = (function () {
    function ClassAttendanceComponent() {
    }
    ClassAttendanceComponent.prototype.ngOnInit = function () {
    };
    ClassAttendanceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-class-attendance',
            template: __webpack_require__("../../../../../src/app/views/student/academic/class-attendance/class-attendance.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/academic/class-attendance/class-attendance.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ClassAttendanceComponent);
    return ClassAttendanceComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/academic/class-time-table/class-time-table.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Student</strong>\n                    <small>Class Timetable</small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-6\">\n                            <label for=\"company\">Registration Code</label>\n                            <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control detailsearch\" placeholder=\"..\" (dblclick)=\"largeModal.show()\">\n\n                            <!-- <input type=\"text\" class=\"form-control\" id=\"company\" placeholder=\"Enter your Registration number\"> -->\n                        </div>\n                    </div>\n                    <div class=\"form-actions\">\n                        <button type=\"submit\" class=\"btn btn-success\"><i class=\"fa fa-eye\"></i> Show</button>\n                        <button class=\"btn btn-danger\" type=\"button\"><i class=\"fa fa-ban\"></i> Cancel</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <i class=\"fa fa-align-justify\"></i> Time Table\n                </div>\n                <div class=\"card-body\">\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>Sr.No.</th>\n                                <th>Mon</th>\n                                <th>Tue</th>\n                                <th>Wed</th>\n                                <th>Thu</th>\n                                <th>Fri</th>\n                                <th>Sat</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n\n</div>\n<!-- /.modal -->\n<div bsModal #largeModal=\"bs-modal\" class=\"col-md-12\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Select Registration Master</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal.hide()\" aria-label=\"Close\">\n                                                                    <span aria-hidden=\"true\">&times;</span>\n                                                                  </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n                                  <option>All</option>\n                                  <option>SI No.</option>\n                                  <option>Registration Code</option>\n                                  <option>Registrationdesc</option>\n                                  <option>Registrationdatefrom</option>\n                                  <option>Registrationdateto</option>\n                                                                            \n                        </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\"><strong>0 Records of 0</strong></span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>Registration Code</th>\n                                <th>Registrationdesc </th>\n                                <th>Registrationdatefrom </th>\n                                <th>Registrationdateto </th>\n\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal.hide()\">Close</button>\n\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/academic/class-time-table/class-time-table.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/academic/class-time-table/class-time-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassTimeTableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ClassTimeTableComponent = (function () {
    function ClassTimeTableComponent() {
    }
    ClassTimeTableComponent.prototype.ngOnInit = function () {
    };
    ClassTimeTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-class-time-table',
            template: __webpack_require__("../../../../../src/app/views/student/academic/class-time-table/class-time-table.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/academic/class-time-table/class-time-table.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ClassTimeTableComponent);
    return ClassTimeTableComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/academic/course-registration/course-registration.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Student</strong>\n                    <small>Course Registration</small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"company\">Registration Number</label>\n                            <input type=\"text\" class=\"form-control\" id=\"company\" placeholder=\"Enter your Registration number\">\n                        </div>\n                        <div class=\"form-group col-md-4\">\n                            <label for=\"vat\">Semester Code</label>\n                            <input type=\"text\" class=\"form-control\" id=\"vat\" placeholder=\"Enter semester code\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"street\">Name</label>\n                            <input type=\"text\" class=\"form-control\" id=\"street\" placeholder=\"Enter  name\">\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"city\">Program</label>\n                            <input type=\"text\" class=\"form-control\" id=\"city\" placeholder=\"Enter program\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"postal-code\">Branch</label>\n                            <input type=\"text\" class=\"form-control\" id=\"postal-code\" placeholder=\"Branch\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"postal-code\">Section</label>\n                            <input type=\"text\" class=\"form-control\" id=\"postal-code\" placeholder=\"Section\">\n                        </div>\n                    </div>\n                    <!--/.row-->\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"city\">Academic Year</label>\n                            <input type=\"text\" class=\"form-control\" id=\"city\" placeholder=\"Year\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"postal-code\">Pre Registration For semester</label>\n                            <input type=\"text\" class=\"form-control\" id=\"postal-code\" placeholder=\"Pre Register\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"postal-code\">Institute</label>\n                            <input type=\"text\" class=\"form-control\" id=\"postal-code\" placeholder=\"Institute\">\n                        </div>\n                    </div>\n                    <div class=\"form-actions\">\n                        <button type=\"submit\" class=\"btn btn-success\"><i class=\"fa fa-dot-circle-o\"></i> Save changes</button>\n                        <button class=\"btn btn-danger\" type=\"button\"><i class=\"fa fa-ban\"></i> Cancel</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <i class=\"fa fa-align-justify\"></i> LIst of Back Paper(s)/Not Registered Paper(s)[If Any]\n                </div>\n                <div class=\"card-body\">\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>Subject Code</th>\n                                <th>STY Number</th>\n                                <th>Subject Desc</th>\n                                <th>Credit/Marks</th>\n                                <th>Status</th>\n                                <th>No. of Attemps</th>\n\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/academic/course-registration/course-registration.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/academic/course-registration/course-registration.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CourseRegistrationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CourseRegistrationComponent = (function () {
    function CourseRegistrationComponent() {
    }
    CourseRegistrationComponent.prototype.ngOnInit = function () {
    };
    CourseRegistrationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-course-registration',
            template: __webpack_require__("../../../../../src/app/views/student/academic/course-registration/course-registration.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/academic/course-registration/course-registration.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CourseRegistrationComponent);
    return CourseRegistrationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/academic/desciplinary-action/desciplinary-action.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Disciplinary Action Details</strong>\n                    <small>Taken by registrar office if any</small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-6\">\n                            <label for=\"company\">Student Name</label>\n                            <p>ABC XYZ</p>\n                        </div>\n                        <div class=\"form-group col-sm-6\">\n                            <label for=\"company\">Registration Number</label>\n                            <p>11MCHC14</p>\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <i class=\"fa fa-align-justify\"></i> Disciplinary Action Details\n                </div>\n                <div class=\"card-body\">\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>Misconduct Date</th>\n                                <th>Misconduct</th>\n                                <th>Action date</th>\n                                <th>Action Initiated</th>\n                                <th>Action Taken</th>\n\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/academic/desciplinary-action/desciplinary-action.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/academic/desciplinary-action/desciplinary-action.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DesciplinaryActionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DesciplinaryActionComponent = (function () {
    function DesciplinaryActionComponent() {
    }
    DesciplinaryActionComponent.prototype.ngOnInit = function () {
    };
    DesciplinaryActionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-desciplinary-action',
            template: __webpack_require__("../../../../../src/app/views/student/academic/desciplinary-action/desciplinary-action.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/academic/desciplinary-action/desciplinary-action.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DesciplinaryActionComponent);
    return DesciplinaryActionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/academic/elective-choice/elective-choice.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Student</strong>\n                    <small>Elective Subject Choice Preferences</small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"company\">Registration Number</label>\n                            <input type=\"text\" class=\"form-control\" id=\"company\" placeholder=\"Enter your Registration number\">\n                        </div>\n                        <div class=\"form-group col-md-4\">\n                            <label for=\"vat\">Semester Code</label>\n                            <input type=\"text\" class=\"form-control\" id=\"vat\" placeholder=\"Enter semester code\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"street\">Name</label>\n                            <input type=\"text\" class=\"form-control\" id=\"street\" placeholder=\"Enter  name\">\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"city\">Program</label>\n                            <input type=\"text\" class=\"form-control\" id=\"city\" placeholder=\"Enter program\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"postal-code\">Branch</label>\n                            <input type=\"text\" class=\"form-control\" id=\"postal-code\" placeholder=\"Branch\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"postal-code\">Section</label>\n                            <input type=\"text\" class=\"form-control\" id=\"postal-code\" placeholder=\"Section\">\n                        </div>\n                    </div>\n                    <!--/.row-->\n                    <div class=\"row\">\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"city\">Academic Year</label>\n                            <input type=\"text\" class=\"form-control\" id=\"city\" placeholder=\"Year\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"postal-code\">Pre Registration For semester</label>\n                            <input type=\"text\" class=\"form-control\" id=\"postal-code\" placeholder=\"Pre Register\">\n                        </div>\n                        <div class=\"form-group col-sm-4\">\n                            <label for=\"postal-code\">Institute</label>\n                            <input type=\"text\" class=\"form-control\" id=\"postal-code\" placeholder=\"Institute\">\n                        </div>\n                    </div>\n                    <div class=\"form-actions\">\n                        <button type=\"submit\" class=\"btn btn-success\"><i class=\"fa fa-dot-circle-o\"></i> Save changes</button>\n                        <button class=\"btn btn-danger\" type=\"button\"><i class=\"fa fa-ban\"></i> Cancel</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/academic/elective-choice/elective-choice.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/academic/elective-choice/elective-choice.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ElectiveChoiceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ElectiveChoiceComponent = (function () {
    function ElectiveChoiceComponent() {
    }
    ElectiveChoiceComponent.prototype.ngOnInit = function () {
    };
    ElectiveChoiceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-elective-choice',
            template: __webpack_require__("../../../../../src/app/views/student/academic/elective-choice/elective-choice.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/academic/elective-choice/elective-choice.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ElectiveChoiceComponent);
    return ElectiveChoiceComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/academic/fee-detail/fee-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Fee Details</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\" for=\"text-input\">Event Code</label>\n                        <div class=\"col-md-3\">\n                            <select class=\"form-control\" id=\"ccmonth\">\n                              <option>1</option>\n                              <option>2</option>\n                              <option>3</option>\n                              <option>4</option>\n                              <option>5</option>\n                              <option>6</option>\n                              <option>7</option>\n                              <option>8</option>\n                              <option>9</option>\n                              <option>10</option>\n                              <option>11</option>\n                              <option>12</option>\n                            </select>\n                        </div>\n                        <div class=\"col-md-3\">\n                            <button type=\"submit\" class=\"btn btn-success\"><i class=\"fa fa-eye \"></i>Show</button>\n                            <button class=\"btn btn-danger\" type=\"button\"><i class=\"fa fa-ban\"></i> Cancel</button>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-sm-6\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">Enrollment No.:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">Gender:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">Status:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">Student Name:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">Semester No.:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-6\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">Academic Year:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">Quota:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">Program:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">Branch:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-3 col-form-label\">STY Type:</label>\n                                        <div class=\"col-md-9\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <i class=\"fa fa-align-justify\"></i> Student Fee Details\n                </div>\n                <div class=\"card-body\">\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>Sr.No.</th>\n                                <th>Event Code</th>\n                                <th>STY Number/STY Type</th>\n                                <th>Quoted</th>\n                                <th>Currency Code</th>\n                                <th>Program Fee</th>\n                                <th>Fine Ammount</th>\n                                <th>App. Amount</th>\n                                <th>Discount</th>\n                                <th>Paid</th>\n                                <th>Excess Paid</th>\n                                <th>Dues</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/academic/fee-detail/fee-detail.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/academic/fee-detail/fee-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeeDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FeeDetailComponent = (function () {
    function FeeDetailComponent() {
    }
    FeeDetailComponent.prototype.ngOnInit = function () {
    };
    FeeDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-fee-detail',
            template: __webpack_require__("../../../../../src/app/views/student/academic/fee-detail/fee-detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/academic/fee-detail/fee-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FeeDetailComponent);
    return FeeDetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/academic/information-updation/information-updation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Student Information Updation</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\" style=\"width:1500px; height:500px; overflow: auto;\">\n                        <div class=\"col-sm-5\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-5 col-form-label\">Student Name: *</label>\n                                        <div class=\"col-md-7\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-5 col-form-label\">Rank:*</label>\n                                        <div class=\"col-md-7\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-5 col-form-label\">Academic Year:*</label>\n                                        <div class=\"col-md-7\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-5 col-form-label\">Admission Year:*</label>\n                                        <div class=\"col-md-7\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-5 col-form-label\">Program:*</label>\n                                        <div class=\"col-md-7\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-5 col-form-label\">Branch:*</label>\n                                        <div class=\"col-md-7\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-5\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-4 col-form-label\">Registration No:</label>\n                                        <div class=\"col-md-2\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                        <label class=\"col-md-4 col-form-label\">Active Status:</label>\n                                        <div class=\"col-md-1\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-4 col-form-label\">Remarks:</label>\n                                        <div class=\"col-md-2\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                        <label class=\"col-md-4 col-form-label\">STY Number:*</label>\n                                        <div class=\"col-md-1\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-5 col-form-label\">Section Code:</label>\n                                        <div class=\"col-md-7\">\n                                            <p class=\"form-control-static\"></p>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-2\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <div class=\"form-group row\">\n                                        <button class=\"btn-primary\">Upload Signature/Photo</button>\n                                    </div>\n\n                                    <div class=\"form-group row\">\n                                        <button class=\"btn-primary\">Up</button>\n                                        <button class=\"btn-primary\"> Photo</button>\n                                        <button class=\"btn-primary\"> ok</button>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n    </div>\n    <div class=\"tab\">\n        <button class=\"tablinks\" onclick=\"openCity(event, 'personal')\">[p]ersonal Details</button>\n        <button class=\"tablinks\" onclick=\"openCity(event, 'academic')\">[A]cademic Qualification</button>\n        <button class=\"tablinks\" onclick=\"openCity(event, 'address')\">[A]ddress Details</button>\n        <button class=\"tablinks\" onclick=\"openCity(event, 'contact')\">[C]ontact Details</button>\n        <button class=\"tablinks\" onclick=\"openCity(event, 'guardian')\">[G]uardian Details</button>\n        <button class=\"tablinks\" onclick=\"openCity(event, 'local')\">[L]ocal Guardian Details</button>\n    </div>\n\n    <div id=\"personal\" class=\"tabcontent\">\n        <div class=\"row\" style=\"width:1500px; height:500px; overflow: auto;\">\n            <div class=\"col-sm-5\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Date Of Birth:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"date\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Gender:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"radio\" name=\"gender\" value=\"male\" checked> Male<br>\n                                <input type=\"radio\" name=\"gender\" value=\"female\"> Female<br>\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Mother's Name:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Father's Name:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Nationality:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Category:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Blood Group:*</label>\n                            <div class=\"col-md-7\">\n                                <p class=\"form-control-static\"></p>\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\"> Willing To Donate Blood?: </label>\n                            <div class=\"col-md-7\">\n                                <input type=\"radio\" name=\"\" value=\"male\" checked> Yes<br>\n                                <input type=\"radio\" name=\"\" value=\"female\"> No<br>\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Residence Status: </label>\n                            <div class=\"col-md-7\">\n                                <input type=\"radio\" name=\"\" value=\"male\" checked> Yes<br>\n                                <input type=\"radio\" name=\"\" value=\"female\"> No<br>\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Software Package Known: </label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Computer Lang.Known: </label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-5\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Name in SSC Marksheet :*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Marital Status:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"radio\" name=\"\" value=\"male\" checked> Single<br>\n                                <input type=\"radio\" name=\"\" value=\"female\"> marriage<br>\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Mother Tongue:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Birth Place:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Admitted Category:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Admitted Sub-Category:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Medium Of Study(Previous) :*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\"> State Of Domicile </label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Regligion: </label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Student Quota: </label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Adhar No: </label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-2\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-12 col-form-label\">Games/Sports:</label>\n                            <select name=\"cars\" size=\"4\" multiple>\n                                    <option value=\"volvo\">Football</option>\n                                    <option value=\"saab\">Volleyball</option>\n                                    <option value=\"fiat\">Cricket</option>\n                                    <option value=\"audi\">Hockey</option>\n                                    <option value=\"audi\">Chess</option>\n                                    <option value=\"audi\">Badminton</option>\n                                  </select>\n\n                            <label class=\"col-md-8 col-form-label\">Hobbies:</label>\n                            <select name=\"cars\" size=\"4\" multiple>\n                                      <option value=\"volvo\">Music</option>\n                                      <option value=\"saab\">Painting</option>\n                                      <option value=\"fiat\">Dance</option>\n                                      <option value=\"audi\">Reading</option>\n                                      <option value=\"audi\">Drama</option>\n                                      <option value=\"audi\">Traveling</option> \n                                      <option value=\"audi\">Craft</option> \n                                    </select>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div id=\"academic\" class=\"tabcontent\">\n        <div class=\"row\" style=\"width:1500px; height:500px; overflow: auto;\">\n            <div class=\"col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Qualification: *</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Name of School:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Name Of Board/Univ :*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Name Of Exam Passed:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">Year Of Passing:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">State Name:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">City:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Country:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Division: *</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">Total Marks:</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Obtained Marks:</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">% Of Marks:</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Grade:</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">CGPA:</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">PCM Percentil:</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Overall Percentile:</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-6\">\n                <button class=\"btn-primary \">Add</button>\n                <button class=\"btn-danger\">Cancel</button>\n                <button class=\"btn-primary \">Ok</button>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-lg-12\">\n                    <div class=\"card\">\n                        <div class=\"card-header\">\n                            <i class=\"fa fa-align-justify\"></i>\n                        </div>\n                        <div class=\"card-body\">\n                            <table class=\"table table-bordered table-striped table-sm\">\n                                <thead>\n                                    <tr>\n                                        <th>uid</th>\n                                        <th>Qualification</th>\n                                        <th>Name Of Board</th>\n                                        <th>Name Of School</th>\n                                        <th>Name Of exam passed</th>\n                                        <th>Year</th>\n                                        <th>Division</th>\n                                        <th>Max Marks</th>\n                                        <th>Marks Obtained</th>\n                                        <th>Percentage</th>\n                                        <th>Grade</th>\n                                        <th>State</th>\n                                        <th>City</th>\n                                        <th>Country</th>\n\n                                    </tr>\n                                </thead>\n                                <tbody>\n\n                                </tbody>\n                            </table>\n\n                        </div>\n                    </div>\n                </div>\n                <!--/.col-->\n            </div>\n        </div>\n\n    </div>\n\n    <div id=\"address\" class=\"tabcontent\">\n        <div class=\"row\" style=\"width:1500px; height:500px; overflow: auto;\">\n            <div class=\"col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <h5>Address Of Parent /Guardian For Correpondance</h5>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-4col-form-label\">Address</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">Country:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Post Office:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">State:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Police Stn:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">City:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Railway stn:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">District:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Pincode:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <h6>Same as present address </h6>\n                        <h5> Permanent Address Of Parent /Guardian </h5>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-4col-form-label\">Address</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">Country:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Post Office:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">State:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Police Stn:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">City:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Railway stn:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-2 col-form-label\">District:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                            <label class=\"col-md-2 col-form-label\">Pincode:*</label>\n                            <div class=\"col-md-4\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <div id=\"contact\" class=\"tabcontent\">\n        <div class=\"row\" style=\"width:1500px; height:500px; overflow: auto;\">\n            <div class=\"col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <h6>Student Details</h6>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">STD code: *</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Telephone No:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Cell No. :*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Email-Id:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <h6>Parent Details</h6>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">STD code: *</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Telephone No:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Cell No. :*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Email-Id:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div id=\"guardian\" class=\"tabcontent\">\n        <div class=\"row\" style=\"width:1500px; height:500px; overflow: auto;\">\n            <div class=\"col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Guardian:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"radio\" name=\"gender\" value=\"male\" checked> Father\n                                <input type=\"radio\" name=\"gender\" value=\"female\"> Mother\n                                <input type=\"radio\" name=\"gender\" value=\"female\">Other\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Name of the Guardian:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Relation With Applicant:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Occupation:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Designation:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Educational Background:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">parents Annual Income:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"radio\" name=\"gender\" value=\"male\" checked> Upto 2.5 Lakh\n                                <input type=\"radio\" name=\"gender\" value=\"female\"> 2.5 to 4.5 Lakh\n                                <input type=\"radio\" name=\"gender\" value=\"female\">4.5 to 6.5 Lakh\n                                <input type=\"radio\" name=\"gender\" value=\"female\">Above 6.5 Lakh\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Organization Name: *</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Address Of Organization:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"textarea\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">City :*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">State*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Pin Code*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Contact Nos.(office)</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Mobile</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"number\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n    <div id=\"local\" class=\"tabcontent\">\n        <div class=\"row\" style=\"width:1500px; height:500px; overflow: auto;\">\n            <div class=\"col-sm-5\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">L.G.Name: *</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">L.G.Email:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Gender:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"radio\" name=\"gender\" value=\"male\" checked> Male\n                                <input type=\"radio\" name=\"gender\" value=\"female\"> Female\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">L.G.Address:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">City Name:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">district:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-5\">\n                <div class=\"card\">\n                    <div class=\"card-body\">\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">L.G.Contact No.: *</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">L.G.Occupation:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">Relation:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">State Name:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n                        <div class=\"form-group row\">\n                            <label class=\"col-md-5 col-form-label\">PIN Code:*</label>\n                            <div class=\"col-md-7\">\n                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-2\"> <button class=\"btn-primary\">ok</button>\n                <button class=\"btn-primary\">Add</button>\n                <button class=\"btn-danger\">Cancel</button>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-lg-12\">\n                    <div class=\"card\">\n                        <div class=\"card-header\">\n                            <i class=\"fa fa-align-justify\"></i>\n                        </div>\n                        <div class=\"card-body\">\n                            <table class=\"table table-bordered table-striped table-sm\">\n                                <thead>\n                                    <tr>\n                                        <th>uid</th>\n                                        <th>L.G.Name</th>\n                                        <th>L.G.Address</th>\n                                        <th>L.G.Address</th>\n                                        <th>City Name</th>\n                                        <th>District</th>\n                                        <th>State name</th>\n                                        <th>PIN</th>\n                                        <th>Guardian Email</th>\n                                        <th>LG Phone No.</th>\n                                        <th>Guardian Occupation</th>\n                                        <th>Gender</th>\n                                        <th>Relation</th>\n                                        <th>Deactive</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n\n                                </tbody>\n                            </table>\n\n                        </div>\n                    </div>\n                </div>\n                <!--/.col-->\n            </div>\n        </div>\n\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/academic/information-updation/information-updation.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body {\n  font-family: Arial; }\n\n/* Style the tab */\n.tab {\n  overflow: hidden;\n  border: 1px solid #ccc;\n  background-color: #f1f1f1; }\n\n/* Style the buttons inside the tab */\n.tab button {\n  background-color: inherit;\n  float: left;\n  border: none;\n  outline: none;\n  cursor: pointer;\n  padding: 14px 16px;\n  transition: 0.3s;\n  font-size: 17px; }\n\n/* Change background color of buttons on hover */\n.tab button:hover {\n  background-color: #ddd; }\n\n/* Create an active/current tablink class */\n.tab button.active {\n  background-color: #ccc; }\n\n/* Style the tab content */\n.tabcontent {\n  display: none;\n  padding: 6px 12px;\n  border: 1px solid #ccc;\n  border-top: none; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/academic/information-updation/information-updation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InformationUpdationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InformationUpdationComponent = (function () {
    function InformationUpdationComponent() {
        this.evt = '';
    }
    InformationUpdationComponent.prototype.ngOnInit = function () {
    };
    InformationUpdationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-information-updation',
            template: __webpack_require__("../../../../../src/app/views/student/academic/information-updation/information-updation.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/academic/information-updation/information-updation.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InformationUpdationComponent);
    return InformationUpdationComponent;
}());



/***/ })

});
//# sourceMappingURL=academic.module.chunk.js.map