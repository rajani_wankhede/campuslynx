webpackJsonp(["exam.module.0"],{

/***/ "../../../../../src/app/views/employee/exam/exam-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_views_employee_exam_marks_entry_marks_entry_component__ = __webpack_require__("../../../../../src/app/views/employee/exam/marks-entry/marks-entry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_views_employee_exam_view_event_subject_marks_view_event_subject_marks_component__ = __webpack_require__("../../../../../src/app/views/employee/exam/view-event-subject-marks/view-event-subject-marks.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_views_employee_exam_externalmarks_entry_permission_externalmarks_entry_permission_component__ = __webpack_require__("../../../../../src/app/views/employee/exam/externalmarks-entry-permission/externalmarks-entry-permission.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_views_employee_exam_marks_entry_dificiency_report_marks_entry_dificiency_report_component__ = __webpack_require__("../../../../../src/app/views/employee/exam/marks-entry-dificiency-report/marks-entry-dificiency-report.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        data: {
            title: 'Exam'
        },
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_2_app_views_employee_exam_marks_entry_marks_entry_component__["a" /* MarksEntryComponent */],
                data: {
                    title: 'Marks Entry'
                }
            },
            {
                path: 'viewEventSubjectMarks',
                component: __WEBPACK_IMPORTED_MODULE_3_app_views_employee_exam_view_event_subject_marks_view_event_subject_marks_component__["a" /* ViewEventSubjectMarksComponent */],
                data: {
                    title: 'View Event Subject Marks'
                }
            },
            {
                path: 'ExternalmarksEntryPermission',
                component: __WEBPACK_IMPORTED_MODULE_4_app_views_employee_exam_externalmarks_entry_permission_externalmarks_entry_permission_component__["a" /* ExternalmarksEntryPermissionComponent */],
                data: {
                    title: 'External Marks Entry Permission'
                }
            },
            {
                path: 'MarksEntryDificiencyReportt',
                component: __WEBPACK_IMPORTED_MODULE_5_app_views_employee_exam_marks_entry_dificiency_report_marks_entry_dificiency_report_component__["a" /* MarksEntryDificiencyReportComponent */],
                data: {
                    title: 'Marks Entry Dificiency Report'
                }
            }
        ]
    }
];
var ExamRoutingModule = (function () {
    function ExamRoutingModule() {
    }
    ExamRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], ExamRoutingModule);
    return ExamRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/exam/exam.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExamModule", function() { return ExamModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_tabs__ = __webpack_require__("../../../../ngx-bootstrap/tabs/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__exam_routing_module__ = __webpack_require__("../../../../../src/app/views/employee/exam/exam-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_views_employee_exam_externalmarks_entry_permission_externalmarks_entry_permission_component__ = __webpack_require__("../../../../../src/app/views/employee/exam/externalmarks-entry-permission/externalmarks-entry-permission.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_views_employee_exam_marks_entry_marks_entry_component__ = __webpack_require__("../../../../../src/app/views/employee/exam/marks-entry/marks-entry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_app_views_employee_exam_marks_entry_dificiency_report_marks_entry_dificiency_report_component__ = __webpack_require__("../../../../../src/app/views/employee/exam/marks-entry-dificiency-report/marks-entry-dificiency-report.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_app_views_employee_exam_view_event_subject_marks_view_event_subject_marks_component__ = __webpack_require__("../../../../../src/app/views/employee/exam/view-event-subject-marks/view-event-subject-marks.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// Forms Component
//import { FormsComponent } from './forms.component';

// Modal Component

// Tabs Component

// Components Routing





var ExamModule = (function () {
    function ExamModule() {
    }
    ExamModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__exam_routing_module__["a" /* ExamRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_tabs__["a" /* TabsModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5_app_views_employee_exam_externalmarks_entry_permission_externalmarks_entry_permission_component__["a" /* ExternalmarksEntryPermissionComponent */],
                __WEBPACK_IMPORTED_MODULE_6_app_views_employee_exam_marks_entry_marks_entry_component__["a" /* MarksEntryComponent */],
                __WEBPACK_IMPORTED_MODULE_7_app_views_employee_exam_marks_entry_dificiency_report_marks_entry_dificiency_report_component__["a" /* MarksEntryDificiencyReportComponent */],
                __WEBPACK_IMPORTED_MODULE_8_app_views_employee_exam_view_event_subject_marks_view_event_subject_marks_component__["a" /* ViewEventSubjectMarksComponent */]
            ]
        })
    ], ExamModule);
    return ExamModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/exam/externalmarks-entry-permission/externalmarks-entry-permission.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Subject Marks Entry Permission</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Registration No.*:</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Exam Event</label>\n                                            <select class=\"col-md-2 form-control\">\n                                              <option>-Select- </option>\n                                              <option>A</option>\n                                              <option>B</option>\n                                              <option>C</option>\n\n                                            </select>\n                                            <button class=\"btn-primary\">ok</button>\n                                            <button class=\"btn-danger\">Cancel</button>\n                                        </div>\n                                        <div disabled class=\"row\">\n                                            <div class=\"card col-sm-6\">\n                                                <div class=\"card-body\">\n                                                    <div class=\"form-group row\">\n                                                        <label class=\"col-md-4 col-form-label\" for=\"email-input\">Entry Level</label>\n                                                        <select disabled class=\"col-md-6 form-control\">\n                                                              <option>-Select- </option>\n                                                              <option>A</option>\n                                                              <option>B</option>\n                                                              <option>C</option>\n                \n                                                            </select>\n\n                                                    </div>\n                                                    <div class=\"form-group row\">\n                                                        <label class=\"col-md-4 col-form-label\">Subject Code</label>\n                                                        <div class=\"col-md-6\">\n                                                            <input disabled type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"form-group row\">\n                                                        <label class=\"col-md-4 col-form-label\">Employee Code</label>\n                                                        <div class=\"col-md-6\">\n                                                            <input disabled type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"form-group row\">\n                                                        <label class=\"col-md-4 col-form-label\">Entry Date</label>\n                                                        <div class=\"col-md-6\">\n                                                            <input disabled type=\"date\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"form-group row\">\n                                                        <label class=\"col-md-4 col-form-label\">Entry By</label>\n                                                        <div class=\"col-md-6\">\n                                                            <input disabled type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                                        </div>\n                                                    </div>\n                                                    <label class=\"col-md-6\"><strong><u>       </u></strong>&nbsp;  <input type=\"checkbox\">Active</label>\n                                                </div>\n                                            </div>\n                                            <div class=\"card col-sm-6\">\n                                                <div class=\"card-body\">\n                                                    <div class=\"form-group row\">\n                                                        <table class=\"table table-bordered table-striped table-sm\">\n                                                            <thead>\n                                                                <tr>\n                                                                    <th>SL.No</th>\n                                                                    <th></th>\n                                                                    <th>Bundle Code </th>\n                                                                </tr>\n                                                            </thead>\n                                                            <tbody>\n                                                            </tbody>\n                                                        </table>\n\n                                                    </div>\n\n\n                                                </div>\n                                            </div>\n\n                                            <!--/.col-->\n                                        </div>\n                                        <div class=\"row\">\n                                            <div class=\"col-lg-12\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-header\">\n\n                                                    </div>\n                                                    <div class=\"card-body\">\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-md-2 col-form-label\">Search:</label>\n                                                            <div class=\"col-md-4\">\n                                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"search\">\n                                                            </div>\n                                                            <label class=\"col-md-3 col-form-label\"></label>\n                                                            <button class=\"btn-primary\">ok</button>\n                                                            <button class=\"btn-primary\">Refresh</button>\n                                                            <button class=\"btn-danger\">Cancel</button>\n                                                            <button class=\"btn-primary\">stop</button>\n                                                        </div>\n\n                                                        <table class=\"table table-bordered table-striped table-sm\">\n                                                            <thead>\n                                                                <tr>\n                                                                    <th>SL.No</th>\n                                                                    <th>Subject Code</th>\n                                                                    <th>Staff Name </th>\n                                                                    <th>Bundle Code</th>\n                                                                    <th>Level 1</th>\n                                                                    <th>Level 2</th>\n                                                                    <th>Entry By </th>\n                                                                    <th>Deactive</th>\n                                                                </tr>\n                                                            </thead>\n                                                            <tbody>\n                                                            </tbody>\n                                                        </table>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <!--/.col-->\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/exam/externalmarks-entry-permission/externalmarks-entry-permission.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/exam/externalmarks-entry-permission/externalmarks-entry-permission.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExternalmarksEntryPermissionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ExternalmarksEntryPermissionComponent = (function () {
    function ExternalmarksEntryPermissionComponent() {
    }
    ExternalmarksEntryPermissionComponent.prototype.ngOnInit = function () {
    };
    ExternalmarksEntryPermissionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-externalmarks-entry-permission',
            template: __webpack_require__("../../../../../src/app/views/employee/exam/externalmarks-entry-permission/externalmarks-entry-permission.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/exam/externalmarks-entry-permission/externalmarks-entry-permission.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ExternalmarksEntryPermissionComponent);
    return ExternalmarksEntryPermissionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/exam/marks-entry-dificiency-report/marks-entry-dificiency-report.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>External Marks Entry Deficiency</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n                                        <div class=\"row\" style=\"width:800px; margin:0 auto;\">\n                                            <div class=\"col-lg-12\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-body\">\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-md-4 col-form-label\" for=\"email-input\">Registration Code*:</label>\n                                                            <select class=\"col-md-4 form-control\">\n                                                                  <option>-Select- </option>\n                                                                  <option>A</option>\n                                                                  <option>B</option>\n                                                                  <option>C</option>\n                                                                </select>\n                                                        </div>\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-md-4 col-form-label\" for=\"email-input\">Exam Event Code*:</label>\n                                                            <select class=\"col-md-4 form-control\">\n                                                                  <option>-Select- </option>\n                                                                  <option>A</option>\n                                                                  <option>B</option>\n                                                                  <option>C</option>\n                                                                </select>\n                                                        </div>\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-md-4 col-form-label\" for=\"email-input\">Subject Code*:</label>\n                                                            <select class=\"col-md-4 form-control\">\n                                                                  <option>-Select- </option>\n                                                                  <option>A</option>\n                                                                  <option>B</option>\n                                                                  <option>C</option>\n                                                                </select>\n                                                        </div>\n                                                        <div class=\"form-group row\">\n                                                            <label class=\"col-md-4 col-form-label\" for=\"email-input\">Report Type</label>\n                                                            <div class=\"col-md-5\">\n                                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">Deficiency Only\n                                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">All </div>\n                                                        </div>\n                                                        <div class=\"form-group row\">\n                                                            <input disabled type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Export To : 1.word  2.Excel  3.HTML  4.PDF\">\n\n                                                            <label class=\"col-md-4 col-form-label\"></label>\n                                                            <div class=\"col-md-4\">\n\n                                                                <button class=\"btn-primary \">Save</button>\n                                                                <button class=\"btn-primary \">Stop</button>\n                                                                <button class=\"btn-primary \">Print</button>\n\n\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                        <!--/.col-->\n\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>\n<!-- /.modal  -->\n<div bsModal #largeModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Semester Code</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal.hide()\" aria-label=\"Close\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n            <option>All</option>\n            <option>SI No</option>\n            <option>Registration code</option>\n            <option>Registrationdesc</option>\n\n          </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\">\n          <strong>0 Records of 0</strong>\n        </span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>Registration code</th>\n                                <th>Registrationdesc</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal1 -->\n<div bsModal #largeModal1=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Registration Number</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal1.hide()\" aria-label=\"Close\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n            <option>All</option>\n            <option>SI No</option>\n            <option>EnrollmentNo</option>\n            <option>Name</option>\n\n          </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\">\n          <strong>0 Records of 0</strong>\n        </span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>EnrollmentNo</th>\n                                <th>Name</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal1.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal1 -->\n<div bsModal #largeModal2=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Exam Event</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal2.hide()\" aria-label=\"Close\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n            <option>All</option>\n            <option>SI No</option>\n            <option>ExamEventcode</option>\n            <option>ExamEventDesc</option>\n\n          </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\">\n          <strong>0 Records of 0</strong>\n        </span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>ExamEventcode</th>\n                                <th>ExamEventDesc</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal2.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/exam/marks-entry-dificiency-report/marks-entry-dificiency-report.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/exam/marks-entry-dificiency-report/marks-entry-dificiency-report.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MarksEntryDificiencyReportComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MarksEntryDificiencyReportComponent = (function () {
    function MarksEntryDificiencyReportComponent() {
    }
    MarksEntryDificiencyReportComponent.prototype.ngOnInit = function () {
    };
    MarksEntryDificiencyReportComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-marks-entry-dificiency-report',
            template: __webpack_require__("../../../../../src/app/views/employee/exam/marks-entry-dificiency-report/marks-entry-dificiency-report.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/exam/marks-entry-dificiency-report/marks-entry-dificiency-report.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MarksEntryDificiencyReportComponent);
    return MarksEntryDificiencyReportComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/exam/marks-entry/marks-entry.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Internal Marks Entry</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Evaluator Name:</label>\n                                            <div class=\"col-md-3\">\n                                                <label>Shanu Sharma[1234]</label>\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\">Marks Entry For :</label>\n                                            <div class=\"col-md-5\">\n                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">Self Batch(by individual)\n                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">All Batch (by coordinatior)\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Semester Code</label>\n                                            <select class=\"col-md-2 form-control\">\n                                              <option>-Select- </option>\n                                              <option>A</option>\n                                              <option>B</option>\n                                              <option>C</option>\n\n                                            </select>\n                                            <label class=\"col-md-3 col-form-label\" for=\"email-input\">Choose Subject Pattern</label>\n                                            <select class=\"col-md-2 form-control\">\n                                              <option>-Select- </option>\n                                              <option>A</option>\n                                              <option>B</option>\n                                              <option>C</option>\n\n                                            </select>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Subject Description</label>\n                                            <select class=\"col-md-2 form-control\">\n                                              <option>-Select- </option>\n                                              <option>A</option>\n                                              <option>B</option>\n                                              <option>C</option>\n\n                                            </select>\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Subject component </label>\n                                            <select class=\"col-md-2 form-control\">\n                                              <option>-Select- </option>\n                                              <option>A</option>\n                                              <option>B</option>\n                                              <option>C</option>\n\n                                            </select>\n                                            <label class=\"col-md-1 col-form-label\" for=\"email-input\">Section</label>\n                                            <select class=\"col-md-2 form-control\">\n                                              <option>-Select- </option>\n                                              <option>A</option>\n                                              <option>B</option>\n                                              <option>C</option>\n\n                                            </select><br>\n                                            <button class=\"btn-primary\">ok</button>\n                                        </div>\n                                        <div class=\"row\">\n                                            <div class=\"col-lg-12\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-body\">\n                                                        <table class=\"table table-bordered table-striped table-sm\">\n                                                            <thead>\n                                                                <tr>\n                                                                    <th>Maximum Marks (Full Marks For Evaluation )</th>\n                                                                    <th>Weightage in marks </th>\n                                                                    <th>Passing Marks </th>\n                                                                    <th>Conditional Passing Marks </th>\n                                                                </tr>\n                                                            </thead>\n                                                            <tbody>\n                                                            </tbody>\n                                                        </table>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-12\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-body\">\n                                                        <div class=\"form-group row\">\n                                                            <div class=\"form-group row\">\n                                                                <label class=\"col-md-2 col-form-label\">Note:</label>\n                                                                <div class=\"col-md-3\">\n                                                                    <label>In case of mistake enter marks again and press tab button to reset prompt</label>\n                                                                </div>\n                                                                <label class=\"col-md-1 col-form-label\"></label>\n                                                                <div class=\"col-md-6\">\n                                                                    <button class=\"btn-primary col-md-2\">Save</button>\n                                                                    <button class=\"btn-primary col-md-2\">Stop</button>\n                                                                    <button class=\"btn-primary col-md-2\">Print</button>\n                                                                </div>\n                                                            </div>\n\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <!--/.col-->\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>\n<!-- /.modal  -->\n<div bsModal #largeModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Semester Code</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal.hide()\" aria-label=\"Close\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n            <option>All</option>\n            <option>SI No</option>\n            <option>Registration code</option>\n            <option>Registrationdesc</option>\n\n          </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\">\n          <strong>0 Records of 0</strong>\n        </span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>Registration code</th>\n                                <th>Registrationdesc</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal1 -->\n<div bsModal #largeModal1=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Registration Number</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal1.hide()\" aria-label=\"Close\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n            <option>All</option>\n            <option>SI No</option>\n            <option>EnrollmentNo</option>\n            <option>Name</option>\n\n          </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\">\n          <strong>0 Records of 0</strong>\n        </span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>EnrollmentNo</th>\n                                <th>Name</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal1.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>\n<!-- /.modal1 -->\n<div bsModal #largeModal2=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Exam Event</h4>\n                <button type=\"button\" class=\"close\" (click)=\"largeModal2.hide()\" aria-label=\"Close\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <select class=\"form-control\">\n            <option>All</option>\n            <option>SI No</option>\n            <option>ExamEventcode</option>\n            <option>ExamEventDesc</option>\n\n          </select>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                    </div>\n                    <span class=\"col-md-4\">\n          <strong>0 Records of 0</strong>\n        </span>\n                </div>\n                <div class=\"row\">\n                    <br>\n                    <table class=\"table table-bordered table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>SI No</th>\n                                <th>ExamEventcode</th>\n                                <th>ExamEventDesc</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n\n                            <tr></tr>\n                        </tbody>\n                    </table>\n\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"largeModal2.hide()\">Close</button>\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n            </div>\n        </div>\n        <!-- /.modal-content -->\n    </div>\n    <!-- /.modal-dialog -->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/exam/marks-entry/marks-entry.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/exam/marks-entry/marks-entry.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MarksEntryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MarksEntryComponent = (function () {
    function MarksEntryComponent() {
    }
    MarksEntryComponent.prototype.ngOnInit = function () {
    };
    MarksEntryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-marks-entry',
            template: __webpack_require__("../../../../../src/app/views/employee/exam/marks-entry/marks-entry.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/exam/marks-entry/marks-entry.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MarksEntryComponent);
    return MarksEntryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/exam/view-event-subject-marks/view-event-subject-marks.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Student Marks View</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Employee Name /Code</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\">Department:</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\">Designation:</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Reg. Code</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"email-input\" name=\"email-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <div class=\"col-md-2\">\n                                                <button class=\"btn-primary\">ok</button>\n                                            </div>\n                                        </div>\n\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Sub -Event Code</label>\n                                            <select class=\"col-md-2 form-control\">\n                        <option>-Select Event Code- </option>\n                        <option>A</option>\n                        <option>B</option>\n                        <option>C</option>\n\n                      </select>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"email-input\">Subject</label>\n                                            <select class=\"col-md-2 form-control\">\n                        <option>-Select- </option>\n                        <option>A</option>\n                        <option>B</option>\n                        <option>C</option>\n\n                      </select>\n                                            <button class=\"btn-primary\">ok</button>\n                                        </div>\n\n                                        <div class=\"row\">\n                                            <div class=\"col-lg-12\">\n                                                <div class=\"card\">\n                                                    <div class=\"card-header\" align=\"middle\">\n                                                        <i></i> Student List With Final Marks (Semester Wise)\n                                                    </div>\n                                                    <div class=\"card-body\">\n                                                        <table class=\"table table-bordered table-striped table-sm\">\n                                                            <thead>\n                                                                <tr>\n                                                                    <th>SI No</th>\n                                                                    <th>Enrollment No.</th>\n                                                                    <th>Student Name</th>\n                                                                    <th>Program</th>\n                                                                    <th>Semester</th>\n                                                                    <th>Marks Obtained(Out of 100)</th>\n                                                                </tr>\n                                                            </thead>\n                                                            <tbody>\n                                                            </tbody>\n                                                        </table>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/exam/view-event-subject-marks/view-event-subject-marks.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/exam/view-event-subject-marks/view-event-subject-marks.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewEventSubjectMarksComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ViewEventSubjectMarksComponent = (function () {
    function ViewEventSubjectMarksComponent() {
    }
    ViewEventSubjectMarksComponent.prototype.ngOnInit = function () {
    };
    ViewEventSubjectMarksComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-view-event-subject-marks',
            template: __webpack_require__("../../../../../src/app/views/employee/exam/view-event-subject-marks/view-event-subject-marks.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/exam/view-event-subject-marks/view-event-subject-marks.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ViewEventSubjectMarksComponent);
    return ViewEventSubjectMarksComponent;
}());



/***/ })

});
//# sourceMappingURL=exam.module.0.chunk.js.map