webpackJsonp(["feedback.module"],{

/***/ "../../../../../src/app/views/employee/feedback/advisorsfeddback-for-student/advisorsfeddback-for-student.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong> Faculty Advisor's Feedback For Students </strong>\n                    <small></small>\n                    <div style=\"background-color:lightgray;border:1px solid black; margin:10px;padding:10px;\">\n                        <small>\n                        <h5 style=\"text-decoration:underline;\">INSTRUCTIONS:</h5>\n                        <p>A) Please give your view / assessment about the student's quality in differnent aspects. The feed back will help us to find the difficulty faced by you and to improve the quality in future</p>\n                        <p>B) Write / put a number to indicate quality against eact factor i.e., Excellent -5 ,Very Good - 4, Good - 3, Avrage - 2, Below Average - 1 </p>\n                        <p>C) Read each factor carefully every time, then think and give your view in Number()i.e. 5,4,3,2 or 1) </p>\n                        <p>D) Write Name or regn. No./Roll No. of any of the student. Do not consult others, give your own rating.</p>\n                       </small>\n                    </div>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-3 col-form-label\"> Ragistation Code</label>\n                                            <div class=\"col-md-3\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Enter ragistation code\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-3 col-form-label\"> Student's Registration No.</label>\n                                            <div class=\"col-md-3\">\n                                                <select class=\"form-control\" id=\"ccmonth\">\n                                                  <option>1</option>\n                                                  <option>2</option>\n                                                  <option>3</option>\n                                                  <option>4</option>\n                                                  <option>5</option>\n                                                  <option>6</option>\n                                                  <option>7</option>\n                                                  <option>8</option>\n                                                  <option>9</option>\n                                                  <option>10</option>\n                                                  <option>11</option>\n                                                  <option>12</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-3 col-form-label\" for=\"text-input\">Event Code</label>\n                                            <div class=\"col-md-3\">\n                                                <select class=\"form-control\" id=\"ccmonth\">\n                                                  <option>1</option>\n                                                  <option>2</option>\n                                                  <option>3</option>\n                                                  <option>4</option>\n                                                  <option>5</option>\n                                                  <option>6</option>\n                                                  <option>7</option>\n                                                  <option>8</option>\n                                                  <option>9</option>\n                                                  <option>10</option>\n                                                  <option>11</option>\n                                                  <option>12</option>\n                                                </select>\n\n                                            </div>\n                                            <p style=\"color: red;padding:10px;\">Note : Feedback once saved can not be reverted back or changed again.</p>\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/advisorsfeddback-for-student/advisorsfeddback-for-student.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/advisorsfeddback-for-student/advisorsfeddback-for-student.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdvisorsfeddbackForStudentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AdvisorsfeddbackForStudentComponent = (function () {
    function AdvisorsfeddbackForStudentComponent() {
    }
    AdvisorsfeddbackForStudentComponent.prototype.ngOnInit = function () {
    };
    AdvisorsfeddbackForStudentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-advisorsfeddback-for-student',
            template: __webpack_require__("../../../../../src/app/views/employee/feedback/advisorsfeddback-for-student/advisorsfeddback-for-student.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/feedback/advisorsfeddback-for-student/advisorsfeddback-for-student.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AdvisorsfeddbackForStudentComponent);
    return AdvisorsfeddbackForStudentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/feedback-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__advisorsfeddback_for_student_advisorsfeddback_for_student_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/advisorsfeddback-for-student/advisorsfeddback-for-student.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__studentfeedback_sent_history_studentfeedback_sent_history_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-sent-history/studentfeedback-sent-history.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__studentfeedback_viewby_advisor_studentfeedback_viewby_advisor_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-viewby-advisor/studentfeedback-viewby-advisor.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__studentfeedback_for_me_studentfeedback_for_me_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-for-me/studentfeedback-for-me.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__studentfeedback_for_faculty_studentfeedback_for_faculty_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-for-faculty/studentfeedback-for-faculty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__student_attendanceentry_schedular_student_attendanceentry_schedular_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/student-attendanceentry-schedular/student-attendanceentry-schedular.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: '',
        data: {
            title: 'Feedback'
        },
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_2__advisorsfeddback_for_student_advisorsfeddback_for_student_component__["a" /* AdvisorsfeddbackForStudentComponent */],
                data: {
                    title: 'Advisors Feddback For Student'
                }
            },
            {
                path: 'StudentfeedbackSentHistory',
                component: __WEBPACK_IMPORTED_MODULE_3__studentfeedback_sent_history_studentfeedback_sent_history_component__["a" /* StudentfeedbackSentHistoryComponent */],
                data: {
                    title: 'Student Feedback Sent History'
                }
            },
            {
                path: 'StudentfeedbackViewbyAdvisor',
                component: __WEBPACK_IMPORTED_MODULE_4__studentfeedback_viewby_advisor_studentfeedback_viewby_advisor_component__["a" /* StudentfeedbackViewbyAdvisorComponent */],
                data: {
                    title: 'Student Feedback View by Advisor'
                }
            },
            {
                path: 'StudentfeedbackForMe',
                component: __WEBPACK_IMPORTED_MODULE_5__studentfeedback_for_me_studentfeedback_for_me_component__["a" /* StudentfeedbackForMeComponent */],
                data: {
                    title: 'Student feedback For Me'
                }
            },
            {
                path: 'StudentfeedbackForFaculty',
                component: __WEBPACK_IMPORTED_MODULE_6__studentfeedback_for_faculty_studentfeedback_for_faculty_component__["a" /* StudentfeedbackForFacultyComponent */],
                data: {
                    title: 'Student feedback For Faculty'
                }
            },
            {
                path: 'StudentAttendanceentrySchedular',
                component: __WEBPACK_IMPORTED_MODULE_7__student_attendanceentry_schedular_student_attendanceentry_schedular_component__["a" /* StudentAttendanceentrySchedularComponent */],
                data: {
                    title: 'Student Attendance Entry Schedular New'
                }
            },
        ]
    }
];
var FeedbackRoutingModule = (function () {
    function FeedbackRoutingModule() {
    }
    FeedbackRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], FeedbackRoutingModule);
    return FeedbackRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/feedback.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedbackModule", function() { return FeedbackModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_tabs__ = __webpack_require__("../../../../ngx-bootstrap/tabs/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__advisorsfeddback_for_student_advisorsfeddback_for_student_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/advisorsfeddback-for-student/advisorsfeddback-for-student.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__studentfeedback_sent_history_studentfeedback_sent_history_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-sent-history/studentfeedback-sent-history.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__studentfeedback_viewby_advisor_studentfeedback_viewby_advisor_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-viewby-advisor/studentfeedback-viewby-advisor.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__studentfeedback_for_me_studentfeedback_for_me_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-for-me/studentfeedback-for-me.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__studentfeedback_for_faculty_studentfeedback_for_faculty_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-for-faculty/studentfeedback-for-faculty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__student_attendanceentry_schedular_student_attendanceentry_schedular_component__ = __webpack_require__("../../../../../src/app/views/employee/feedback/student-attendanceentry-schedular/student-attendanceentry-schedular.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_app_views_employee_feedback_feedback_routing_module__ = __webpack_require__("../../../../../src/app/views/employee/feedback/feedback-routing.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// Forms Component
//import { FormsComponent } from './forms.component';

// Modal Component

// Tabs Component








var FeedbackModule = (function () {
    function FeedbackModule() {
    }
    FeedbackModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_10_app_views_employee_feedback_feedback_routing_module__["a" /* FeedbackRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_tabs__["a" /* TabsModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__advisorsfeddback_for_student_advisorsfeddback_for_student_component__["a" /* AdvisorsfeddbackForStudentComponent */],
                __WEBPACK_IMPORTED_MODULE_5__studentfeedback_sent_history_studentfeedback_sent_history_component__["a" /* StudentfeedbackSentHistoryComponent */],
                __WEBPACK_IMPORTED_MODULE_6__studentfeedback_viewby_advisor_studentfeedback_viewby_advisor_component__["a" /* StudentfeedbackViewbyAdvisorComponent */],
                __WEBPACK_IMPORTED_MODULE_7__studentfeedback_for_me_studentfeedback_for_me_component__["a" /* StudentfeedbackForMeComponent */],
                __WEBPACK_IMPORTED_MODULE_8__studentfeedback_for_faculty_studentfeedback_for_faculty_component__["a" /* StudentfeedbackForFacultyComponent */],
                __WEBPACK_IMPORTED_MODULE_9__student_attendanceentry_schedular_student_attendanceentry_schedular_component__["a" /* StudentAttendanceentrySchedularComponent */]
            ]
        })
    ], FeedbackModule);
    return FeedbackModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/student-attendanceentry-schedular/student-attendanceentry-schedular.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  student-attendanceentry-schedular works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/student-attendanceentry-schedular/student-attendanceentry-schedular.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/student-attendanceentry-schedular/student-attendanceentry-schedular.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentAttendanceentrySchedularComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StudentAttendanceentrySchedularComponent = (function () {
    function StudentAttendanceentrySchedularComponent() {
    }
    StudentAttendanceentrySchedularComponent.prototype.ngOnInit = function () {
    };
    StudentAttendanceentrySchedularComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-student-attendanceentry-schedular',
            template: __webpack_require__("../../../../../src/app/views/employee/feedback/student-attendanceentry-schedular/student-attendanceentry-schedular.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/feedback/student-attendanceentry-schedular/student-attendanceentry-schedular.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StudentAttendanceentrySchedularComponent);
    return StudentAttendanceentrySchedularComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-for-faculty/studentfeedback-for-faculty.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>  Feedback Analysis For Faculty </strong>\n                    <small></small>\n\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <div class=\"card-header\" style=\"margin-bottom:25px\">N\n                                        <h5> <strong>Department</strong></h5>\n\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-2 col-form-label\">Department</label>\n                                        <div class=\"col-md-4\">\n                                            <table class=\"table table-bordered table-striped table-sm\">\n                                                <!-- <thead>\n                                                    <tr>\n                                                        <th>Sr.No.</th>\n                                                        <th>Student Name(Regist. No.)</th>\n                                                        <th>Event Code</th>\n                                                        <th>Feedback Send Date and Time</th>\n                                                    </tr>\n                                                </thead> -->\n                                                <tbody>\n                                                    <tr>\n                                                        <td>Samppa Nori</td>\n                                                        <td>2012/01/01</td>\n                                                        <td>Member</td>\n                                                        <td>\n                                                            <span class=\"badge badge-success\">Active</span>\n                                                        </td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Estavan Lykos</td>\n                                                        <td>2012/02/01</td>\n                                                        <td>Staff</td>\n                                                        <td>\n                                                            <span class=\"badge badge-danger\">Banned</span>\n                                                        </td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Chetan Mohamed</td>\n                                                        <td>2012/02/01</td>\n                                                        <td>Admin</td>\n                                                        <td>\n                                                            <span class=\"badge badge-secondary\">Inactive</span>\n                                                        </td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Derick Maximinus</td>\n                                                        <td>2012/03/01</td>\n                                                        <td>Member</td>\n                                                        <td>\n                                                            <span class=\"badge badge-warning\">Pending</span>\n                                                        </td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Friderik Dávid</td>\n                                                        <td>2012/01/21</td>\n                                                        <td>Staff</td>\n                                                        <td>\n                                                            <span class=\"badge badge-success\">Active</span>\n                                                        </td>\n                                                    </tr>\n                                                </tbody>\n                                            </table>\n                                        </div>\n\n                                        <label class=\"col-md-2 col-form-label\">Employee</label>\n                                        <div class=\"col-md-4\">\n                                            <table class=\"table table-bordered table-striped table-sm\">\n                                                <!-- <thead>\n                                                    <tr>\n                                                        <th>Sr.No.</th>\n                                                        <th>Student Name(Regist. No.)</th>\n                                                        <th>Event Code</th>\n                                                        <th>Feedback Send Date and Time</th>\n                                                    </tr>\n                                                </thead> -->\n                                                <tbody>\n                                                    <tr>\n                                                        <td>Samppa Nori</td>\n                                                        <td>2012/01/01</td>\n                                                        <td>Member</td>\n                                                        <td>\n                                                            <span class=\"badge badge-success\">Active</span>\n                                                        </td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Estavan Lykos</td>\n                                                        <td>2012/02/01</td>\n                                                        <td>Staff</td>\n                                                        <td>\n                                                            <span class=\"badge badge-danger\">Banned</span>\n                                                        </td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Chetan Mohamed</td>\n                                                        <td>2012/02/01</td>\n                                                        <td>Admin</td>\n                                                        <td>\n                                                            <span class=\"badge badge-secondary\">Inactive</span>\n                                                        </td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Derick Maximinus</td>\n                                                        <td>2012/03/01</td>\n                                                        <td>Member</td>\n                                                        <td>\n                                                            <span class=\"badge badge-warning\">Pending</span>\n                                                        </td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Friderik Dávid</td>\n                                                        <td>2012/01/21</td>\n                                                        <td>Staff</td>\n                                                        <td>\n                                                            <span class=\"badge badge-success\">Active</span>\n                                                        </td>\n                                                    </tr>\n                                                </tbody>\n                                            </table>\n\n                                        </div>\n                                        <div class=\"col-md-4\">\n                                            <button type=\"button\" style=\"margin-left:400px;\" class=\"btn btn-primary\" disabled=\"disabled\">Primary</button>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <a href=\"#\">Double click on the above grid to get employees for the Department.</a>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-for-faculty/studentfeedback-for-faculty.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-for-faculty/studentfeedback-for-faculty.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentfeedbackForFacultyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StudentfeedbackForFacultyComponent = (function () {
    function StudentfeedbackForFacultyComponent() {
    }
    StudentfeedbackForFacultyComponent.prototype.ngOnInit = function () {
    };
    StudentfeedbackForFacultyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-studentfeedback-for-faculty',
            template: __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-for-faculty/studentfeedback-for-faculty.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-for-faculty/studentfeedback-for-faculty.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StudentfeedbackForFacultyComponent);
    return StudentfeedbackForFacultyComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-for-me/studentfeedback-for-me.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong> Feedback Analysis For Me</strong>\n                    <small></small>\n\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\"> SRS Event Code</label>\n                                            <div class=\"col-md-4\">\n\n                                                <input type=\"text\" id=\"text\" name=\"text\" placeholder=\"\">\n                                            </div>\n\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\"> Reg. Code</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text\" name=\"text\" placeholder=\"\">\n                                            </div>\n                                        </div>\n\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\"> Subject</label>\n                                            <div class=\"col-md-8\">\n                                                <table class=\"table table-bordered table-striped table-sm\">\n                                                    <thead>\n                                                        <tr>\n                                                            <th>Sr.No.</th>\n                                                            <th>Subject</th>\n                                                            <th>Description</th>\n\n                                                        </tr>\n                                                    </thead>\n                                                    <tbody>\n                                                        <tr>\n\n                                                            <td>Vishnu Serghei</td>\n                                                            <td>2012/01/01</td>\n                                                            <td>Member</td>\n\n                                                        </tr>\n\n\n                                                        <tr>\n                                                            <td>Vishnu Serghei</td>\n                                                            <td>2012/01/01</td>\n                                                            <td>Member</td>\n                                                        </tr>\n                                                        <tr>\n                                                            <td>Vishnu Serghei</td>\n                                                            <td>2012/01/01</td>\n                                                            <td>Member</td>\n\n                                                        </tr>\n                                                        <tr>\n                                                            <td>Vishnu Serghei</td>\n                                                            <td>2012/01/01</td>\n                                                            <td>Member</td>\n\n                                                        </tr>\n                                                        <tr>\n                                                            <td>Vishnu Serghei</td>\n                                                            <td>2012/01/01</td>\n                                                            <td>Member</td>\n\n                                                        </tr>\n                                                        <tr>\n                                                            <td>Vishnu Serghei</td>\n                                                            <td>2012/01/01</td>\n                                                            <td>Member</td>\n                                                        </tr>\n\n                                                    </tbody>\n                                                </table>\n                                            </div>\n                                        </div>\n\n                                        <div class=\"form-group row\">\n                                            <button type=\"button\" style=\"margin-left:400px;\" class=\"btn btn-lg btn-primary\">SAVE</button>\n                                        </div>\n                                    </form>\n\n                                </div>\n\n                            </div>\n                            <div class=\"card-body\">\n                                <table class=\"table table-bordered table-striped table-sm\">\n                                    <thead>\n                                        <tr>\n\n                                            <th>Subject</th>\n                                            <th>Description</th>\n                                            <th>Component</th>\n                                            <th>Average</th>\n                                            <th>Median</th>\n                                            <th>STD Dev.</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-success\">Active</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-danger\">Banned</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-secondary\">Inactive</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-warning\">Pending</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-success\">Active</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-success\">Active</span>\n                                            </td>\n                                        </tr>\n\n\n                                    </tbody>\n                                </table>\n\n                                <ul class=\"pagination\">\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Prev</a></li>\n                                    <li class=\"page-item active\">\n                                        <a class=\"page-link\" href=\"#\">1</a>\n                                    </li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">4</a></li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\n                                </ul>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-for-me/studentfeedback-for-me.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".checkbox {\n  border: 1px solid black;\n  border-radius: 20px;\n  padding: 10px;\n  margin: 15px;\n  text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-for-me/studentfeedback-for-me.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentfeedbackForMeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StudentfeedbackForMeComponent = (function () {
    function StudentfeedbackForMeComponent() {
    }
    StudentfeedbackForMeComponent.prototype.ngOnInit = function () {
    };
    StudentfeedbackForMeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-studentfeedback-for-me',
            template: __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-for-me/studentfeedback-for-me.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-for-me/studentfeedback-for-me.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StudentfeedbackForMeComponent);
    return StudentfeedbackForMeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-sent-history/studentfeedback-sent-history.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong> Student's Feedback Send History </strong>\n                    <small></small>\n\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n\n                                        <div class=\"form-group row\">\n\n                                            <div class=\"col-md-3\">\n                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">Feedback Event Wise\n                                            </div>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"feedback\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <div class=\"col-md-3\">\n                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">Student Wise\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\"> Registration No.</label>\n                                            <div class=\"col-md-3\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\"> Name</label>\n                                            <div class=\"col-md-3\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n\n                                        <div class=\"form-group row\">\n                                            <div class=\"col-md-3\">\n                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">Duration Wise\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\"> From</label>\n                                            <div class=\"col-md-3\">\n                                                <input type=\"date\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-1 col-form-label\"> To</label>\n                                            <div class=\"col-md-3\">\n                                                <input type=\"date\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n\n                                        <button type=\"button\" style=\"margin-left:400px;\" class=\"btn  btn-primary btn-lg\">SAVE</button>\n\n                                    </form>\n\n                                </div>\n\n                            </div>\n                            <div class=\"card-body\">\n                                <table class=\"table table-bordered table-striped table-sm\">\n                                    <thead>\n                                        <tr>\n                                            <th>Sr.No.</th>\n                                            <th>Student Name(Regist. No.)</th>\n                                            <th>Event Code</th>\n                                            <th>Feedback Send Date and Time</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n                                            <td>Samppa Nori</td>\n                                            <td>2012/01/01</td>\n                                            <td>Member</td>\n                                            <td>\n                                                <span class=\"badge badge-success\">Active</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td>Estavan Lykos</td>\n                                            <td>2012/02/01</td>\n                                            <td>Staff</td>\n                                            <td>\n                                                <span class=\"badge badge-danger\">Banned</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td>Chetan Mohamed</td>\n                                            <td>2012/02/01</td>\n                                            <td>Admin</td>\n                                            <td>\n                                                <span class=\"badge badge-secondary\">Inactive</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td>Derick Maximinus</td>\n                                            <td>2012/03/01</td>\n                                            <td>Member</td>\n                                            <td>\n                                                <span class=\"badge badge-warning\">Pending</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td>Friderik Dávid</td>\n                                            <td>2012/01/21</td>\n                                            <td>Staff</td>\n                                            <td>\n                                                <span class=\"badge badge-success\">Active</span>\n                                            </td>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                                <ul class=\"pagination\">\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Prev</a></li>\n                                    <li class=\"page-item active\">\n                                        <a class=\"page-link\" href=\"#\">1</a>\n                                    </li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">4</a></li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\n                                </ul>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-sent-history/studentfeedback-sent-history.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".checkbox {\n  border: 1px solid black;\n  border-radius: 20px;\n  padding: 10px;\n  margin: 15px;\n  text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-sent-history/studentfeedback-sent-history.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentfeedbackSentHistoryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StudentfeedbackSentHistoryComponent = (function () {
    function StudentfeedbackSentHistoryComponent() {
    }
    StudentfeedbackSentHistoryComponent.prototype.ngOnInit = function () {
    };
    StudentfeedbackSentHistoryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-studentfeedback-sent-history',
            template: __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-sent-history/studentfeedback-sent-history.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-sent-history/studentfeedback-sent-history.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StudentfeedbackSentHistoryComponent);
    return StudentfeedbackSentHistoryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-viewby-advisor/studentfeedback-viewby-advisor.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong> Student's Feedback View By Advisor</strong>\n                    <small></small>\n\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\"> Feedback Event Code</label>\n                                            <div class=\"col-md-4\">\n                                                <select id=\"select\" name=\"select\" class=\"form-control\">\n                                                  <option value=\"0\">Please select</option>\n                                                  <option value=\"1\">Option #1</option>\n                                                  <option value=\"2\">Option #2</option>\n                                                  <option value=\"3\">Option #3</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\"> Advisor Name</label>\n                                            <div class=\"col-md-6\">\n                                                <input type=\"text\" id=\"text\" name=\"text\" placeholder=\"Shanu Sharma\">\n                                            </div>\n                                        </div>\n\n                                        <button type=\"button\" style=\"margin-left:400px;\" class=\"btn btn-lg btn-primary \">SAVE</button>\n                                    </form>\n\n                                </div>\n\n                            </div>\n                            <div class=\"card-body\">\n                                <table class=\"table table-bordered table-striped table-sm\">\n                                    <thead>\n                                        <tr>\n                                            <th>Sr.No.</th>\n                                            <th>Questions</th>\n                                            <th>Maximum Rateing</th>\n                                            <th>Given Rating</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-success\">Active</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-danger\">Banned</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td> </td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-secondary\">Inactive</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-warning\">Pending</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td> </td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-success\">Active</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td> </td>\n                                            <td></td>\n                                            <td></td>\n                                            <td>\n                                                <span class=\"badge badge-success\">Active</span>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td> </td>\n                                            <td></td>\n                                            <td>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-md-4 \"> Rating Percen:</label>\n                                                    <div class=\"col-md-3\">\n                                                        <input type=\"text\" id=\"text-input\" name=\"text-input\" placeholder=\"\">\n                                                    </div>\n                                                </div>\n\n\n                                            </td>\n                                            <td>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-md-4 \"> Overall Rating:</label>\n                                                    <div class=\"col-md-3\">\n                                                        <input type=\"text\" id=\"text-input\" name=\"text-input\" placeholder=\"\">\n                                                    </div>\n                                                </div>\n\n\n                                            </td>\n                                        </tr>\n\n                                    </tbody>\n                                </table>\n\n                                <ul class=\"pagination\">\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Prev</a></li>\n                                    <li class=\"page-item active\">\n                                        <a class=\"page-link\" href=\"#\">1</a>\n                                    </li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">4</a></li>\n                                    <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\n                                </ul>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-viewby-advisor/studentfeedback-viewby-advisor.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".checkbox {\n  border: 1px solid black;\n  border-radius: 20px;\n  padding: 10px;\n  margin: 15px;\n  text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/employee/feedback/studentfeedback-viewby-advisor/studentfeedback-viewby-advisor.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentfeedbackViewbyAdvisorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StudentfeedbackViewbyAdvisorComponent = (function () {
    function StudentfeedbackViewbyAdvisorComponent() {
    }
    StudentfeedbackViewbyAdvisorComponent.prototype.ngOnInit = function () {
    };
    StudentfeedbackViewbyAdvisorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-studentfeedback-viewby-advisor',
            template: __webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-viewby-advisor/studentfeedback-viewby-advisor.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/employee/feedback/studentfeedback-viewby-advisor/studentfeedback-viewby-advisor.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StudentfeedbackViewbyAdvisorComponent);
    return StudentfeedbackViewbyAdvisorComponent;
}());



/***/ })

});
//# sourceMappingURL=feedback.module.chunk.js.map