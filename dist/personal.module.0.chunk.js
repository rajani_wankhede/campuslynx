webpackJsonp(["personal.module.0"],{

/***/ "../../../../../src/app/views/student/personal/drcradvice/drcradvice.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n    <section class=\"content\" id=\"tb\">\n        <div class=\"card\">\n            <div class=\"card-body\">\n                <div class=\"well\">\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\">Enrollnment No</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Text\">\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"text-input\">Name</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Text\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\">Branch Code</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"number\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"number\">\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"text-input\">Branch Description</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"branch\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-md-2 col-form-label\">From Date</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"date\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"from date\">\n                        </div>\n                        <label class=\"col-md-2 col-form-label\" for=\"text-input\">To Date</label>\n                        <div class=\"col-md-4\">\n                            <input type=\"date\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Text\">\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"well\" color=\"Red\">\n                    &nbsp;&nbsp;\n                    <div class=\"form-group col-md-2\">\n                    </div>\n                </div>\n                <div class=\"well\" color=\"Red\">\n                    <button class=\"btn btn-md  btn-primary\" (click)=\"searchjs()\"><i class=\"fa fa-spinner\" aria-hidden=\"true\"></i>Search</button>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-lg-12\">\n                        <div class=\"card\">\n                            <div class=\"card-header\">\n                                <i class=\"fa fa-align-justify\"></i> Debit credit Advice\n                            </div>\n                            <div class=\"card-body\">\n                                <table class=\"table table-bordered table-striped table-sm\">\n                                    <thead>\n                                        <tr>\n                                            <th>SL.No</th>\n                                            <th>Advice Date</th>\n                                            <th>Advice Type DR/CR</th>\n                                            <th>Amount</th>\n                                            <th>Voucher no/Voucher Date</th>\n                                            <th>From/Description</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                    </tbody>\n                                </table>\n                            </div>\n                        </div>\n                    </div>\n                    <!--/.col-->\n                </div>\n            </div>\n        </div>\n    </section>\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/personal/drcradvice/drcradvice.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/personal/drcradvice/drcradvice.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DrcradviceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DrcradviceComponent = (function () {
    function DrcradviceComponent() {
    }
    DrcradviceComponent.prototype.ngOnInit = function () {
    };
    DrcradviceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-drcradvice',
            template: __webpack_require__("../../../../../src/app/views/student/personal/drcradvice/drcradvice.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/personal/drcradvice/drcradvice.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DrcradviceComponent);
    return DrcradviceComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/personal/personal-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonalRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__personal_component__ = __webpack_require__("../../../../../src/app/views/student/personal/personal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__drcradvice_drcradvice_component__ = __webpack_require__("../../../../../src/app/views/student/personal/drcradvice/drcradvice.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        data: {
            title: 'Personal'
        },
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_2__personal_component__["a" /* PersonalComponent */],
                data: {
                    title: 'Personal'
                }
            },
            {
                path: 'drcradvice',
                component: __WEBPACK_IMPORTED_MODULE_3__drcradvice_drcradvice_component__["a" /* DrcradviceComponent */],
                data: {
                    title: 'My DR/CR advice'
                }
            },
        ]
    }
];
var PersonalRoutingModule = (function () {
    function PersonalRoutingModule() {
    }
    PersonalRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], PersonalRoutingModule);
    return PersonalRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/personal/personal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>Student Personal Form</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-9\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Student Name</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Text\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"text-input\">Gender</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\"> Option 1\n                                            </div>\n\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Registration Number</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"text-input\">Room Number</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"disabled-input\">Program/Branch</label>\n                                            <div class=\"col-md-9\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Semester</label>\n                                            <div class=\"col-md-3\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"text-input\">Date Of Birth</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"text-input\">Blood Group</label>\n                                            <div class=\"col-md-1\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Nationality</label>\n                                            <div class=\"col-md-3\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"text-input\">Marital Status</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"text-input\">Category</label>\n                                            <div class=\"col-md-1\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Bank Name</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"text-input\">Bank A/C No</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">Father's Name</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"text-input\">Designation</label>\n                                            <div class=\"col-md-4\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\" for=\"disabled-input\">Mother's Name</label>\n                                            <div class=\"col-md-9\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n\n                                            </div>\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-2\">\n                            <div class=\"card\">\n                                <img src=\"assets/img/avatars/6.jpg\" alt=\"Paris\" width=\"300\" height=\"300\">\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"card-body\">\n                <div class=\"row\">\n                    <div class=\"col-sm-12\">\n                        <div class=\"card\">\n                            <div class=\"card-body\">\n                                <div class=\"well\">\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-6 col-form-label\" for=\"disabled-input\">Student Contact Address</label>\n                                        <label class=\"col-md-6 col-form-label\" for=\"disabled-input\">Parent/Guardian Contact Detail</label>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-2 col-form-label\">Cell/Mobile</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                        <label class=\"col-md-2 col-form-label\" for=\"text-input\">Cell/Mobile</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-2 col-form-label\">Telephone</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                        <label class=\"col-md-2 col-form-label\" for=\"text-input\">Telephone</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-2 col-form-label\">E-mail</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                        <label class=\"col-md-2 col-form-label\" for=\"text-input\">E-mail</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                    </div>\n\n                                </div>\n\n                                <div class=\"well\">\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-6 col-form-label\" for=\"disabled-input\"> Correspondence Address </label>\n                                        <label class=\"col-md-6 col-form-label\" for=\"disabled-input\">Permanent Address</label>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-2 col-form-label\">Address</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                        <label class=\"col-md-2 col-form-label\">Address</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-2 col-form-label\">District</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                        <label class=\"col-md-2 col-form-label\" for=\"text-input\">District</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-2 col-form-label\">City/PIN</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                        <label class=\"col-md-2 col-form-label\">City/PIN</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-md-2 col-form-label\">State</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                        <label class=\"col-md-2 col-form-label\">State</label>\n                                        <div class=\"col-md-4\">\n                                            <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"\">\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-lg-12\">\n                                            <div class=\"card\">\n                                                <div class=\"card-header\">\n                                                    <i class=\"fa fa-align-justify\"></i>\n                                                </div>\n                                                <div class=\"card-body\">\n                                                    <table class=\"table table-bordered table-striped table-sm\">\n                                                        <thead>\n                                                            <tr>\n                                                                <th>Qualification</th>\n                                                                <th>Board/School </th>\n                                                                <th>Year</th>\n                                                                <th>Division</th>\n                                                                <th>Full Marks</th>\n                                                                <th>Obtained Marks</th>\n                                                                <th>Percentage Marks</th>\n                                                                <th>Grade</th>\n\n                                                            </tr>\n                                                        </thead>\n                                                        <tbody>\n\n                                                        </tbody>\n                                                    </table>\n\n                                                </div>\n                                            </div>\n                                        </div>\n                                        <!--/.col-->\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/personal/personal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PersonalComponent = (function () {
    function PersonalComponent() {
    }
    PersonalComponent.prototype.ngOnInit = function () {
    };
    PersonalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-personal',
            template: __webpack_require__("../../../../../src/app/views/student/personal/personal.component.html"),
        }),
        __metadata("design:paramtypes", [])
    ], PersonalComponent);
    return PersonalComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/personal/personal.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PersonalModule", function() { return PersonalModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__personal_component__ = __webpack_require__("../../../../../src/app/views/student/personal/personal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__drcradvice_drcradvice_component__ = __webpack_require__("../../../../../src/app/views/student/personal/drcradvice/drcradvice.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap_tabs__ = __webpack_require__("../../../../ngx-bootstrap/tabs/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__personal_routing_module__ = __webpack_require__("../../../../../src/app/views/student/personal/personal-routing.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// Forms Component
//import { FormsComponent } from './forms.component';

// Modal Component

// Tabs Component

// Components Routing

var PersonalModule = (function () {
    function PersonalModule() {
    }
    PersonalModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_6__personal_routing_module__["a" /* PersonalRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap_modal__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap_tabs__["a" /* TabsModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__personal_component__["a" /* PersonalComponent */],
                __WEBPACK_IMPORTED_MODULE_2__drcradvice_drcradvice_component__["a" /* DrcradviceComponent */]
            ]
        })
    ], PersonalModule);
    return PersonalModule;
}());



/***/ })

});
//# sourceMappingURL=personal.module.0.chunk.js.map