webpackJsonp(["dashboard.module"],{

/***/ "../../../../../src/app/views/dashboard/dashboard-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_component__ = __webpack_require__("../../../../../src/app/views/dashboard/dashboard.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__dashboard_component__["a" /* DashboardComponent */],
        data: {
            title: 'Dashboard'
        }
    }
];
var DashboardRoutingModule = (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"btn-group col-md-1 col-sm-2 float-right \" dropdown>\n        <button type=\"button\" class=\"btn btn-lg btn-primary dropdown-toggle p-0 \" dropdownToggle>Display</button>\n        <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\n            <form name=\"frm\" method=\"post\">\n\n                <ul id=\"checkBoxes\">\n                    <input type=\"checkbox\" name=\"check\" value=\"check1\" (click)=\"changeShowStatus()\" class=\"selectedId\" id=\"check1\">Frame 1<br>\n                    <input type=\"checkbox\" name=\"check\" value=\"check2\" (click)=\"changeShowStatus()\" class=\"selectedId\" id=\"check2\">Frame 2<br>\n                    <input type=\"checkbox\" name=\"check\" value=\"check3\" (click)=\"changeShowStatus()\" class=\"selectedId\" id=\"check3\">Frame 3<br>\n                    <input type=\"checkbox\" name=\"check\" value=\"check4\" (click)=\"changeShowStatus()\" class=\"selectedId\" id=\"check4\">Frame 4<br>\n                    <input type=\"checkbox\" name=\"check\" value=\"check5\" (click)=\"changeShowStatus()\" class=\"selectedId\" id=\"check5\">Frame 5<br>\n                    <input type=\"checkbox\" name=\"check\" value=\"check6\" (click)=\"changeShowStatus()\" class=\"selectedId\" id=\"check6\">Frame 6<br>\n                </ul>\n            </form>\n        </div>\n    </div>\n\n    <!-- <ul class=\"chk-container\">\n            <li><input type=\"checkbox\" (click)=\"checkall()\" id=\"selectAll\" />Select All</li>\n            <li><input type=\"checkbox\" name=\"check\" value=\"check1\" class=\"selectedId\" />Checkbox 1</li>\n            <li><input type=\"checkbox\" name=\"check\" value=\"check2\" class=\"selectedId\" />Checkbox 2</li>\n            <li><input type=\"checkbox\" name=\"check\" value=\"check3\" class=\"selectedId\" />Checkbox 3</li>\n            <li><input type=\"checkbox\" name=\"check\" value=\"check4\" class=\"selectedId\" />Checkbox 4</li>\n            <li><input type=\"checkbox\" name=\"check\" value=\"check5\" class=\"selectedId\" />Checkbox 5</li>\n        </ul> -->\n\n    <!-- <div id=\"AdvancedOption\" style=\"display: none;\" class=\"col-md-8 col-sm-8 btn-group-sm\">\n            <a href=\"#\" class=\"btn btn-danger toolbar2 deleteAccount\" title=\"Delete this account\" role=\"button\">\n                <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>\n            </a>\n            <a href=\"#\" class=\"btn btn-warning toolbar1 editTeam\" title=\"Edit this account\" role=\"button\" style=\"display: none;\">\n                <span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>\n            </a>\n        </div> -->\n    <div id=\"snackbar0\">You have selected Frame-1</div>\n    <div id=\"snackbar1\">You have selected Frame-2</div>\n    <div id=\"snackbar2\">You have selected Frame-3</div>\n    <div id=\"snackbar3\">You have selected Frame-4</div>\n    <div id=\"snackbar4\">You have selected Frame-5</div>\n    <div id=\"snackbar5\">You have selected Frame-6</div>\n\n    <div class=\"row\">\n        <div id=\"frame1\" class=\"col-sm-12 col-lg-6\">\n            <div class=\"card text-white bg-primary\">\n                <div class=\"card-body pb-0\">\n                    <div class=\"btn-group float-right\" dropdown>\n                        <button type=\"button\" class=\"btn btn-transparent dropdown-toggle p-0\" dropdownToggle>\n                                <i class=\"icon-settings\"></i>\n                             </button>\n                        <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\n                            <a class=\"dropdown-item\" href=\"#\">Action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Another action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n                        </div>\n                    </div>\n                    <h4 class=\"mb-0\">Frame-1</h4>\n                    <p>All Notification or information or general Broadcast </p>\n                </div>\n                <div class=\"chart-wrapper px-3\" style=\"height:70px;\">\n                    <canvas baseChart class=\"chart\" [datasets]=\"lineChart1Data\" [labels]=\"lineChart1Labels\" [options]=\"lineChart1Options\" [colors]=\"lineChart1Colours\" [legend]=\"lineChart1Legend\" [chartType]=\"lineChart1Type\" (chartHover)=\"chartHovered($event)\" (chartClick)=\"chartClicked($event)\"></canvas>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n        <div id='frame4' style=\"display: none;\" class=\"col-sm-12 col-lg-6\">\n            <div class=\"card text-white bg-info\">\n                <div class=\"card-body pb-0\">\n                    <button type=\"button\" class=\"btn btn-transparent p-0 float-right\">\n                <i class=\"icon-location-pin\"></i>\n              </button>\n                    <h4 class=\"mb-0\">Frame-4</h4>\n                    <p></p>\n                </div>\n                <div class=\"chart-wrapper px-3\" style=\"height:70px;\">\n                    <canvas baseChart class=\"chart\" [datasets]=\"lineChart2Data\" [labels]=\"lineChart2Labels\" [options]=\"lineChart2Options\" [colors]=\"lineChart2Colours\" [legend]=\"lineChart2Legend\" [chartType]=\"lineChart2Type\" (chartHover)=\"chartHovered($event)\" (chartClick)=\"chartClicked($event)\"></canvas>\n                </div>\n            </div>\n        </div>\n        <!--/.col-->\n        <div id='frame2' style=\"display: none;\" class=\"col-sm-12 col-lg-6\">\n            <div class=\"card text-white bg-warning\">\n                <div class=\"card-body pb-0\">\n                    <div class=\"btn-group float-right\" dropdown>\n                        <button type=\"button\" class=\"btn btn-transparent dropdown-toggle p-0\" dropdownToggle>\n                  <i class=\"icon-settings\"></i>\n                </button>\n                        <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\n                            <a class=\"dropdown-item\" href=\"#\">Action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Another action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n                        </div>\n                    </div>\n                    <h4 class=\"mb-0\">Frame-2</h4>\n                    <p>All Approving request for action</p>\n                </div>\n                <div class=\"chart-wrapper\" style=\"height:70px;\">\n                    <canvas baseChart class=\"chart\" [datasets]=\"lineChart3Data\" [labels]=\"lineChart3Labels\" [options]=\"lineChart3Options\" [colors]=\"lineChart3Colours\" [legend]=\"lineChart3Legend\" [chartType]=\"lineChart3Type\" (chartHover)=\"chartHovered($event)\" (chartClick)=\"chartClicked($event)\"></canvas>\n                </div>\n            </div>\n        </div>\n        <div id='frame5' style=\"display: none;\" class=\"col-sm-12 col-lg-6\">\n            <div class=\"card text-white bg-warning\">\n                <div class=\"card-body pb-0\">\n                    <div class=\"btn-group float-right\" dropdown>\n                        <button type=\"button\" class=\"btn btn-transparent dropdown-toggle p-0\" dropdownToggle>\n                <i class=\"icon-settings\"></i>\n              </button>\n                        <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\n                            <a class=\"dropdown-item\" href=\"#\">Action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Another action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n                        </div>\n                    </div>\n                    <h4 class=\"mb-0\">Frame-5</h4>\n                    <p></p>\n                </div>\n                <div class=\"chart-wrapper\" style=\"height:70px;\">\n                    <canvas baseChart class=\"chart\" [datasets]=\"lineChart3Data\" [labels]=\"lineChart3Labels\" [options]=\"lineChart3Options\" [colors]=\"lineChart3Colours\" [legend]=\"lineChart3Legend\" [chartType]=\"lineChart3Type\" (chartHover)=\"chartHovered($event)\" (chartClick)=\"chartClicked($event)\"></canvas>\n                </div>\n            </div>\n        </div>\n        <div id='frame3' style=\"display: none;\" class=\"col-sm-12 col-lg-6\">\n            <div class=\"card text-white bg-danger\">\n                <div class=\"card-body pb-0\">\n                    <div class=\"btn-group float-right\" dropdown>\n                        <button type=\"button\" class=\"btn btn-transparent dropdown-toggle p-0\" dropdownToggle>\n                <i class=\"icon-settings\"></i>\n              </button>\n                        <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\n                            <a class=\"dropdown-item\" href=\"#\">Action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Another action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n                        </div>\n                    </div>\n                    <h4 class=\"mb-0\">Frame-3</h4>\n                    <p>SCGA Semester Wise </p>\n                </div>\n                <div class=\"chart-wrapper px-3\" style=\"height:70px;\">\n                    <canvas baseChart class=\"chart\" [datasets]=\"barChart1Data\" [labels]=\"barChart1Labels\" [options]=\"barChart1Options\" [colors]=\"barChart1Colours\" [legend]=\"barChart1Legend\" [chartType]=\"barChart1Type\" (chartHover)=\"chartHovered($event)\" (chartClick)=\"chartClicked($event)\"></canvas>\n                </div>\n            </div>\n        </div>\n        <div id='frame6' style=\"display: none;\" class=\"col-sm-12 col-lg-6\">\n            <div class=\"card text-white bg-primary\">\n                <div class=\"card-body pb-0\">\n                    <div class=\"btn-group float-right\" dropdown>\n                        <button type=\"button\" class=\"btn btn-transparent dropdown-toggle p-0\" dropdownToggle>\n                <i class=\"icon-settings\"></i>\n              </button>\n                        <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu>\n                            <a class=\"dropdown-item\" href=\"#\">Action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Another action</a>\n                            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n                        </div>\n                    </div>\n                    <h4 class=\"mb-0\">Frame-6</h4>\n                    <p></p>\n                </div>\n                <div class=\"chart-wrapper px-3\" style=\"height:70px;\">\n                    <canvas baseChart class=\"chart\" [datasets]=\"lineChart1Data\" [labels]=\"lineChart1Labels\" [options]=\"lineChart1Options\" [colors]=\"lineChart1Colours\" [legend]=\"lineChart1Legend\" [chartType]=\"lineChart1Type\" (chartHover)=\"chartHovered($event)\" (chartClick)=\"chartClicked($event)\"></canvas>\n                </div>\n            </div>\n        </div>\n\n        <!--/.col-->\n    </div>\n    <!--/.row-->\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/dashboard/dashboard.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#snackbar0,\n#snackbar1,\n#snackbar2,\n#snackbar3,\n#snackbar4,\n#snackbar5 {\n  visibility: hidden;\n  min-width: 250px;\n  margin-left: -125px;\n  background-color: #1864dd;\n  color: #fff;\n  text-align: center;\n  border-radius: 2px;\n  padding: 16px;\n  position: fixed;\n  z-index: 1;\n  left: 50%;\n  bottom: 30px;\n  font-size: 17px; }\n\n#snackbar0.show,\n#snackbar0.show,\n#snackbar1.show,\n#snackbar2.show,\n#snackbar3.show,\n#snackbar4.show,\n#snackbar5.show {\n  visibility: visible;\n  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;\n  animation: fadein 0.5s, fadeout 0.5s 2.5s; }\n\n@-webkit-keyframes fadein {\n  from {\n    bottom: 0;\n    opacity: 0; }\n  to {\n    bottom: 30px;\n    opacity: 1; } }\n\n@keyframes fadein {\n  from {\n    bottom: 0;\n    opacity: 0; }\n  to {\n    bottom: 30px;\n    opacity: 1; } }\n\n@-webkit-keyframes fadeout {\n  from {\n    bottom: 30px;\n    opacity: 1; }\n  to {\n    bottom: 0;\n    opacity: 0; } }\n\n@keyframes fadeout {\n  from {\n    bottom: 30px;\n    opacity: 1; }\n  to {\n    bottom: 0;\n    opacity: 0; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = (function () {
    function DashboardComponent() {
        this.brandPrimary = '#20a8d8';
        this.brandSuccess = '#4dbd74';
        this.brandInfo = '#63c2de';
        this.brandWarning = '#f8cb00';
        this.brandDanger = '#f86c6b';
        // lineChart1
        this.lineChart1Data = [
            {
                data: [65, 59, 84, 84, 51, 55, 40],
                label: 'Series A'
            }
        ];
        this.lineChart1Labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
        this.lineChart1Options = {
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        gridLines: {
                            color: 'transparent',
                            zeroLineColor: 'transparent'
                        },
                        ticks: {
                            fontSize: 2,
                            fontColor: 'transparent',
                        }
                    }],
                yAxes: [{
                        display: false,
                        ticks: {
                            display: false,
                            min: 40 - 5,
                            max: 84 + 5,
                        }
                    }],
            },
            elements: {
                line: {
                    borderWidth: 1
                },
                point: {
                    radius: 4,
                    hitRadius: 10,
                    hoverRadius: 4,
                },
            },
            legend: {
                display: false
            }
        };
        this.lineChart1Colours = [
            {
                backgroundColor: this.brandPrimary,
                borderColor: 'rgba(255,255,255,.55)'
            }
        ];
        this.lineChart1Legend = false;
        this.lineChart1Type = 'line';
        // lineChart2
        this.lineChart2Data = [
            {
                data: [1, 18, 9, 17, 34, 22, 11],
                label: 'Series A'
            }
        ];
        this.lineChart2Labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
        this.lineChart2Options = {
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        gridLines: {
                            color: 'transparent',
                            zeroLineColor: 'transparent'
                        },
                        ticks: {
                            fontSize: 2,
                            fontColor: 'transparent',
                        }
                    }],
                yAxes: [{
                        display: false,
                        ticks: {
                            display: false,
                            min: 1 - 5,
                            max: 34 + 5,
                        }
                    }],
            },
            elements: {
                line: {
                    tension: 0.00001,
                    borderWidth: 1
                },
                point: {
                    radius: 4,
                    hitRadius: 10,
                    hoverRadius: 4,
                },
            },
            legend: {
                display: false
            }
        };
        this.lineChart2Colours = [
            {
                backgroundColor: this.brandInfo,
                borderColor: 'rgba(255,255,255,.55)'
            }
        ];
        this.lineChart2Legend = false;
        this.lineChart2Type = 'line';
        // lineChart3
        this.lineChart3Data = [
            {
                data: [78, 81, 80, 45, 34, 12, 40],
                label: 'Series A'
            }
        ];
        this.lineChart3Labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
        this.lineChart3Options = {
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        display: false
                    }],
                yAxes: [{
                        display: false
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                },
            },
            legend: {
                display: false
            }
        };
        this.lineChart3Colours = [
            {
                backgroundColor: 'rgba(255,255,255,.2)',
                borderColor: 'rgba(255,255,255,.55)',
            }
        ];
        this.lineChart3Legend = false;
        this.lineChart3Type = 'line';
        // barChart1
        this.barChart1Data = [
            {
                data: [78, 81, 80, 45, 34, 12, 40, 78, 81, 80, 45, 34, 12, 40, 12, 40],
                label: 'Series A'
            }
        ];
        this.barChart1Labels = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16'];
        this.barChart1Options = {
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        display: false,
                        barPercentage: 0.6,
                    }],
                yAxes: [{
                        display: false
                    }]
            },
            legend: {
                display: false
            }
        };
        this.barChart1Colours = [
            {
                backgroundColor: 'rgba(255,255,255,.3)',
                borderWidth: 0
            }
        ];
        this.barChart1Legend = false;
        this.barChart1Type = 'bar';
        // mainChart
        this.mainChartElements = 27;
        this.mainChartData1 = [];
        this.mainChartData2 = [];
        this.mainChartData3 = [];
        this.mainChartData = [
            {
                data: this.mainChartData1,
                label: 'Current'
            },
            {
                data: this.mainChartData2,
                label: 'Previous'
            },
            {
                data: this.mainChartData3,
                label: 'BEP'
            }
        ];
        /* tslint:disable:max-line-length */
        this.mainChartLabels = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Thursday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        /* tslint:enable:max-line-length */
        this.mainChartOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        gridLines: {
                            drawOnChartArea: false,
                        },
                        ticks: {
                            callback: function (value) {
                                return value.charAt(0);
                            }
                        }
                    }],
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            stepSize: Math.ceil(250 / 5),
                            max: 250
                        }
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: false
            }
        };
        this.mainChartColours = [
            {
                backgroundColor: this.convertHex(this.brandInfo, 10),
                borderColor: this.brandInfo,
                pointHoverBackgroundColor: '#fff'
            },
            {
                backgroundColor: 'transparent',
                borderColor: this.brandSuccess,
                pointHoverBackgroundColor: '#fff'
            },
            {
                backgroundColor: 'transparent',
                borderColor: this.brandDanger,
                pointHoverBackgroundColor: '#fff',
                borderWidth: 1,
                borderDash: [8, 5]
            }
        ];
        this.mainChartLegend = false;
        this.mainChartType = 'line';
        // social box charts
        this.socialChartData1 = [
            {
                data: [65, 59, 84, 84, 51, 55, 40],
                label: 'Facebook'
            }
        ];
        this.socialChartData2 = [
            {
                data: [1, 13, 9, 17, 34, 41, 38],
                label: 'Twitter'
            }
        ];
        this.socialChartData3 = [
            {
                data: [78, 81, 80, 45, 34, 12, 40],
                label: 'LinkedIn'
            }
        ];
        this.socialChartData4 = [
            {
                data: [35, 23, 56, 22, 97, 23, 64],
                label: 'Google+'
            }
        ];
        this.socialChartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
        this.socialChartOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        display: false,
                    }],
                yAxes: [{
                        display: false,
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: false
            }
        };
        this.socialChartColours = [
            {
                backgroundColor: 'rgba(255,255,255,.1)',
                borderColor: 'rgba(255,255,255,.55)',
                pointHoverBackgroundColor: '#fff'
            }
        ];
        this.socialChartLegend = false;
        this.socialChartType = 'line';
        // sparkline charts
        this.sparklineChartData1 = [
            {
                data: [35, 23, 56, 22, 97, 23, 64],
                label: 'Clients'
            }
        ];
        this.sparklineChartData2 = [
            {
                data: [65, 59, 84, 84, 51, 55, 40],
                label: 'Clients'
            }
        ];
        this.sparklineChartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
        this.sparklineChartOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        display: false,
                    }],
                yAxes: [{
                        display: false,
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: false
            }
        };
        this.sparklineChartDefault = [
            {
                backgroundColor: 'transparent',
                borderColor: '#d1d4d7',
            }
        ];
        this.sparklineChartPrimary = [
            {
                backgroundColor: 'transparent',
                borderColor: this.brandPrimary,
            }
        ];
        this.sparklineChartInfo = [
            {
                backgroundColor: 'transparent',
                borderColor: this.brandInfo,
            }
        ];
        this.sparklineChartDanger = [
            {
                backgroundColor: 'transparent',
                borderColor: this.brandDanger,
            }
        ];
        this.sparklineChartWarning = [
            {
                backgroundColor: 'transparent',
                borderColor: this.brandWarning,
            }
        ];
        this.sparklineChartSuccess = [
            {
                backgroundColor: 'transparent',
                borderColor: this.brandSuccess,
            }
        ];
        this.sparklineChartLegend = false;
        this.sparklineChartType = 'line';
    }
    // events
    DashboardComponent.prototype.chartClicked = function (e) {
        console.log(e);
    };
    DashboardComponent.prototype.chartHovered = function (e) {
        console.log(e);
    };
    // convert Hex to RGBA
    DashboardComponent.prototype.convertHex = function (hex, opacity) {
        hex = hex.replace('#', '');
        var r = parseInt(hex.substring(0, 2), 16);
        var g = parseInt(hex.substring(2, 4), 16);
        var b = parseInt(hex.substring(4, 6), 16);
        var rgba = 'rgba(' + r + ', ' + g + ', ' + b + ', ' + opacity / 100 + ')';
        return rgba;
    };
    DashboardComponent.prototype.random = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    DashboardComponent.prototype.ngOnInit = function () {
        // generate random values for mainChart
        for (var i = 0; i <= this.mainChartElements; i++) {
            this.mainChartData1.push(this.random(50, 200));
            this.mainChartData2.push(this.random(80, 100));
            this.mainChartData3.push(65);
        }
    };
    DashboardComponent.prototype.checkall = function () {
        $('input[type="checkbox"]').click(function () {
            /* Check all checkboxes if 'Select All' is clicked */
            if (this.id === 'selectAll') {
                var isChecked_1 = this.checked;
                $('.selectedId').each(function () {
                    this.checked = isChecked_1;
                });
            }
            /* Check / Uncheck 'Select All' checkbox if all the checkboxes are checked / unchecked */
            if ($('.selectedId:checked').length === $('.selectedId').length) {
                $('#selectAll').prop('checked', true);
            }
            else {
                $('#selectAll').prop('checked', false);
            }
            /* Toggle controls on the basis of selected checkboxes */
            if ($('.selectedId:checked').length >= 1) {
                $('#frame1').fadeIn('fast');
                $('#frame2').fadeIn('fast');
                $('#frame3').fadeIn('fast');
                $('#frame4').fadeIn('fast');
                $('#frame5').fadeIn('fast');
                $('#frame6').fadeIn('fast');
            }
            else {
                $('.toolbar1').fadeOut('fast');
                $('#frame1').fadeOut('fast');
                $('#frame2').fadeOut('fast');
                $('#frame3').fadeOut('fast');
                $('#frame4').fadeOut('fast');
                $('#frame5').fadeOut('fast');
                $('#frame6').fadeOut('fast');
            }
        });
    };
    // changeShowStatus() {
    //   $(document).ready(function () {
    //     $('#check1').click(function () {
    //       if (this.checked) {
    //         $('#frame1').show();
    //         const x = document.getElementById('snackbar0')
    //       x.className = 'show';
    //       setTimeout(function () { x.className = x.className.replace('show', ''); }, 3000);
    //       } else {
    //         $('#frame1').hide();
    //       }
    //     });
    //     $('#check2').click(function () {
    //       if (this.checked) {
    //         $('#frame2').show();
    //         const x = document.getElementById('snackbar1')
    //         x.className = 'show';
    //         setTimeout(function () { x.className = x.className.replace('show', ''); }, 3000);
    //       } else {
    //         $('#frame2').hide();
    //       }
    //     });
    //     $('#check3').click(function () {
    //       if (this.checked) {
    //         $('#frame3').show();
    //         const x = document.getElementById('snackbar2')
    //         x.className = 'show';
    //         setTimeout(function () { x.className = x.className.replace('show', ''); }, 3000);
    //       } else {
    //         $('#frame3').hide();
    //       }
    //     });
    //     $('#check4').click(function () {
    //       if (this.checked) {
    //         $('#frame4').show();
    //         const x = document.getElementById('snackbar3')
    //         x.className = 'show';
    //         setTimeout(function () { x.className = x.className.replace('show', ''); }, 3000);
    //       } else {
    //         $('#frame4').hide();
    //       }
    //     });
    //     $('#check5').click(function () {
    //       if (this.checked) {
    //         $('#frame5').show();
    //         const x = document.getElementById('snackbar4')
    //         x.className = 'show';
    //         setTimeout(function () { x.className = x.className.replace('show', ''); }, 3000);
    //       } else {
    //         $('#frame5').hide();
    //       }
    //     });
    //     $('#check6').click(function () {
    //       if (this.checked) {
    //         $('#frame6').show();
    //         const x = document.getElementById('snackbar5')
    //         x.className = 'show';
    //         setTimeout(function () { x.className = x.className.replace('show', ''); }, 3000);
    //       } else {
    //         $('#frame6').hide();
    //       }
    //     });
    //   });
    // }
    DashboardComponent.prototype.changeShowStatus = function () {
        // this.showHide = !this.showHide;
        //   console.log(this.showHide);
        $(document).ready(function () {
            $('#check1').click(function () {
                if (this.checked) {
                    $('#frame1').show();
                    // alert('We have selected Frame 1');
                }
                else {
                    $('#frame1').hide();
                }
            });
            $('#check2').click(function () {
                if (this.checked) {
                    $('#frame2').show();
                }
                else {
                    $('#frame2').hide();
                }
            });
            $('#check3').click(function () {
                if (this.checked) {
                    $('#frame3').show();
                }
                else {
                    $('#frame3').hide();
                }
            });
            $('#check4').click(function () {
                if (this.checked) {
                    $('#frame4').show();
                }
                else {
                    $('#frame4').hide();
                }
            });
            $('#check5').click(function () {
                if (this.checked) {
                    $('#frame5').show();
                }
                else {
                    $('#frame5').hide();
                }
            });
            $('#check6').click(function () {
                $('#frame6').toggle();
                if (this.checked) {
                    $('#frame6').show();
                }
                else {
                    $('#frame6').hide();
                }
            });
            // $('input[type="checkbox"]').click(function(){
            //     var inputValue = $(this).attr("value");
            //     $("." + inputValue).toggle();
            // });
        });
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("../../../../../src/app/views/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/dashboard/dashboard.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts__ = __webpack_require__("../../../../ng2-charts/ng2-charts.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_component__ = __webpack_require__("../../../../../src/app/views/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_routing_module__ = __webpack_require__("../../../../../src/app/views/dashboard/dashboard-routing.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__dashboard_routing_module__["a" /* DashboardRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts__["ChartsModule"],
                __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__dashboard_component__["a" /* DashboardComponent */]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ })

});
//# sourceMappingURL=dashboard.module.chunk.js.map