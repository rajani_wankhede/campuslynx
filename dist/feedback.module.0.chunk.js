webpackJsonp(["feedback.module.0"],{

/***/ "../../../../../src/app/views/student/feedback/feedback-history/feedback-history.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n    <div class=\"row\">\n        <!--/.col-->\n        <div class=\"col-sm-12\">\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <strong>  FeedBack Form</strong>\n                    <small></small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">\n\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-1 col-form-label\"> Name:</label>\n                                            <div class=\"col-md-3\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Name\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\"> Program Branch:</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"branch\">\n                                            </div>\n                                            <label class=\"col-md-2 col-form-label\" for=\"text-input\">Semester</label>\n                                            <div class=\"col-md-2\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"sem\">\n                                            </div>\n\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\">SRC Type:*</label>\n                                            <div class=\"col-md-3\">\n                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">Faculty Wise\n                                            </div>\n\n                                            <div class=\"col-md-3\">\n                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">Course Wise\n                                            </div>\n\n                                            <div class=\"col-md-3\">\n                                                <input type=\"radio\" id=\"radio1\" name=\"radios\" value=\"option1\">Other/Topic Wise\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group row\">\n                                            <label class=\"col-md-2 col-form-label\"> SRC For :*</label>\n                                            <div class=\"col-md-6\">\n                                                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\" placeholder=\"Select SRC For\">\n                                            </div>\n                                            <div class=\"col-md-2\">\n                                                <button> GO</button>\n                                            </div>\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <!--/.col-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/student/feedback/feedback-history/feedback-history.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/feedback/feedback-history/feedback-history.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackHistoryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FeedbackHistoryComponent = (function () {
    function FeedbackHistoryComponent() {
    }
    FeedbackHistoryComponent.prototype.ngOnInit = function () {
    };
    FeedbackHistoryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-feedback-history',
            template: __webpack_require__("../../../../../src/app/views/student/feedback/feedback-history/feedback-history.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/feedback/feedback-history/feedback-history.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FeedbackHistoryComponent);
    return FeedbackHistoryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/feedback/feedback-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__feedback_history_feedback_history_component__ = __webpack_require__("../../../../../src/app/views/student/feedback/feedback-history/feedback-history.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__feedbackform_feedbackform_component__ = __webpack_require__("../../../../../src/app/views/student/feedback/feedbackform/feedbackform.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        data: {
            title: 'Exam'
        },
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_3__feedbackform_feedbackform_component__["a" /* FeedbackformComponent */],
                data: {
                    title: 'Feedback form'
                }
            },
            {
                path: 'history',
                component: __WEBPACK_IMPORTED_MODULE_2__feedback_history_feedback_history_component__["a" /* FeedbackHistoryComponent */],
                data: {
                    title: 'Feedbackhistory'
                }
            },
        ]
    }
];
var FeedbackRoutingModule = (function () {
    function FeedbackRoutingModule() {
    }
    FeedbackRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], FeedbackRoutingModule);
    return FeedbackRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/feedback/feedback.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedbackModule", function() { return FeedbackModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_tabs__ = __webpack_require__("../../../../ngx-bootstrap/tabs/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__feedback_history_feedback_history_component__ = __webpack_require__("../../../../../src/app/views/student/feedback/feedback-history/feedback-history.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__feedbackform_feedbackform_component__ = __webpack_require__("../../../../../src/app/views/student/feedback/feedbackform/feedbackform.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__feedback_routing_module__ = __webpack_require__("../../../../../src/app/views/student/feedback/feedback-routing.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// Forms Component
//import { FormsComponent } from './forms.component';

// Modal Component

// Tabs Component



// Components Routing

var FeedbackModule = (function () {
    function FeedbackModule() {
    }
    FeedbackModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_6__feedback_routing_module__["a" /* FeedbackRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap_tabs__["a" /* TabsModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__feedbackform_feedbackform_component__["a" /* FeedbackformComponent */],
                __WEBPACK_IMPORTED_MODULE_4__feedback_history_feedback_history_component__["a" /* FeedbackHistoryComponent */]
            ]
        })
    ], FeedbackModule);
    return FeedbackModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/student/feedback/feedbackform/feedbackform.component.html":
/***/ (function(module, exports) {

module.exports = "<h4>feedback works</h4>"

/***/ }),

/***/ "../../../../../src/app/views/student/feedback/feedbackform/feedbackform.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/views/student/feedback/feedbackform/feedbackform.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackformComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FeedbackformComponent = (function () {
    function FeedbackformComponent() {
    }
    FeedbackformComponent.prototype.ngOnInit = function () {
    };
    FeedbackformComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-feedbackform',
            template: __webpack_require__("../../../../../src/app/views/student/feedback/feedbackform/feedbackform.component.html"),
            styles: [__webpack_require__("../../../../../src/app/views/student/feedback/feedbackform/feedbackform.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FeedbackformComponent);
    return FeedbackformComponent;
}());



/***/ })

});
//# sourceMappingURL=feedback.module.0.chunk.js.map