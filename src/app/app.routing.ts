import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { P500Component } from './views/pages/500.component';
// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'pages/login',
    pathMatch: 'full',
  },
  
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'student/personal',
        loadChildren: './views/student/personal/personal.module#PersonalModule'
      },
      {
        path: 'student/academic',
        loadChildren: './views/student/academic/academic.module#AcademicModule'
      },
      {
        path: 'student/exam',
        loadChildren: './views/student/exam/exam.module#ExamModule'
      },
      {
        path: 'student/feedback',
        loadChildren: './views/student/feedback/feedback.module#FeedbackModule'
      },
      {
        path: 'employee/personal',
        loadChildren: './views/employee/personal/personal.module#PersonalModule'
      },
      {
        path: 'employee/academic',
        loadChildren: './views/employee/academic/academic.module#AcademicModule'
      },
      {
        path: 'employee/exam',
        loadChildren: './views/employee/exam/exam.module#ExamModule'
      },
      {
        path: 'employee/feedback',
        loadChildren: './views/employee/feedback/feedback.module#FeedbackModule'
      },
      // {
      //   path: 'components',
      //   loadChildren: './views/components/components.module#ComponentsModule'
      // },

      // {
      //   path: 'icons',
      //   loadChildren: './views/icons/icons.module#IconsModule'
      // },
      // {
      //   path: 'widgets',
      //   loadChildren: './views/widgets/widgets.module#WidgetsModule'
      // },
      // {
      //   path: 'charts',
      //   loadChildren: './views/chartjs/chartjs.module#ChartJSModule'
      // }
    ]
  },
  {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: './views/pages/pages.module#PagesModule',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),FormsModule,CommonModule],
  exports: [RouterModule,FormsModule,CommonModule]
})
export class AppRoutingModule { }
