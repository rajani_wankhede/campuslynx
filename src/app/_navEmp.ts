export const navigationEmp = [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: ''
      }
    },
    {
      title: true,
      name: 'UI elements'
    },
    {
      name: 'Personal',
      url: '/employee/personal',
      icon: 'icon-user',
      children: [
        {
          name: 'Personal Infomation',
          url: '/employee/personal',
          icon: 'icon-puzzle'
        },
               {
            name: 'View Self Tax Declaration Form',
            url: '/employee/personal/ViewSelfTaxDeclarationForm',
            icon: 'icon-puzzle'
          },
          {
            name: 'Facaulty Information Updation',
            url: '/employee/personal/FacultyInfoUpdation',
            icon: 'icon-puzzle'
          },
          {
            name: 'Edit Contact Information by Staff',
            url: '/employee/personal/EditContactInformation',
            icon: 'icon-puzzle'
          },
          {
            name: 'Purchase Requisition',
            url: '/employee/personal/PurchaseRequisition',
            icon: 'icon-puzzle'
          },
          {
            name: 'Staff File Download',
            url: '/employee/personal/StaffFileDownload',
            icon: 'icon-puzzle'
          },
          {
            name: 'Staff File Upload',
            url: '/employee/personal/StaffFileUpload',
            icon: 'icon-puzzle'
          },
          {
            name: 'Self Tax Declaration',
            url: '/employee/personal/SelfTaxDeclaration',
            icon: 'icon-puzzle'
          },
          {
            name: 'Self Salary Slip',
            url: '/employee/personal/SelfSalarySlip',
            icon: 'icon-puzzle'
          },
          {
            name: 'Employee Self Vacation Detail Reportn',
            url: '/employee/personal/SelfVacationDetailReport',
            icon: 'icon-puzzle'
          },
          {
            name: 'Leave Query Self',
            url: '/employee/personal/LeaveQueryself',
            icon: 'icon-puzzle'
          },
          {
            name: 'My DR/CR Advice',
            url: '/employee/personal/myDrCrAdvice',
            icon: 'icon-puzzle'
          },
      ]
    },
    {
      name: 'Academic',
      url: '/employee/academic',
      icon: 'icon-layers',
      children: [
              {
          name: 'My Class TimeTable',
          url: '/employee/academic',
          icon: 'icon-book-open'
        },
        {
          name: 'Student Attendance Entry New',
          url: '/employee/academic/Studentattendanceentry',
          icon: 'icon-notebook'
        }
        , {
            name: 'Institute Wise Student Information',
            url: '/employee/academic/InstitutewiseStudentInfo',
            icon: 'icon-pencil'
          }
        , {
          name: 'Student Attendance Muster',
          url: '/employee/academic/StudentAttendanceMuster',
          icon: 'icon-list'
        }, {
          name: 'Student Attendance Extra class ',
          url: '/employee/academic/StudentAttendanceExtraclass',
          icon: 'icon-support'
        }

      ]
    },
    {
      name: 'Exam Info',
      url: '/emploee/exam',
      icon: 'icon-graduation',
      children: [
        {
          name: 'Marks Entry',
          url: '/employee/exam',
          icon: 'icon-hourglass',
          // badge: {
          //   variant: 'secondary',
          //   text: '4.7'
          // }
        },
        {
          name: 'View Event Subject Marks',
          url: '/employee/exam/viewEventSubjectMarks',
          icon: 'icon-docs'
        }, {
          name: 'External Marks Entry Permission',
          url: '/employee/exam/ExternalmarksEntryPermission',
          icon: 'icon-target'
        }
        , {
            name: 'Marks Entry Dificiency Report',
            url: '/employee/exam/MarksEntryDificiencyReportt',
            icon: 'icon-target'
          }
      ]
    },
    {
      name: 'Feedback',
      url: '/employee/feedback',
      icon: 'icon-star',
      children: [
        {
          name: 'Advisors Feddback For Student',
          url: '/employee/feedback',
          icon: 'icon-star',
          // badge: {
          //   variant: 'secondary',
          //   text: '4.7'
          // }
        },
        {
          name: 'Student Feedback Sent History',
          url: '/employee/feedback/StudentfeedbackSentHistory',
          icon: 'icon-star'
        },
        {
          name: 'Student Feedback View by Advisor',
          url: '/employee/feedback/StudentfeedbackViewbyAdvisor',
          icon: 'icon-star'
        },
        {
          name: 'Student Feedback For Me',
          url: '/employee/feedback/StudentfeedbackForMe',
          icon: 'icon-star'
        }
        ,{
          name: 'Student Feedback For Faculty',
          url: '/employee/feedback/StudentfeedbackForFaculty',
          icon: 'icon-star'
        },{
          name: 'Student Attendance Entry Schedular New',
          url: '/employee/feedback/StudentAttendanceentrySchedular',
          icon: 'icon-star'
        }
      ]
    },
    // {
    //   divider: true
    // },
    // {
    //   title: true,
    //   name: 'Extras',
    // },
    // {
    //   name: 'Pages',
    //   url: '/pages',
    //   icon: 'icon-star',
    //   children: [
    //     {
    //       name: 'Login',
    //       url: '/pages/login',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Register',
    //       url: '/pages/register',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Error 404',
    //       url: '/pages/404',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Error 500',
    //       url: '/pages/500',
    //       icon: 'icon-star'
    //     }
    //   ]
    // },
    // {
    //   name: 'Download CoreUI',
    //   url: 'http://coreui.io/angular/',
    //   icon: 'icon-cloud-download',
    //   class: 'mt-auto',
    //   variant: 'success'
    // },
    // {
    //   name: 'Try CoreUI PRO',
    //   url: 'http://coreui.io/pro/angular/',
    //   icon: 'icon-layers',
    //   variant: 'danger'
    // }
  ];
