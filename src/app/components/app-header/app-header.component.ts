import { Component } from '@angular/core';
import { LoginComponent } from '../../views/pages/login.component';
declare var $: any;
import { LoginService } from '../../views/services/login.service';
import { Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html'
})
export class AppHeaderComponent {
    adminname = '';
    constructor(private loginservice: LoginService, private router: Router) {
    }
 ngOnInit () {
  this.adminname = window.localStorage.getItem('adminname');
//  this._appheaderservice.getLogos()
//  .subscribe(images =>this.logos=images);
    var event= localStorage.getItem('event');
    console.log(event);
     if(event == "notselected")
        {
         // require("style-loader!assets/css/style1.css");
           $("#logo0").show();
           $("#logo2").hide();
           $("#logo3").hide();
          $("#logo1").hide();
          
        }
      if(event == "pune")
        {
         // require("style-loader!assets/css/style1.css");
           $("#logo0").hide();
           $("#logo2").hide();
           $("#logo3").hide();
          $("#logo1").show();
          
        }
        else if(event == "delhi")
        {
          //require("style-loader!assets/css/style2.css");
         
           $("#logo2").show();
            $("#logo1").hide();
            $("#logo3").hide();
            $("#logo0").hide();
         }
        else if(event == "mumbai")
        {
         // require("style-loader!assets/css/style3.css");
          $("#logo0").hide();
          $("#logo2").hide();
          $("#logo1").hide();
          $("#logo3").show();
        }
}
logout() {
  this.loginservice.logout().subscribe(result => {
    console.log(result);
    if (result.err === false) {
      // this.toastr.success('Logged out Successful!', 'Success!');
      localStorage.removeItem("Token");
      localStorage.removeItem("id");
      localStorage.removeItem("username");
          this.router.navigate(['']);
    }
    else {

      //  this.toastr.error('Retry again!', 'Oops.. Logout failed!');
      //this.router.navigate(['/login']);
    }
  });
}

}
