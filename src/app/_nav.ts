export const navigation = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: ''
    }
  },
  {
    title: true,
    name: 'UI elements'
  },
  {
    name: 'Personal',
    url: '/student/personal',
    icon: 'icon-user',
    children: [
      {
        name: 'Personal Infomation',
        url: '/student/personal',
        icon: 'icon-puzzle'
      },
      {
        name: 'My DR/CR Advice',
        url: '/student/personal/drcradvice',
        icon: 'icon-puzzle'
      },

    ]
  },
  {
    name: 'Academic',
    url: '/student/academic',
    icon: 'icon-layers',
    children: [
      {
        name: 'Elective choice',
        url: '/student/academic',
        icon: 'icon-layers',
        // badge: {
        //   variant: 'secondary',
        //   text: '4.7'
        // }
      },
      {
        name: 'My Class TimeTable',
        url: '/student/academic/ClassTimetable',
        icon: 'icon-book-open'
      }, {
        name: 'Information updation',
        url: '/student/academic/informationUpdate',
        icon: 'icon-pencil'
      },
      {
        name: 'Course Registration',
        url: '/student/academic/CourseRegistration',
        icon: 'icon-notebook'
      }
      , {
        name: 'My Class Attendance',
        url: '/student/academic/ClassAttendance',
        icon: 'icon-list'
      }, {
        name: 'Desciplinary Action ',
        url: '/student/academic/DesciplinaryAction',
        icon: 'icon-support'
      }, {
        name: 'Fee Details',
        url: '/student/academic/FeeDetails',
        icon: 'icon-wallet'
      }

    ]
  },
  {
    name: 'Exam Info',
    url: '/student/exam',
    icon: 'icon-graduation',
    children: [
      {
        name: 'Examination Schedule',
        url: '/student/exam',
        icon: 'icon-hourglass',
        // badge: {
        //   variant: 'secondary',
        //   text: '4.7'
        // }
      },
      {
        name: 'Internal Exam Eventwise Marks',
        url: '/student/exam/internalExamEventMarks',
        icon: 'icon-docs'
      }, {
        name: 'Results',
        url: '/student/exam/result',
        icon: 'icon-target'
      }

    ]
  },
  {
    name: 'Feedback',
    url: '/student/feedback',
    icon: 'icon-star',
    children: [
      {
        name: 'Feedback form',
        url: '/student/feedback',
        icon: 'icon-star',
        // badge: {
        //   variant: 'secondary',
        //   text: '4.7'
        // }
      },
      {
        name: 'Student feedback for faculties',
        url: '/student/feedback/history',
        icon: 'icon-star'
      }
    ]
  },
  // {
  //   divider: true
  // },
  // {
  //   title: true,
  //   name: 'Extras',
  // },
  // {
  //   name: 'Pages',
  //   url: '/pages',
  //   icon: 'icon-star',
  //   children: [
  //     {
  //       name: 'Login',
  //       url: '/pages/login',
  //       icon: 'icon-star'
  //     },
  //     {
  //       name: 'Register',
  //       url: '/pages/register',
  //       icon: 'icon-star'
  //     },
  //     {
  //       name: 'Error 404',
  //       url: '/pages/404',
  //       icon: 'icon-star'
  //     },
  //     {
  //       name: 'Error 500',
  //       url: '/pages/500',
  //       icon: 'icon-star'
  //     }
  //   ]
  // },
  // {
  //   name: 'Download CoreUI',
  //   url: 'http://coreui.io/angular/',
  //   icon: 'icon-cloud-download',
  //   class: 'mt-auto',
  //   variant: 'success'
  // },
  // {
  //   name: 'Try CoreUI PRO',
  //   url: 'http://coreui.io/pro/angular/',
  //   icon: 'icon-layers',
  //   variant: 'danger'
  // }
];
