import { NgModule } from '@angular/core';


// Forms Component
//import { FormsComponent } from './forms.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

// Components Routing
import { ExamRoutingModule } from './exam-routing.module';
import { ExternalmarksEntryPermissionComponent } from 'app/views/employee/exam/externalmarks-entry-permission/externalmarks-entry-permission.component';
import { MarksEntryComponent } from 'app/views/employee/exam/marks-entry/marks-entry.component';
import { MarksEntryDificiencyReportComponent } from 'app/views/employee/exam/marks-entry-dificiency-report/marks-entry-dificiency-report.component';
import { ViewEventSubjectMarksComponent } from 'app/views/employee/exam/view-event-subject-marks/view-event-subject-marks.component';

@NgModule({
  imports: [
    ExamRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule
  ],
  declarations: [
 ExternalmarksEntryPermissionComponent,
 MarksEntryComponent,
 MarksEntryDificiencyReportComponent,
 ViewEventSubjectMarksComponent
  ]
})
export class ExamModule { }
