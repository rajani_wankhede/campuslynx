import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarksEntryComponent } from 'app/views/employee/exam/marks-entry/marks-entry.component';
import { ViewEventSubjectMarksComponent } from 'app/views/employee/exam/view-event-subject-marks/view-event-subject-marks.component';
import { ExternalmarksEntryPermissionComponent } from 'app/views/employee/exam/externalmarks-entry-permission/externalmarks-entry-permission.component';
import { MarksEntryDificiencyReportComponent } from 'app/views/employee/exam/marks-entry-dificiency-report/marks-entry-dificiency-report.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Exam'
    },
    children: [
      {
        path: '',
        component: MarksEntryComponent,
        data: {
          title: 'Marks Entry'
        }
      },
      {
        path: 'viewEventSubjectMarks',
        component: ViewEventSubjectMarksComponent,
        data: {
          title: 'View Event Subject Marks'
        }
      },
      {
        path: 'ExternalmarksEntryPermission',
        component: ExternalmarksEntryPermissionComponent,
        data: {
          title: 'External Marks Entry Permission'
        }
      },
      {
        path: 'MarksEntryDificiencyReportt',
        component: MarksEntryDificiencyReportComponent,
        data: {
          title: 'Marks Entry Dificiency Report'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamRoutingModule {}
