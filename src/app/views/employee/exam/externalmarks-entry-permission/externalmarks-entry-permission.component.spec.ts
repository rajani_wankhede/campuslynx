import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalmarksEntryPermissionComponent } from './externalmarks-entry-permission.component';

describe('ExternalmarksEntryPermissionComponent', () => {
  let component: ExternalmarksEntryPermissionComponent;
  let fixture: ComponentFixture<ExternalmarksEntryPermissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalmarksEntryPermissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalmarksEntryPermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
