import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarksEntryDificiencyReportComponent } from './marks-entry-dificiency-report.component';

describe('MarksEntryDificiencyReportComponent', () => {
  let component: MarksEntryDificiencyReportComponent;
  let fixture: ComponentFixture<MarksEntryDificiencyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarksEntryDificiencyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarksEntryDificiencyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
