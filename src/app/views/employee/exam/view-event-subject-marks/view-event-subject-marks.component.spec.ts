import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEventSubjectMarksComponent } from './view-event-subject-marks.component';

describe('ViewEventSubjectMarksComponent', () => {
  let component: ViewEventSubjectMarksComponent;
  let fixture: ComponentFixture<ViewEventSubjectMarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEventSubjectMarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEventSubjectMarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
