import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacultlyInfoUpdationComponent } from './facultly-info-updation.component';

describe('FacultlyInfoUpdationComponent', () => {
  let component: FacultlyInfoUpdationComponent;
  let fixture: ComponentFixture<FacultlyInfoUpdationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultlyInfoUpdationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacultlyInfoUpdationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
