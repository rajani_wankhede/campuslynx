import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfSalaryslipComponent } from './self-salaryslip.component';

describe('SelfSalaryslipComponent', () => {
  let component: SelfSalaryslipComponent;
  let fixture: ComponentFixture<SelfSalaryslipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfSalaryslipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfSalaryslipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
