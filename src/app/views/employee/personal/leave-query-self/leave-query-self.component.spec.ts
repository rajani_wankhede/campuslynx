import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveQuerySelfComponent } from './leave-query-self.component';

describe('LeaveQuerySelfComponent', () => {
  let component: LeaveQuerySelfComponent;
  let fixture: ComponentFixture<LeaveQuerySelfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveQuerySelfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveQuerySelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
