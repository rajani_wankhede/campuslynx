import { NgModule } from '@angular/core';

import { PersonalInfoComponent } from './personal-info/personal-info.component';

// Forms Component
//import { FormsComponent } from './forms.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

// Components Routing
import { PersonalRoutingModule } from './personal-routing.module';
import { FacultlyInfoUpdationComponent } from './facultly-info-updation/facultly-info-updation.component';
import { EditContactInfoComponent } from './edit-contact-info/edit-contact-info.component';
import { PurchaseRequisitionComponent } from './purchase-requisition/purchase-requisition.component';
import { StaffFileUploadComponent } from './staff-file-upload/staff-file-upload.component';
import { StaffFileDownloadComponent } from './staff-file-download/staff-file-download.component';
import { SelfSalaryslipComponent } from './self-salaryslip/self-salaryslip.component';
import { SelfVacationdetailReportComponent } from './self-vacationdetail-report/self-vacationdetail-report.component';
import { LeaveQuerySelfComponent } from './leave-query-self/leave-query-self.component';
import { MyDrCrAdviceComponent } from './my-dr-cr-advice/my-dr-cr-advice.component';
import { SelfTaxDeclarationformComponent } from 'app/views/employee/personal/self-tax-declarationform/self-tax-declarationform.component';
import { SelfTaxDeclarationComponent } from 'app/views/employee/personal/self-tax-declaration/self-tax-declaration.component';

@NgModule({
  imports: [
    PersonalRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule
  ],
  declarations: [
   PersonalInfoComponent,
   FacultlyInfoUpdationComponent,
   EditContactInfoComponent,
   PurchaseRequisitionComponent,
   StaffFileUploadComponent,
   StaffFileDownloadComponent,
   SelfSalaryslipComponent,
   SelfVacationdetailReportComponent,
   LeaveQuerySelfComponent,
   MyDrCrAdviceComponent,
   SelfTaxDeclarationComponent,
   SelfTaxDeclarationformComponent
 
  ]
})
export class PersonalModule { }
