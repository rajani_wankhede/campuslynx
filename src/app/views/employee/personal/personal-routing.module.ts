import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { FacultlyInfoUpdationComponent } from './facultly-info-updation/facultly-info-updation.component';
import { EditContactInfoComponent } from './edit-contact-info/edit-contact-info.component';
import { PurchaseRequisitionComponent } from './purchase-requisition/purchase-requisition.component';
import { StaffFileUploadComponent } from './staff-file-upload/staff-file-upload.component';
import { StaffFileDownloadComponent } from './staff-file-download/staff-file-download.component';
import { SelfSalaryslipComponent } from './self-salaryslip/self-salaryslip.component';
import { SelfVacationdetailReportComponent } from './self-vacationdetail-report/self-vacationdetail-report.component';
import { LeaveQuerySelfComponent } from './leave-query-self/leave-query-self.component';
import { MyDrCrAdviceComponent } from './my-dr-cr-advice/my-dr-cr-advice.component';
import { SelfTaxDeclarationformComponent } from 'app/views/employee/personal/self-tax-declarationform/self-tax-declarationform.component';
import { SelfTaxDeclarationComponent } from 'app/views/employee/personal/self-tax-declaration/self-tax-declaration.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Personal Information'
    },
    children: [
      {
        path: '',
        component: PersonalInfoComponent,
        data: {
          title: 'Personal Information'
        }
      },
      {
        path: 'ViewSelfTaxDeclarationForm',
        component: SelfTaxDeclarationformComponent,
        data: {
          title: 'View Self Tax Declaration Form'
        }
      },
      {
        path: 'FacultyInfoUpdation',
        component: FacultlyInfoUpdationComponent,
        data: {
          title: 'Facaulty Information Updation'
        }
      },
      {
        path: 'EditContactInformation',
        component: EditContactInfoComponent,
        data: {
          title: 'Edit Contact Information by Staff'
        }
      },
      {
        path: 'PurchaseRequisition',
        component: PurchaseRequisitionComponent,
        data: {
          title: 'Purchase Requisition'
        }
      },
      {
        path: 'StaffFileDownload',
        component: StaffFileDownloadComponent,
        data: {
          title: 'Staff File Download'
        }
      },
      {
        path: 'StaffFileUpload',
        component: StaffFileUploadComponent,
        data: {
          title: 'Staff File Upload'
        }
      },
      {
        path: 'SelfTaxDeclaration',
        component: SelfTaxDeclarationComponent,
        data: {
          title: 'Self Tax Declaration'
        }
      },
      {
        path: 'SelfSalarySlip',
        component: SelfSalaryslipComponent,
        data: {
          title: 'Self Salary Slip'
        }
      },
      {
        path: 'SelfVacationDetailReport',
        component: SelfVacationdetailReportComponent,
        data: {
          title: 'Employee Self Vacation Detail Report'
        }
      },
      {
        path: 'LeaveQueryself',
        component: LeaveQuerySelfComponent,
        data: {
          title: 'Leave Query Self'
        }
      },
      {
        path: 'myDrCrAdvice',
        component: MyDrCrAdviceComponent,
        data: {
          title: 'My DR/CR Advice'
        }
      },
     
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule,CommonModule],
  exports: [RouterModule,FormsModule,CommonModule]
})
export class PersonalRoutingModule {}
