import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfTaxDeclarationformComponent } from './self-tax-declarationform.component';

describe('SelfTaxDeclarationformComponent', () => {
  let component: SelfTaxDeclarationformComponent;
  let fixture: ComponentFixture<SelfTaxDeclarationformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfTaxDeclarationformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfTaxDeclarationformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
