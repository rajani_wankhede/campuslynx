import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfTaxDeclarationComponent } from './self-tax-declaration.component';

describe('SelfTaxDeclarationComponent', () => {
  let component: SelfTaxDeclarationComponent;
  let fixture: ComponentFixture<SelfTaxDeclarationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfTaxDeclarationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfTaxDeclarationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
