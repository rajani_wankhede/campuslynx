import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffFileDownloadComponent } from './staff-file-download.component';

describe('StaffFileDownloadComponent', () => {
  let component: StaffFileDownloadComponent;
  let fixture: ComponentFixture<StaffFileDownloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffFileDownloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffFileDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
