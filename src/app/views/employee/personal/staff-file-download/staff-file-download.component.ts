import { Component, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
@Component({
  selector: 'app-staff-file-download',
  templateUrl: './staff-file-download.component.html',
  styleUrls: ['./staff-file-download.component.scss']
})
export class StaffFileDownloadComponent implements OnInit {
selectedtype:any='Self';
public myModal;
public largeModal;
public smallModal;
public primaryModal;
public successModal;
public warningModal;
public dangerModal;
public infoModal;
  constructor() { }

  ngOnInit() {
  }
  changetype(current){
    this.selectedtype=current;
  }


}
