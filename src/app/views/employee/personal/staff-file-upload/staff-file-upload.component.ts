import { Component, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
@Component({
  selector: 'app-staff-file-upload',
  templateUrl: './staff-file-upload.component.html',
  styleUrls: ['./staff-file-upload.component.scss']
})
export class StaffFileUploadComponent implements OnInit {
  public myModal;
  public largeModal;
  public smallModal;
  public primaryModal;
  public successModal;
  public warningModal;
  public dangerModal;
  public infoModal;
  constructor() { }

  ngOnInit() {
  }

}
