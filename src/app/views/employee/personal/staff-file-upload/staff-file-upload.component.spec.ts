import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffFileUploadComponent } from './staff-file-upload.component';

describe('StaffFileUploadComponent', () => {
  let component: StaffFileUploadComponent;
  let fixture: ComponentFixture<StaffFileUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffFileUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffFileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
