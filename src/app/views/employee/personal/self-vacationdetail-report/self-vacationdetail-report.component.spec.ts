import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfVacationdetailReportComponent } from './self-vacationdetail-report.component';

describe('SelfVacationdetailReportComponent', () => {
  let component: SelfVacationdetailReportComponent;
  let fixture: ComponentFixture<SelfVacationdetailReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfVacationdetailReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfVacationdetailReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
