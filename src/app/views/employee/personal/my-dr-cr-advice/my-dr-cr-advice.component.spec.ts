import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDrCrAdviceComponent } from './my-dr-cr-advice.component';

describe('MyDrCrAdviceComponent', () => {
  let component: MyDrCrAdviceComponent;
  let fixture: ComponentFixture<MyDrCrAdviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDrCrAdviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDrCrAdviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
