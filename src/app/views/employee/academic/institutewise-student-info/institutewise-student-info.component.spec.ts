import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutewiseStudentInfoComponent } from './institutewise-student-info.component';

describe('InstitutewiseStudentInfoComponent', () => {
  let component: InstitutewiseStudentInfoComponent;
  let fixture: ComponentFixture<InstitutewiseStudentInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitutewiseStudentInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutewiseStudentInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
