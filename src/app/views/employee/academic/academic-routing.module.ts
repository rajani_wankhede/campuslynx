import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstitutewiseStudentInfoComponent } from './institutewise-student-info/institutewise-student-info.component';
import { ClassTimetableComponent } from './class-timetable/class-timetable.component';
import { StudentAttendanceEntryComponent } from './student-attendance-entry/student-attendance-entry.component';
import { StudentAttendanceExtraclassComponent } from './student-attendance-extraclass/student-attendance-extraclass.component';
import { StudentAttendanceMusterComponent } from './student-attendance-muster/student-attendance-muster.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Academic'
    },
    children: [
        {
            path: '',
            component: ClassTimetableComponent,
            data: {
              title: 'My class time Table'
            }
          },
      {
        path: 'InstitutewiseStudentInfo',
        component: InstitutewiseStudentInfoComponent,
        data: {
          title: 'Institute Wise Student Information'
        }
      },
      
      {
        path: 'Studentattendanceentry',
        component: StudentAttendanceEntryComponent,
        data: {
          title: 'Student Attendance Entry'
        }
      },
      {
        path: 'StudentAttendanceMuster',
        component: StudentAttendanceMusterComponent,
        data: {
          title: 'Student Attendance Muster'
        }
      },
      {
        path: 'StudentAttendanceExtraclass',
        component: StudentAttendanceExtraclassComponent,
        data: {
          title: 'Student Attendance Extra class'
        }
      },
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcademicRoutingModule {}
