import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAttendanceEntryComponent } from './student-attendance-entry.component';

describe('StudentAttendanceEntryComponent', () => {
  let component: StudentAttendanceEntryComponent;
  let fixture: ComponentFixture<StudentAttendanceEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentAttendanceEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentAttendanceEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
