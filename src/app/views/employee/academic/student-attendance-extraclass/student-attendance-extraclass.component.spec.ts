import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAttendanceExtraclassComponent } from './student-attendance-extraclass.component';

describe('StudentAttendanceExtraclassComponent', () => {
  let component: StudentAttendanceExtraclassComponent;
  let fixture: ComponentFixture<StudentAttendanceExtraclassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentAttendanceExtraclassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentAttendanceExtraclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
