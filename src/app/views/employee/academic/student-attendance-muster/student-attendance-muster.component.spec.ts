import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAttendanceMusterComponent } from './student-attendance-muster.component';

describe('StudentAttendanceMusterComponent', () => {
  let component: StudentAttendanceMusterComponent;
  let fixture: ComponentFixture<StudentAttendanceMusterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentAttendanceMusterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentAttendanceMusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
