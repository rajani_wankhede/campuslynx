import { NgModule } from '@angular/core';


import { InstitutewiseStudentInfoComponent } from './institutewise-student-info/institutewise-student-info.component';
import { ClassTimetableComponent } from './class-timetable/class-timetable.component';
import { StudentAttendanceEntryComponent } from './student-attendance-entry/student-attendance-entry.component';
import { StudentAttendanceExtraclassComponent } from './student-attendance-extraclass/student-attendance-extraclass.component';
import { StudentAttendanceMusterComponent } from './student-attendance-muster/student-attendance-muster.component';

// Forms Component
//import { FormsComponent } from './forms.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

// Components Routing
import { AcademicRoutingModule } from './academic-routing.module';

@NgModule({
  imports: [
    AcademicRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule
  ],
  declarations: [
  InstitutewiseStudentInfoComponent,
  ClassTimetableComponent,
  StudentAttendanceEntryComponent,
  StudentAttendanceExtraclassComponent,
  StudentAttendanceMusterComponent
  ]
})
export class AcademicModule { }
