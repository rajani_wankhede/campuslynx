import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAttendanceentrySchedularComponent } from './student-attendanceentry-schedular.component';

describe('StudentAttendanceentrySchedularComponent', () => {
  let component: StudentAttendanceentrySchedularComponent;
  let fixture: ComponentFixture<StudentAttendanceentrySchedularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentAttendanceentrySchedularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentAttendanceentrySchedularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
