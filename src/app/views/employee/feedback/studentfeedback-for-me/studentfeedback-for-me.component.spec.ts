import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentfeedbackForMeComponent } from './studentfeedback-for-me.component';

describe('StudentfeedbackForMeComponent', () => {
  let component: StudentfeedbackForMeComponent;
  let fixture: ComponentFixture<StudentfeedbackForMeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentfeedbackForMeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentfeedbackForMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
