import { NgModule } from '@angular/core';

// Forms Component
//import { FormsComponent } from './forms.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AdvisorsfeddbackForStudentComponent } from './advisorsfeddback-for-student/advisorsfeddback-for-student.component';
import { StudentfeedbackSentHistoryComponent } from './studentfeedback-sent-history/studentfeedback-sent-history.component';
import { StudentfeedbackViewbyAdvisorComponent } from './studentfeedback-viewby-advisor/studentfeedback-viewby-advisor.component';
import { StudentfeedbackForMeComponent } from './studentfeedback-for-me/studentfeedback-for-me.component';
import { StudentfeedbackForFacultyComponent } from './studentfeedback-for-faculty/studentfeedback-for-faculty.component';
import { StudentAttendanceentrySchedularComponent } from './student-attendanceentry-schedular/student-attendanceentry-schedular.component';
import { FeedbackRoutingModule } from 'app/views/employee/feedback/feedback-routing.module';

@NgModule({
  imports: [
  FeedbackRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule
  ],
  declarations: [
 
  AdvisorsfeddbackForStudentComponent,
 
  StudentfeedbackSentHistoryComponent,
 
  StudentfeedbackViewbyAdvisorComponent,
 
  StudentfeedbackForMeComponent,
 
  StudentfeedbackForFacultyComponent,
 
  StudentAttendanceentrySchedularComponent]
})
export class FeedbackModule { }
