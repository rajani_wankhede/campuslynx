import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentfeedbackSentHistoryComponent } from './studentfeedback-sent-history.component';

describe('StudentfeedbackSentHistoryComponent', () => {
  let component: StudentfeedbackSentHistoryComponent;
  let fixture: ComponentFixture<StudentfeedbackSentHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentfeedbackSentHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentfeedbackSentHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
