import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentfeedbackForFacultyComponent } from './studentfeedback-for-faculty.component';

describe('StudentfeedbackForFacultyComponent', () => {
  let component: StudentfeedbackForFacultyComponent;
  let fixture: ComponentFixture<StudentfeedbackForFacultyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentfeedbackForFacultyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentfeedbackForFacultyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
