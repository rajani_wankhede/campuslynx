import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvisorsfeddbackForStudentComponent } from './advisorsfeddback-for-student/advisorsfeddback-for-student.component';
import { StudentfeedbackSentHistoryComponent } from './studentfeedback-sent-history/studentfeedback-sent-history.component';
import { StudentfeedbackViewbyAdvisorComponent } from './studentfeedback-viewby-advisor/studentfeedback-viewby-advisor.component';
import { StudentfeedbackForMeComponent } from './studentfeedback-for-me/studentfeedback-for-me.component';
import { StudentfeedbackForFacultyComponent } from './studentfeedback-for-faculty/studentfeedback-for-faculty.component';
import { StudentAttendanceentrySchedularComponent } from './student-attendanceentry-schedular/student-attendanceentry-schedular.component';

const routes: Routes = [
    {
        path: '',
        data: {
          title: 'Feedback'
        },
        children: [
          {
            path: '',
            component: AdvisorsfeddbackForStudentComponent,
            data: {
              title: 'Advisors Feddback For Student'
            }
          },
          {
            path: 'StudentfeedbackSentHistory',
            component: StudentfeedbackSentHistoryComponent,
            data: {
              title: 'Student Feedback Sent History'
            }
          },
          {
            path: 'StudentfeedbackViewbyAdvisor',
            component: StudentfeedbackViewbyAdvisorComponent,
            data: {
              title: 'Student Feedback View by Advisor'
            }
          },
          {
            path: 'StudentfeedbackForMe',
            component: StudentfeedbackForMeComponent,
            data: {
              title: 'Student feedback For Me'
            }
          },
          {
            path: 'StudentfeedbackForFaculty',
            component: StudentfeedbackForFacultyComponent,
            data: {
              title: 'Student feedback For Faculty'
            }
          },
          {
            path: 'StudentAttendanceentrySchedular',
            component: StudentAttendanceentrySchedularComponent,
            data: {
              title: 'Student Attendance Entry Schedular New'
            }
          },
        ]
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedbackRoutingModule {}
