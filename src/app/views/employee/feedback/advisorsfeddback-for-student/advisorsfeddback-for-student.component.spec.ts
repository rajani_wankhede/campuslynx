import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvisorsfeddbackForStudentComponent } from './advisorsfeddback-for-student.component';

describe('AdvisorsfeddbackForStudentComponent', () => {
  let component: AdvisorsfeddbackForStudentComponent;
  let fixture: ComponentFixture<AdvisorsfeddbackForStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvisorsfeddbackForStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvisorsfeddbackForStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
