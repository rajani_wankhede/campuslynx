import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentfeedbackViewbyAdvisorComponent } from './studentfeedback-viewby-advisor.component';

describe('StudentfeedbackViewbyAdvisorComponent', () => {
  let component: StudentfeedbackViewbyAdvisorComponent;
  let fixture: ComponentFixture<StudentfeedbackViewbyAdvisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentfeedbackViewbyAdvisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentfeedbackViewbyAdvisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
