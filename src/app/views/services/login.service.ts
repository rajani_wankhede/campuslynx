import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActivatedRoute, Routes, Router } from '@angular/router';

import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

  constructor(private http: Http) { }

  loginAdmin(jsonObj) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    // return this.http.post('http://localhost:5000/admin/login', jsonObj, options).map(res => res.json());
     return this.http.post('http://125.17.78.251:8988/CampusLynxPortal/token/pretokencheck', jsonObj, options).map(res => res.json());

  }
  logout() {
    const id = localStorage.getItem('id');
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.post('http://localhost:5000/admin/logout/' + id, '', options).map(res => res.json());
  }
}
