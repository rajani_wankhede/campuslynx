import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { P404Component } from './404.component';
import { P500Component } from './500.component';
import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';
import { PagesRoutingModule } from './pages-routing.module';
import { ForgotpasswordComponent } from './forgotpassword.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RecaptchaModule } from 'ng-recaptcha';
import { AppFooterComponent } from 'app/components';
// import { BotDetectCaptchaModule } from 'angular-captcha';
@NgModule({
  imports: [ PagesRoutingModule ,
     HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
    }), RecaptchaModule.forRoot() ],
  declarations: [
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
      ForgotpasswordComponent
  ]
})
export class PagesModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
