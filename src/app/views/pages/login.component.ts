import { Component, ViewContainerRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from './../services/login.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

declare var Recaptcha: any;
declare var require: any;
declare var $: any;
declare var formValidation: any;
@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  password: any;
  email: any;
  a: any = '';
  b: any = '';
  c: any = '';
  d: any = '';
  e: any = '';
  code: any = '';
  why: any = '';
  LoginAs: any = '';
  username = '';
  parentlogin = '';
  // tslint:disable-next-line:max-line-length
  constructor(public toastr: ToastsManager, vcr: ViewContainerRef, private router: Router, private translate: TranslateService, private loginservice: LoginService) {
    this.toastr.setRootViewContainerRef(vcr);
    this.a = Math.ceil(Math.random() * 9) + '';
    this.b = Math.ceil(Math.random() * 9) + '';
    this.c = Math.ceil(Math.random() * 9) + '';
    this.d = Math.ceil(Math.random() * 9) + '';
    this.e = Math.ceil(Math.random() * 9) + '';
    this.code = this.a + this.b + this.c + this.d + this.e;
    // captch afunction
    function ValidCaptcha() {
      const str1 = removeSpaces($('#txtCaptcha').value);
      const str2 = removeSpaces($('#CaptchaInput').value);


      if (str1 === str2) {
        return true;
      } else {
        return false;
      }
    }

    // Remove the spaces from the entered and generated code
    function removeSpaces(string) {
      return string.split(' ').join('');
    }
  }
  switchLanguage(language: string) {
    this.translate.use(language);
    setTimeout(() => {
      console.log('i was here')
      if (language !== 'en') {
        return;
      }
      const obj = {
        'Title': 'Translation example',
        'Login': 'Login with new english',
        'Sign In to your account': 'Sign In to your account',
        'Student': 'Student',
        'Employee': 'Employee',
        'Other Employees': 'Other Employees',
        'Admin': 'Admin',
        'Offline': 'Offline',
        'Online': 'Online',
        'Sign up': 'Sign up',
        // tslint:disable-next-line:max-line-length
        'p1': 'Lorem ipsum dolor sit amet consectetur adipisicing elit sed to a half-time temporal incubator and lab and other magnet alloy.',
        'Register Now!': 'Register Now!',
        'Forgot password?': 'Forgot password?'
      };
      this.translate.setTranslation(language, obj);
    }, 3000);
  }
  // checkform(this) {
  //   localStorage.setItem('LoginAs', this.LoginAs);
  //   //   console.log('print function');
  //   //   // alert(this.translate  ' user login');
  //   //   this.router.navigate(['/dashboard'])

  // }
  logincheck() {
    console.log('in login form method');
    const jsonObj = JSON.stringify({
      username: this.username,
      parentlogin: 'N'
    });
    console.log(jsonObj)
    this.loginservice.loginAdmin(jsonObj).subscribe(result => {
      if (result.err === false) {
        this.toastr.success('You are LOGIN!', 'Success!');
        localStorage.setItem('Token', result.result.token);
        localStorage.setItem('id', result.result.id);
        localStorage.setItem('UserID', result.result.UserID);
        localStorage.setItem('adminname', result.result.adminname);
        this.router.navigate(['/dashboard']);
      } else {
        console.log(result);
        //  toastr.error('Please enter correct credintioal.', 'Error!')
        if (result.err === true || result.err.code === 404) {

          this.toastr.error('Username and password not found!', 'Oops!');
        } else if (result.err.code === 401) {
          this.toastr.error('Server not found!', 'Oops!');
        } else if (result.err.code === 501) {
          this.toastr.error('Internal Server not found!', 'Oops!');
        }
      }
    }, error => {
      if (error) {
        this.toastr.error('Internal Server error!', 'Oops!');
      }
    });
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    $(document).ready(function () {
      $('select[name="stylecss"]').change(function () {

        // tslint:disable-next-line:prefer-const
        let event = $('select[name="stylecss"]').val();
        localStorage.setItem('event', event);
        // $('#test option').length
        console.log(event);

        if (event === 'notselected') {
          require('style-loader!assets/css/style0.css');
          $('#logo0').show();
          $('#logo1').hide();
          $('#logo2').hide();
          $('#logo3').hide();
          //  imageslider
        } else if (event === 'pune') {
          require('style-loader!assets/css/style1.css');
          $('#logo1').show();

          $('#logo0').hide();
          $('#logo2').hide();
          $('#logo3').hide();


        } else if (event === 'delhi') {
          require('style-loader!assets/css/style2.css');
          $('#logo2').show();
          $('#logo1').hide();
          $('#logo3').hide();
          $('#logo0').hide();
        } else if (event === 'mumbai') {
          require('style-loader!assets/css/style3.css');
          $('#logo3').show();
          $('#logo2').hide();
          $('#logo1').hide();
          $('#logo0').hide();
        }

      });
          Recaptcha.create('6LecZEcUAAAAALvg0zJzcKB5UiiyC8yu_obikOJc', 'recaptcha', {
        theme: 'white',
        // captchaLoaded function will be called after the captcha image is loaded
        callback: captchaLoaded
      });

      function captchaLoaded() {
        // Add new field after loading captcha
        $('#defaultForm').formValidation('addField', 'recaptcha_response_field', {
          validators: {
            notEmpty: {
              message: 'The captcha is required and can\'t be empty'
            }
          }
        });
      };

      // // Call the FormValidation plugin
      // $('#defaultForm').formValidation({
      //   icon: {
      //     valid: 'glyphicon glyphicon-ok',
      //     invalid: 'glyphicon glyphicon-remove',
      //     validating: 'glyphicon glyphicon-refresh'
      //   }
      //   // You can define other settings and other fields on form here
      // });

      // // Replace 'PUBLIC_KEY' with your public key
      // // The second parameter 'recaptcha' is the Id of element showing up the captcha image
      // Recaptcha.create('6LecZEcUAAAAALvg0zJzcKB5UiiyC8yu_obikOJc', 'recaptcha', {
      //   theme: 'white'
      // });

    });
    $('div#imageslider').not(':has("div")').hide();
  }
  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response ${captchaResponse}:`);
  }
}
