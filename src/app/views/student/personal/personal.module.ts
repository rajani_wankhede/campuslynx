import { NgModule } from '@angular/core';

import { PersonalComponent } from './personal.component';
import { DrcradviceComponent } from './drcradvice/drcradvice.component';

// Forms Component
//import { FormsComponent } from './forms.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

// Components Routing
import { PersonalRoutingModule } from './personal-routing.module';

@NgModule({
  imports: [
    PersonalRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule
  ],
  declarations: [
   PersonalComponent,
   DrcradviceComponent
  ]
})
export class PersonalModule { }
