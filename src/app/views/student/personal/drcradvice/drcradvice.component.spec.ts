import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrcradviceComponent } from './drcradvice.component';

describe('DrcradviceComponent', () => {
  let component: DrcradviceComponent;
  let fixture: ComponentFixture<DrcradviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrcradviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrcradviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
