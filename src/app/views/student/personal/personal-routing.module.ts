import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonalComponent } from './personal.component';
import { DrcradviceComponent } from './drcradvice/drcradvice.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Personal'
    },
    children: [
      {
        path: '',
        component: PersonalComponent,
        data: {
          title: 'Personal'
        }
      },
      {
        path: 'drcradvice',
        component: DrcradviceComponent,
        data: {
          title: 'My DR/CR advice'
        }
      },
     
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalRoutingModule {}
