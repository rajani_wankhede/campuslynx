import { NgModule } from '@angular/core';

// Forms Component
//import { FormsComponent } from './forms.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

import { FeedbackComponent } from './feedback.component';
import { FeedbackHistoryComponent } from './feedback-history/feedback-history.component';
import { FeedbackformComponent } from './feedbackform/feedbackform.component';

// Components Routing
import { FeedbackRoutingModule } from './feedback-routing.module';
import { ResultComponent } from 'app/views/student/exam/result/result.component';

@NgModule({
  imports: [
    FeedbackRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule
  ],
  declarations: [
 FeedbackformComponent,
 FeedbackHistoryComponent
  ]
})
export class FeedbackModule { }
