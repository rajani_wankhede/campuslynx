import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedbackComponent } from './feedback.component';
import { FeedbackHistoryComponent } from './feedback-history/feedback-history.component';
import { FeedbackformComponent } from './feedbackform/feedbackform.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Exam'
    },
    children: [
      {
        path: '',
        component: FeedbackformComponent,
        data: {
          title: 'Feedback form'
        }
      },
      {
        path: 'history',
        component: FeedbackHistoryComponent,
        data: {
          title: 'Feedbackhistory'
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedbackRoutingModule {}
