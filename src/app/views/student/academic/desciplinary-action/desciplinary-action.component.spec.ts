import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesciplinaryActionComponent } from './desciplinary-action.component';

describe('DesciplinaryActionComponent', () => {
  let component: DesciplinaryActionComponent;
  let fixture: ComponentFixture<DesciplinaryActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesciplinaryActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesciplinaryActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
