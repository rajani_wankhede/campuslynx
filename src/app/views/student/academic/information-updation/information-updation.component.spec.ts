import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationUpdationComponent } from './information-updation.component';

describe('InformationUpdationComponent', () => {
  let component: InformationUpdationComponent;
  let fixture: ComponentFixture<InformationUpdationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationUpdationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationUpdationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
