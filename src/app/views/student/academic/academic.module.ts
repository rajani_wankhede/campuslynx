import { NgModule } from '@angular/core';

import { AcademicComponent } from './academic.component';
import { ClassAttendanceComponent } from './class-attendance/class-attendance.component';
import { ClassTimeTableComponent } from './class-time-table/class-time-table.component';
import { CourseRegistrationComponent } from './course-registration/course-registration.component';
import { DesciplinaryActionComponent } from './desciplinary-action/desciplinary-action.component';
import { ElectiveChoiceComponent } from './elective-choice/elective-choice.component';
import { FeeDetailComponent } from './fee-detail/fee-detail.component';
import { InformationUpdationComponent } from './information-updation/information-updation.component';
// Forms Component
//import { FormsComponent } from './forms.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

// Components Routing
import { AcademicRoutingModule } from './academic-routing.module';

@NgModule({
  imports: [
    AcademicRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule
  ],
  declarations: [
  AcademicComponent,
  ClassTimeTableComponent,
  ClassAttendanceComponent,
  CourseRegistrationComponent,
  DesciplinaryActionComponent,
  ElectiveChoiceComponent,
  FeeDetailComponent,
  InformationUpdationComponent
  ]
})
export class AcademicModule { }
