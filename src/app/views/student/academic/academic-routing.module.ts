import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AcademicComponent } from './academic.component';
import { ClassAttendanceComponent } from './class-attendance/class-attendance.component';
import { ClassTimeTableComponent } from './class-time-table/class-time-table.component';
import { CourseRegistrationComponent } from './course-registration/course-registration.component';
import { DesciplinaryActionComponent } from './desciplinary-action/desciplinary-action.component';
import { ElectiveChoiceComponent } from './elective-choice/elective-choice.component';
import { FeeDetailComponent } from './fee-detail/fee-detail.component';
import { InformationUpdationComponent } from './information-updation/information-updation.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Academic'
    },
    children: [
      {
        path: '',
        component: ElectiveChoiceComponent,
        data: {
          title: 'Student Elective Choice'
        }
      },
      {
        path: 'ClassTimetable',
        component: ClassTimeTableComponent,
        data: {
          title: 'My class time Table'
        }
      },
      {
        path: 'informationUpdate',
        component: InformationUpdationComponent,
        data: {
          title: 'Information Updation'
        }
      },
      {
        path: 'CourseRegistration',
        component: CourseRegistrationComponent,
        data: {
          title: 'Course Registration'
        }
      },
      {
        path: 'ClassAttendance',
        component: ClassAttendanceComponent,
        data: {
          title: 'Class Attendance'
        }
      },
      {
        path: 'DesciplinaryAction',
        component: DesciplinaryActionComponent,
        data: {
          title: 'Desciplinary Action'
        }
      },
      {
        path: 'FeeDetails',
        component: FeeDetailComponent,
        data: {
          title: 'Fee Details'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcademicRoutingModule {}
