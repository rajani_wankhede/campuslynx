import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectiveChoiceComponent } from './elective-choice.component';

describe('ElectiveChoiceComponent', () => {
  let component: ElectiveChoiceComponent;
  let fixture: ComponentFixture<ElectiveChoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectiveChoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectiveChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
