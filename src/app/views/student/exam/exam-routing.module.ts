import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExamComponent } from './exam.component';
import { ExamScheduleComponent } from './exam-schedule/exam-schedule.component';
import { InternalExamMarksComponent } from './internal-exam-marks/internal-exam-marks.component';

import { ResultComponent } from './result/result.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Exam'
    },
    children: [
      {
        path: '',
        component: ExamScheduleComponent,
        data: {
          title: 'Examination Schedule'
        }
      },
      {
        path: 'internalExamEventMarks',
        component: InternalExamMarksComponent,
        data: {
          title: 'Internal Exam Event Wise Marks'
        }
      },
      {
        path: 'result',
        component: ResultComponent,
        data: {
          title: 'Student Result SGPA/CGPA'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamRoutingModule {}
