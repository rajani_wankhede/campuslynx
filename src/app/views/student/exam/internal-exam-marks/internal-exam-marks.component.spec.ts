import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalExamMarksComponent } from './internal-exam-marks.component';

describe('InternalExamMarksComponent', () => {
  let component: InternalExamMarksComponent;
  let fixture: ComponentFixture<InternalExamMarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalExamMarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalExamMarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
