import { NgModule } from '@angular/core';


import { ExamComponent } from './exam.component';
import { ExamScheduleComponent } from './exam-schedule/exam-schedule.component';
import { InternalExamMarksComponent } from './internal-exam-marks/internal-exam-marks.component';

// Forms Component
//import { FormsComponent } from './forms.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

// Components Routing
import { ExamRoutingModule } from './exam-routing.module';
import { ResultComponent } from 'app/views/student/exam/result/result.component';

@NgModule({
  imports: [
    ExamRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule
  ],
  declarations: [
 ExamComponent,
 InternalExamMarksComponent,
 ResultComponent,
 ExamScheduleComponent
  ]
})
export class ExamModule { }
